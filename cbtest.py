""" Test suite for chipbeta and dataproc

When run as a script, this module will perform the 
full test suite for chipbeta.py and dataproc.py.

Authors: **Eric M. Benner and Daniel O'Malley**

Date:    7/24/14

Updated: 9/11/14
"""

"""
TODO: Software License?
"""

"""
TODO: Use try/except structures?
"""

import chipbeta as cb
import dataproc as dp
import matplotlib.pyplot as plt
import numpy as np
import time

# 
#  Standard string for anouncing test
#
def printTestInitMessage(mstring, stdRuntime):
	# Print out the formatting around string for test start
	print ""
	print "******************************************************************"
	print "  "+mstring
	if stdRuntime < 2:
		print "  - This test should take a second or less"
	else:
		print "  - This test should take approximately "+str(round(stdRuntime))+" seconds"
	print ""

# 
#  Standard string message for ending a test
#
def printTestEndMessage(success, residual, errorType, runtime):
	# Print out the formatted strings for end of test
	if success:
		print "  - Run Succeded!"
	else: 
		print "  - Run Failed!"
		print "    *"+errorType
	print "  - Residual: "+str(residual)
	print "  - Runtime:  "+str(runtime)+" seconds"
	print "------------------------------------------------------------------"

# 
#  Standard string message for start of full test suite
#
def printRunAllTests():
	# Print out the formatted strings for beginning all tests in the suite
	print ""
	print "******************************************************************"
	print ""
	print "                RUNNING FULL CHIPBETA TEST SUITE                  "
	print ""
	print "******************************************************************"

# 
#  Standard string message for a failed set of tests
#
def printAllTestsEnd(mstring, t):
	# Print out the formatted strings for a failed suite of tests
	print ""
	print "******************************************************************"
	print ""
	print "                    CHIPBETA TESTS COMPLETED                      "
	print ""
	print mstring
	print ""
	print "         TOTAL TEST SUITE RUNTIME: "+str(t)+" SECONDS"
	print ""
	print "******************************************************************"
	print ""

# 
#  Standard string message for a succesful set of tests
#
def printTestsSucceded(t):
	# Print out the formatted strings for a successful suite of tests
	##### "******************************************************************"
	mst = "                        ALL TESTS PASSED!                         "
	printAllTestsEnd(mst, t)

# 
#  Standard string message for a failed set of tests
#
def printTestsFailed(n, nfail, t):
	# Print out the formatted strings for a failed suite of tests
	##### "******************************************************************"
	##### "                      FAILED ** OF ** TESTS                       "
	mst = "                      FAILED "+str(nfail)+" OF "+str(n)+" TESTS"
	printAllTestsEnd(mst, t)

#######################################################################
#  Unit tests
#######################################################################

#
# Test data I/O functionality
#

# print out the coalated data
#for i in range(len(T)):
#print str(T[i])+","+str(H[i])+","+str(B[i])+","+str(ET[i])

# TODO: write test of simple I/O routines

#######################################################################
#  Regression tests
#######################################################################

# TODO: tests of data with real pumping drawdowns

#
# Configure dictionary of functions and standard results for all tests
#
def setupTestsData():
	"""Set up the dictionary of tests and results
	
	This function sets up loading routines and standard reference data
	for the set of test data into a single dictionary.  Each test has a
	dictionary with datasets and key names.
	This function does not require and inputs.

	When tests fail, these data may need to be re-baselined.
	This is best done by running the runDiagnosticTest() function on
	the failed test, determining the cause, and if needed 
	rebaselining the problem.  
	The data which is printed may be copied directly into the np.array
	structures, but *commas* will also need to be added.
	
	:returns: dictionary of test data
	:rtype: dict
	
	"""
	# initialize the dictionary
	D = dict()
	#
	# --- ExR1Mo
	#
	testname = 'ExR1Mo'
	# set name and loading function
	Di = dict({'testName': testname, 'loadFunction': "loadExR1Mo"})
	# add standard nonpumping results
	an = np.array([-0.02422231, -0.00253667,  0.00021759,  0.00208381, -0.00157938, -0.00177905, -0.00363721])
	bn = np.array([0.0115869,  -0.0596031,   0.1360594,  -0.17594997,  0.13581128, -0.0593415,  0.01149946])
	pn = np.array([])
	Di['noPumpingData'] = [an, bn, pn]
	Di['noPumpingResidual'] = 1.47082962977
	Di['noPumpingStdRuntime'] = 0.0797009468079
	# add standard pumping results
	ap = np.array([-2.39327903e-02,  -3.10134294e-03,  -8.61975506e-07,   2.01034205e-03,  -5.69843174e-04,  -2.45917653e-03,  -3.07518617e-03])
	bp = np.array([0.00968693, -0.04978214,  0.11337769, -0.14618122,  0.11244444, -0.04894487,  0.00944318])
	pp = np.array([6.24226778e-03,   1.04371525e+03])
	Di['pumpingData'] = [ap, bp, pp]
	Di['pumpingResidual'] = 1.46757119024
	Di['pumpingStdRuntime'] = 58.3420798779
	# add dataset dictionary to full dictionary
	D[testname] = Di

	#
	# --- ExR3Mo
	#
	testname = 'ExR3Mo'
	# set name and loading function
	Di = dict({'testName': testname, 'loadFunction': "loadExR3Mo"})
	# add standard nonpumping results
	an = np.array([-2.56963227e-02,  -1.16160840e-03,   3.04805124e-03,  -3.22178791e-03,  -2.74380194e-05,  -2.42545577e-03,   3.62067131e-04])
	bn = np.array([-0.00785578,  0.04115297, -0.09557821,  0.12484241, -0.09658901,  0.04201864, -0.00808581])
	pn = np.array([])
	Di['noPumpingData'] = [an, bn, pn]
	Di['noPumpingResidual'] = 3.91490746431
	Di['noPumpingStdRuntime'] = 0.223679065704
	# add standard pumping results
	ap = np.array([-0.02746617, -0.00107239,  0.00207901, -0.00479857,  0.00034205, -0.00343251,  0.0041699])
	bp = np.array([-0.01953931,  0.10076027, -0.23283479,  0.30596209, -0.24054055,  0.10720617, -0.02125877])
	pp = np.array([1.00000000e-09,   2.88484192e+03])
	Di['pumpingData'] = [ap, bp, pp]
	Di['pumpingResidual'] = 4.95235462558
	Di['pumpingStdRuntime'] = 40.1038181782
	# add dataset dictionary to full dictionary
	D[testname] = Di

	#
	# --- R34Raw
	#
	testname = 'R34Raw'
	# set name and loading function
	Di = dict({'testName': testname, 'loadFunction': "loadR34Raw"})
	# add standard nonpumping results
	an = np.array([-0.00987073, -0.00184213, -0.00061672, -0.00022091,  0.00119508, -0.00465685,  0.00137187])
	bn = np.array([-0.00346502,  0.01955994, -0.04726241,  0.06245945, -0.04764803,  0.01992845, -0.00358109])
	pn = np.array([])
	Di['noPumpingData'] = [an, bn, pn]
	Di['noPumpingResidual'] = 6.7070135832
	Di['noPumpingStdRuntime'] = 5.94774699211
	# add standard pumping results
	ap = np.array([-0.00994521, -0.00180043, -0.00053728, -0.00028292,  0.00118462, -0.0047805,  0.0015329])
	bp = np.array([-0.00387193,  0.02163308, -0.05194832,  0.06845261, -0.05222449,  0.02191162, -0.00396479])
	pp = np.array([2.21602017e-02,   8.66517757e+02])
	Di['pumpingData'] = [ap, bp, pp]
	Di['pumpingResidual'] = 6.72200492369
	Di['pumpingStdRuntime'] = 106.295593977
	# add dataset dictionary to full dictionary
	D[testname] = Di

	#
	# --- Sca5Raw
	#
	testname = 'Sca5Raw'
	# set name and loading function
	Di = dict({'testName': testname, 'loadFunction': "loadSca5Raw"})
	# add standard nonpumping results
	an = np.array([8.23920886e-07,   1.72105545e-05,  -1.02255278e-05,  -6.74917906e-06,   1.99207824e-05,  -1.54048227e-05,   1.19605982e-05])
	bn = np.array([-0.0001229,   0.00062836, -0.00142331,  0.0018218,  -0.00139025,  0.00060097, -0.00011567])
	pn = np.array([])
	Di['noPumpingData'] = [an, bn, pn]
	Di['noPumpingResidual'] = 0.00516012810461
	Di['noPumpingStdRuntime'] = 6.25559091568
	# add standard pumping results
	ap = np.array([1.09855644e-06,   1.91981033e-05,  -6.33294357e-06,  -4.84367764e-06,   2.24896992e-05,  -1.46143559e-05, 1.23806393e-05])
	bp = np.array([-0.00014208,  0.00072999, -0.00166478,  0.00214733, -0.00165193,  0.00071975, -0.00013958])
	pp = np.array([1.11135771e-02,   9.85727533e+02])
	Di['pumpingData'] = [ap, bp, pp]
	Di['pumpingResidual'] = 0.00541010173652
	Di['pumpingStdRuntime'] = 66.796254158
	# add dataset dictionary to full dictionary
	D[testname] = Di

	#
	# --- Sci2Raw
	#
	testname = 'Sci2Raw'
	# set name and loading function
	Di = dict({'testName': testname, 'loadFunction': "loadSci2Raw"})
	# add standard nonpumping results
	an = np.array([-0.00444443, -0.02697747,  0.0005432,  -0.00063114, -0.00015678, -0.00088655,  0.00121404])
	bn = np.array([ 0.00184402, -0.00939758,  0.02109056, -0.02657935,  0.01983874, -0.00833452,  0.0015511])
	pn = np.array([])
	Di['noPumpingData'] = [an, bn, pn]
	Di['noPumpingResidual'] = 0.0621185241112
	Di['noPumpingStdRuntime'] = 5.80837893486
	# add standard pumping results
	ap = np.array([-0.00392629, -0.02611875, -0.00073139, -0.0017512,   0.00246259, -0.00012247, -0.00071053])
	bp = np.array([0.0020377,  -0.01190452,  0.02962482, -0.04052704,  0.03223316, -0.01418214,  0.00270649])
	pp = np.array([3.09939653e-01,   4.11279901e+02])
	Di['pumpingData'] = [ap, bp, pp]
	Di['pumpingResidual'] = 0.406629990877
	Di['pumpingStdRuntime'] = 33.1973948479
	# add dataset dictionary to full dictionary
	D[testname] = Di

	#
	# --- Kac1
	#
	testname = 'Kac1'
	# set name and loading function
	Di = dict({'testName': testname, 'loadFunction': "loadKac1"})
	# add standard nonpumping results
	an = np.array([-0.6857551,   0.32463937,  0.10554776,  0.06314958, -0.0184905,  -0.00359852,  0.04731387])
	bn = np.array([8.69106267e-05,  -2.63891464e-04,   4.44530346e-04,  -4.78045545e-04,   3.55508645e-04,  -1.78917789e-04,   5.56403997e-05])
	pn = np.array([])
	Di['noPumpingData'] = [an, bn, pn]
	Di['noPumpingResidual'] = 0.0124243805739
	Di['noPumpingStdRuntime'] = 0.0110058784485
	# add standard pumping results
	ap = np.array([-0.7240239,   0.34451609,  0.18034237,  0.06518455, -0.03457462, -0.02500131, -0.02663295])
	bp = np.array([2.68808924e-05,  -1.25795679e-04,   1.59400686e-04,  -4.25209373e-05,  -1.50965363e-04,   1.80199167e-04,  -8.68812443e-05])
	pp = np.array([6.51724253e-02,   3.61840587e+02])
	Di['pumpingData'] = [ap, bp, pp]
	Di['pumpingResidual'] = 0.0709109561713
	Di['pumpingStdRuntime'] = 3.65219092369
	# add dataset dictionary to full dictionary
	D[testname] = Di

	#
	# --- Wipp30
	#
	testname = 'Wipp30'
	# set name and loading function
	Di = dict({'testName': testname, 'loadFunction': "loadWipp30"})
	# add standard nonpumping results
	an = np.array([-0.14937882, -0.19451978, -0.11546476, -0.07602899, -0.05607306, -0.0213534, -0.03682927])
	bn = np.array([-0.00140999,  0.00670689, -0.01400751,  0.01623784, -0.01089338,  0.00391871, -0.00055396])
	pn = np.array([])
	Di['noPumpingData'] = [an, bn, pn]
	Di['noPumpingResidual'] = 0.231784562565
	Di['noPumpingStdRuntime'] = 0.131102085114
	# add standard pumping results
	ap = np.array([-0.15131773, -0.19345709, -0.1068687,  -0.07728783, -0.0615721,  -0.0175495, -0.03887341])
	bp = np.array([-0.00155114,  0.00751797, -0.01591382,  0.0186119,  -0.01254189,  0.00451571, -0.00063876])
	pp = np.array([1.,          447.06402301])
	Di['pumpingData'] = [ap, bp, pp]
	Di['pumpingResidual'] = 0.654869238368
	Di['pumpingStdRuntime'] = 222.594362974
	# add dataset dictionary to full dictionary
	D[testname] = Di
	
	return D
	# END LOADING DICTIONARY

#
# Run a single test on a data set with or without pumping
#
def runSingleTest(Di, doPumping, diagnostics=False):
	"""Run test on a single data set

	This function runs a standard test on one data set.  
	It is not usually run by itself but through
	runDiagnosticTest() or runAllTests().
	
	:param Di: Dictionary containing problem name, loading function, and standard data
	:type Di: dict
	:param doPumping: Turns on pumping mode if True
	:type doPumping: bool
	:param diagnostics: diagnostic mode - prints out alpha, beta, ppar, and calculated standard regression, and plots current data (default=False) 
	:type diagnostics: bool
	:returns: Success status
	:rtype: int
	
	"""
	# start the clock	
	ticker = time.time()
	# Load the data
	loadfun = getattr(dp, Di['loadFunction'])
	T, H, B, ET = loadfun()
	Horig = H
	#
	#  Initialize data and specs
	# 
	if doPumping:
		# starting message		
		printTestInitMessage("Running pumping test of: "+Di['testName'], Di['pumpingStdRuntime'])
		# load reference parameters
		refres = Di['pumpingResidual']
		abpr = Di['pumpingData']
		# set data
		minT = min(T)
		maxT = max(T)
		Q = [np.array([2000., 0.])]
		Qtime = [np.array([.66 * minT + .34 * maxT, .34 * minT + .66 * maxT])]
		r = [np.array(100.)]
		# params
		stor = 0.01
		trans = 1000.
		ppar = np.array([stor, trans])
		# initialize pumped H
		H += cb.getPumpingEffect(T, Q, r, Qtime, ppar)
		ppar0 = cb.guessPumpingParams(T, H, Q[0], r[0], Qtime[0])
		#ppar0 = np.array([0.001, 100.])
	else:
		# starting message
		printTestInitMessage("Running test of: "+Di['testName'], Di['noPumpingStdRuntime'])
		# load reference parameters
		refres = Di['noPumpingResidual']
		abpr = Di['noPumpingData']
		# set data
		Q = []
		Qtime = []
		r = []
		# params
		ppar0 = []
	#
	#  Compute system
	#
	# parameter settings
	alpham = 7
	betam = 7
	mab = max(alpham, betam)
	# calc initial alpha beta
	alpha0, beta0 = cb.getAlphaBet(H, B, ET, alpham, betam)
	# get best parameters
	alpha, beta, ppar = cb.getBestParams(T, H, B, ET, Q, r, Qtime, alpha0, beta0, ppar0,'normal')
	if diagnostics:
		print "  alpha: "
		print alpha
		print "  beta: "
		print beta
		print "  pumping [S, T]: "
		print ppar
	# correct the data (for speed testing)
	Hnew = cb.correctHead(T, H, B, ET, Q, r, Qtime, alpha, beta, ppar)
	# get residual
	res = cb.getSquaredError(T, H, B, ET, Q, r, Qtime, alpha, beta, ppar)
	oldres = cb.getSquaredError(T, H, B, ET, Q, r, Qtime, *abpr)
	if diagnostics:
		print "  reference residual: "
		print refres
		print "  calculated old residual: "
		print oldres
	# 
	#  Check for success
	#
	# initialize
	success = True
	errType = ""
	# comparison criterion
	conveps = 1e-10 # don't go tighter because of sig figs!
	rdiff = abs((res - refres)/refres)
	if (rdiff > conveps):
		success = False
		errType += " Residual of result did not match standard residual! "
	# comparison criterion
	conveps = 1e-4 # don't go tighter because of sig figs!
	rdiff = abs((res - oldres)/oldres)
	if (rdiff > conveps):
		success = False
		errType += " Residual of result did not match standard dataset calculated residual! "
	# pumping parameter sanity check
	if doPumping:
		if (ppar[0] >= 1) or (ppar[0] <= 1e-9) or (ppar[1] <= 1) or (ppar[1] >= 1e6):
			success = False
			errType += " Pumping parameters are out of bounds! "
	#
	#  print status
	#
	printTestEndMessage(success, res, errType, time.time()-ticker)
	
	#
	# plot for diagnostics
	#
	#'''
	if diagnostics:
		nbins = 50
		# Calc new data
		# correct the data
		Hb = cb.correctHead(T, H, B, 0*ET, [], [], [], alpha, beta, [])
		Hbt = cb.correctHead(T, H, B, ET, [], [], [], alpha, beta, [])
		if doPumping:
			Hall = Hnew
		# Plot smoothing effects only
		plt.figure()
		# data plot
		plt.subplot(211)
		plt.title("Data over time")	
		plt.plot(T, Horig, 'r+', label="raw data")
		plt.plot(T[mab + 1:], Hb, 'm', label="barometric")
		plt.plot(T[mab + 1:], Hbt, 'b', label="tide and baro")
		if doPumping:
			plt.plot(T[mab + 1:], Hall, 'g+', label="all")
		plt.xlabel("time (h)")
		plt.ylabel("well head (ft)")
		plt.legend(loc=2)
		# histograms
		plt.subplot(212)
		plt.title("Local variation of data")
		plt.hist(np.diff(Horig), bins=nbins, color='red', label="raw data")
		plt.hist(np.diff(Hb), bins=nbins, color='magenta', label="barometric")
		plt.hist(np.diff(Hbt), bins=nbins, color='blue', label="tide and baro")
		if doPumping:
			plt.hist(np.diff(Hall), bins=nbins, color='green', label="all")
		plt.xlabel("differential well head (ft)")
		plt.ylabel("counts")
		plt.legend(loc=2)
		# show the figure
		plt.show()
	#'''
	
	# Return success
	if success:
		return 0
	else:
		return 1
	# END OF TEST

#
# Run a single test for with full diagnostics for both pumping and non-pumping
#
def runDiagnosticTest(testname, diagnostics=True):
	"""Runs test for pumping/no pumping with full diagnostics on

	This function runs a full test with artificial pumping both off then on.  
	It is designed for re-baselining results and is very useful for debugging!

	:param testname: name of test to run
	:type testname: string
	:param diagnostics: diagnostic mode - True prints out alpha, beta, ppar, and calculated standard regression, and plots current data (default=True)
	:type diagnostics: bool

	"""
	print ""
	print ""
	print "  CHIPBETA DIAGNOSTIC TEST OF: "+testname
	print ""
	# initialize the data
	D = setupTestsData()
	# loop over 
	for i in [False, True]:
		runSingleTest(D[testname], i, diagnostics)
	print ""
	print "  END CHIPBETA DIAGNOSTIC TEST"
	print ""
	# END OF TEST

#
#  Run all tests in the testing suite 
#
def runAllTests(diagnostics=False):
	"""Runs all tests in the suite
	
	This function runs all test that are loaded into the test
	dictionary and returns whether all tests were passed.  
	In future this function may be used to set up hooks for the
	code.
	
	:param diagnostics: diagnostic mode - prints out alpha, beta, ppar, and calculated standard regression, and plots current data (default=False) 
	:type diagnostics: bool
	:returns: Success status
	:rtype: bool
	
	"""
	printRunAllTests()
	# start total timer
	aticker = time.time()
	# initialize for loop
	nrun = 0
	nfail = 0
	#
	#  Main data tests
	#
	D = setupTestsData()
	# loop over all tests
	for i in [False, True]:
		for j in iter(D):
			nrun += 1
			nfail += runSingleTest(D[j], i, diagnostics)
	# 
	#  Print Results
	# 
	if nfail == 0:
		printTestsSucceded(time.time() - aticker)
		return True
	else:
		printTestsFailed(nrun, nfail, time.time() - aticker)
		return False
	# END OF TEST

#
#  script to run
#
if __name__ == "__main__":
	#runDiagnosticTest("ExR1Mo")
	#runDiagnosticTest("Wipp30")
	fullSuccess = runAllTests(True)
	# END OF TESTS TO RUN

#
#  End of File
#
