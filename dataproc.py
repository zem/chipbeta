""" Data import, generation, and output functions for Chipbeta

Authors: **Eric M. Benner and Daniel O'Malley**

Date:    7/9/14

Updated: 9/15/14
"""

"""
TODO: Software License?
"""

"""
TODO: Use try/except structures?
"""

# packages
import chipbeta as cb
import copy
import datetime as dt
import math
import numpy as np
import os
import pickle
import random
import scipy.interpolate as interp
###############################################################################

#
#  List processing (internal functions)
# 
# TODO: python to mysql database routines for direct data pulls

# string of date and time in iso format to datetime object
def sdtIso2dt(Tinp):
	# Convert string of date and time (in iso format) to datetime object
	# break down string
	Ti = Tinp.split("-")
	Tj = Ti[2].split(":")
	Tj[0] = Tj[0].split(" ")[1]
	Ti[2] = Ti[2].split(" ")[0]
	# assemble datetime object
	T = dt.datetime(int(Ti[0]), int(Ti[1]), int(Ti[2]),
                    int(Tj[0]), int(Tj[1]), int(Tj[2]))
	# return 
	return T

# process list into timedelta list
def tList2dt(t0, T):
	# Convert list of times into timedelta list anchored at a single reference time
	dT = []
	for i in range(len(T)):
		dT.append((T[i]-t0))
	return dT

# take list of time deltas and make it a list of just times in seconds
def dTList2seconds(T):
	# Convert time delta list into list of seconds
	Ts = []
	for i in range(len(T)):
		Ts.append(T[i].seconds + 24*3600*T[i].days)
	return Ts

# take list of datetimes and convert them to hours from a base time
def dtList2hours(t0, T):
	'''Take list of datetimes and convert them to hours from a base time
	
	This function is helpful to give correct time type with 
	getPumpingEffect().
	
	:param t1: Base time
	:type t1: datetime
	:param T: List of times
	:type T: list of datetimes
	:returns: Time difference list in hours
	:rtype: list of ints
	
	.. _fun-dtList2hours:
	'''
	Th = []
	for i in range(len(T)):
		dti = T[i] - t0
		Th.append( dti.days*24 + int(math.floor(dti.seconds/3600)) )
	return Th

# check if variable is a float
def isfloat(s):
	# Check if variable is a float
	try:
		float(s)
		return True
	except ValueError:
		return False
###############################################################################

#
#  File processing
#
# TODO: further adaptions of method for the Q, r, Qt data of pumping?
# TODO: coupling with Wells/MADS?

# convert file into datetime and data lists
def readTextDtd(filename):
	"""Process date time, data text files into datetime and data string lists

	Takes a text file of the form date time, single data and returns lists of
	the data and times (in increasing order by time).

	:param filename: name of file with time and data
	:type filename: string
	:returns: data time
	:rtype: list of fl64, list of datetime objects

	.. _fun-readTextDtd:
	"""
	# Internal function
	# string of date and time in iso format to datetime object
	def sdtIso2dt(Tinp):
		"""Convert string of date and time (in iso format) to datetime object"""
		# break down string
		Ti = Tinp.split("-")
		Tj = Ti[2].split(":")
		Tj[0] = Tj[0].split(" ")[1]
		Ti[2] = Ti[2].split(" ")[0]
		# assemble datetime object
		T = dt.datetime(int(Ti[0]), int(Ti[1]), int(Ti[2]),
		            int(Tj[0]), int(Tj[1]), int(Tj[2]))
		# return 
		return T

	# input file data
	with open(filename, "r") as f:
		lines = f.readlines()
		f.close()
	# process for date and barometric
	Ti = []
	D = []
	for j in range(len(lines)):
		splitline = lines[j].split(",")
		if isfloat(splitline[1]):
			Ti.append(splitline[0])
			D.append(float(splitline[1]))
	# process dates and times
	T = []
	for i in range(len(Ti)):
		Tii = sdtIso2dt(Ti[i])
		T.append(Tii)
	# uniformly orient time
	if T[1] > T[-1]:
		T.reverse()
		D.reverse()
	# return
	return D, T

# print out num elements of datetime and data list for debugging
def printDtd(D, T, num=0):
	"""Print out items in the datetime and data lists
	
	This function prints out time data in the ISO format and any 
	data set of interest.  
	This is primarily useful for I/O debugging. 
	
	:param D: data to print
	:type D: fl64 list
	:param T: times to print
	:type T: datetime list
	:param num: number of data points to print out (default all)
	:type num: int

	.. _fun-printDtd:
	"""
	# if num is zero then do all points
	if num == 0:
		num = len(T)
	# print out the data
	for i in range(num):
		print T[i].isoformat()+","+str(D[i])
	# Return
	return

# Find best beginning and end data points in units of seconds 
# for single list or pair of times
def roundStartEndTimes(Tse, dateZero):
	# Bound the times of complete data record
	# latest first time point
	ts = min(dTList2seconds(tList2dt(dateZero, Tse)))
	# set start time
	StartTime = int(3600*(math.ceil(float(ts)/3600.0)))

	# find earliest last time point
	te = max(dTList2seconds(tList2dt(dateZero, Tse)))
	# set end time
	EndTime = int(3600*(math.floor(float(te)/3600.0)))

	# return
	return StartTime, EndTime

# Find best beginning and end data points in units of seconds
def findStartEndTimes(T1, T2, T3, dateZero):
	# Bound the times of complete data record
	# latest first time point
	ts = max(min(dTList2seconds(tList2dt(dateZero, T1))), min(dTList2seconds(tList2dt(dateZero, T2))), min(dTList2seconds(tList2dt(dateZero, T3))))
	# set start time
	StartTime = int(3600*(math.ceil(float(ts)/3600.0)))

	# find earliest last time point
	te = min(max(dTList2seconds(tList2dt(dateZero, T1))), max(dTList2seconds(tList2dt(dateZero, T2))), max(dTList2seconds(tList2dt(dateZero, T3))))
	# set end time
	EndTime = int(3600*(math.floor(float(te)/3600.0)))

	# return
	return StartTime, EndTime

# dataset loading for single physical variable
def getPhysicalData(Pfile, dates):
	"""Assemble matched hydrological data from files
	
	This function collects all data for a particular physical effect
	from the given file over the time span of the given set of dates.
	It uses interpolation to correlate data at each hour.
	A default zero time of January 1, 2000 at 12:00:00 AM is used.
	
	This function is primarily used in the makeDataWithBET() function.
	
	:param Hfile: name of file with pressure head times and values
	:type Hfile: string
	:param dates: List of Start and End dates/times
	:type dates: Two item list of datetimes
	:returns: Physical force array
	:rtype: ndarray of fl64
	
	.. _fun-getPhysicalData:
	"""
	# zero time for hourly times
	dateZero = dt.datetime(2000,1,1,0,0,0)
	# data
	Pdata, Tp = readTextDtd(Pfile)
	dTps = dTList2seconds(tList2dt(dateZero, Tp))
	fP = interp.interp1d(dTps, Pdata)
	# set data ranges and times
	TStart, TEnd = roundStartEndTimes(dates, dateZero)
	Tsec = range(TStart, TEnd, 3600)
	#T = []
	#for i in range(len(Tsec)): 
	#	T.append(Tsec[i]/3600)
	# Process interpolated data
	P = fP(Tsec)
	# END OF FUNCTION
	return np.array(P)
###############################################################################

#
# Process the normal three data files from test directories
#
# combine all head, barometric, and earth tide data into four 
# synchronized arrays
def assembleHydroData(Hfile, Bfile, ETfile):
	"""Assemble matched hydrological data from files
	
	This function collects and matches all data from filenames of the head,
	barometric and earthtide time/data files.  
	It uses interpolation to correlate data at each hour.
	A default zero time of January 1, 2000 at 12:00:00 AM is used.
	
	This function is used heavily in the load***() functions.
	
	:param Hfile: name of file with pressure head times and values
	:type Hfile: string
	:param Bfile: name of file with barometric times and values
	:type Bfile: string
	:param ETfile: name of file with Earth tide head times and values
	:type ETfile: string
	:returns: Time, Head, Barometric, and EarthTide arrays
	:rtype: 4 ndarrays of fl64
	
	.. _fun-assembleHydroData:
	"""
	# zero time for hourly times
	dateZero = dt.datetime(2000,1,1,0,0,0)
	
	# head data
	Hdata, Th = readTextDtd(Hfile)
	dThs = dTList2seconds(tList2dt(dateZero, Th))
	fH = interp.interp1d(dThs, Hdata)
	
	# barometric data
	Bdata, Tb = readTextDtd(Bfile)
	dTbs = dTList2seconds(tList2dt(dateZero, Tb))
	fB = interp.interp1d(dTbs, Bdata)
	
	# earth tide data
	ETdata, Te = readTextDtd(ETfile)
	dTes = dTList2seconds(tList2dt(dateZero, Te))
	fET = interp.interp1d(dTes, ETdata)
	
	# set data ranges and times
	TStart, TEnd = findStartEndTimes(Th, Tb, Te, dateZero)
	Tsec = range(TStart, TEnd, 3600)
	T = []
	for i in range(len(Tsec)): 
		T.append(Tsec[i]/3600)
	
	# Process interpolated data
	H = fH(Tsec)
	B = fB(Tsec)
	ET = fET(Tsec)

	# return
	return np.array(T), np.array(H), np.array(B), np.array(ET)

# processes T. Rassmussen's files
def csvM2dat(filename):
	"""Convert time-matched csv file into hydrological data arrays.

	This function converts a .csv file into four arrays of 
	time, well head, barometric and earth tide.
	The data files that use this function are primarily from 
	Todd Rassmussen's BETCO code.
	
	:param filename: name of csv file with full dataset
	:type filename: string
	:returns: Time, Head, Barometric, and EarthTide arrays
	:rtype: 4 ndarrays of fl64

	.. _fun-csvM2dat:
	"""
	with open(filename) as f:
		lines = f.readlines()
		f.close()
	H = []
	B = []
	ET= []
	T = []
	for line in lines:
		splitline = line.split(",")
		T.append(float(splitline[0]))
		H.append(float(splitline[1]))
		B.append(float(splitline[2]))
		ET.append(float(splitline[3]))
	# convert array
	H = np.array(H)
	B = np.array(B)
	ET= np.array(ET)
	T = np.array(T)
	# END OF FUNCTION
	return T, H, B, ET
###############################################################################

#
#  Dataset loading routines
#
# for T.R. datasets
def loadWipp30():
	"""Load the data from T.R.'s WIPP 30 set

	:returns: See returns for csvM2dat()

	.. _fun-loadWipp30:
	"""
	# file locations
	packdir = os.path.dirname(os.path.abspath(__file__))
	datadir = 'data'
	testdir = 'wipp30'
	# individual files
	Dfile = os.path.join(packdir, datadir, testdir, 'data.csv')
	# assemble the data
	return csvM2dat(Dfile)

def loadKac1():
	"""Load the data from T.R.'s KAC set

	:returns: See returns for csvM2dat()

	.. _fun-loadKac1:
	"""
	# file locations
	packdir = os.path.dirname(os.path.abspath(__file__))
	datadir = 'data'
	testdir = 'kac1'
	# individual files
	Dfile = os.path.join(packdir, datadir, testdir, 'data.csv')
	# assemble the data
	return csvM2dat(Dfile)

# for example data sets
def loadExR1Mo():
	"""Load the data from example well (R34) from Jan. 2013

	:returns: See returns for assembleHydroData()

	.. _fun-loadExR1Mo:
	"""
	# file locations
	packdir = os.path.dirname(os.path.abspath(__file__))
	datadir = 'data'
	testdir = '2013_01-02'
	# individual files
	Hfile = os.path.join(packdir, datadir, testdir, 'well.txt')
	Bfile = os.path.join(packdir, datadir, testdir, 'baro.txt')
	ETfile = os.path.join(packdir, datadir, testdir, 'earthtide.txt')
	# assemble the data
	return assembleHydroData(Hfile, Bfile, ETfile)

def loadExR3Mo():
	"""Load the data from example well (R34) from Nov. 2012-Feb. 2013
	
	:returns: See returns for assembleHydroData()

	.. _fun-loadExR3Mo:
	"""
	# file locations
	packdir = os.path.dirname(os.path.abspath(__file__))
	datadir = 'data'
	testdir = '2012_11-2013_02'
	# individual files
	Hfile = os.path.join(packdir, datadir, testdir, 'well.txt')
	Bfile = os.path.join(packdir, datadir, testdir, 'baro.txt')
	ETfile = os.path.join(packdir, datadir, testdir, 'earthtide.txt')
	# assemble the data
	return assembleHydroData(Hfile, Bfile, ETfile)

# For raw data sets
def loadR34Raw():
	"""Load pre-processed data from example well (R34) from June 2012-Feb. 2014
	
	:returns: See returns for assembleHydroData()

	.. _fun-loadR34Raw:
	"""
	# file locations
	packdir = os.path.dirname(os.path.abspath(__file__))
	datadir = 'data'
	testdir = 'r34raw'
	# individual files
	Hfile = os.path.join(packdir, datadir, testdir, 'well.txt')
	Bfile = os.path.join(packdir, datadir, testdir, 'baro.txt')
	ETfile = os.path.join(packdir, datadir, testdir, 'earthtide.txt')
	# assemble the data
	return assembleHydroData(Hfile, Bfile, ETfile)

def loadSca5Raw():
	"""Load the data from SCA-5 from Feb 2007-July 2007
	
	:returns: See returns for assembleHydroData()

	.. _fun-loadSca5Raw:
	"""
	# file locations
	packdir = os.path.dirname(os.path.abspath(__file__))
	datadir = 'data'
	testdir = 'sca5raw'
	# individual files
	Hfile = os.path.join(packdir, datadir, testdir, 'well.txt')
	Bfile = os.path.join(packdir, datadir, testdir, 'baro.txt')
	ETfile = os.path.join(packdir, datadir, testdir, 'earthtide.txt')
	# assemble the data
	return assembleHydroData(Hfile, Bfile, ETfile)

def loadSci2Raw():
	"""Load the data from SCI-2 from April 2014-May 2014
	
	:returns: See returns for assembleHydroData()

	.. _fun-loadSci2Raw:
	"""
	# file locations
	packdir = os.path.dirname(os.path.abspath(__file__))
	datadir = 'data'
	testdir = 'sci2raw'
	# individual files
	Hfile = os.path.join(packdir, datadir, testdir, 'well.txt')
	Bfile = os.path.join(packdir, datadir, testdir, 'baro.txt')
	ETfile = os.path.join(packdir, datadir, testdir, 'earthtide.txt')
	# assemble the data
	return assembleHydroData(Hfile, Bfile, ETfile)
###############################################################################

#
#  Pickle wrappers
#

def saveObjectToFile(obj, name, loc='results/'):
	""" save dictionary of results to pickle file
	
	No returns; outputs a file
	
	:param obj: object to save
	:type obj: dict (or other type)
	:param name: name of file
	:type name: string
	:param loc: relative location of file
	:type loc: string
	
	.. _fun-saveObjectToFile:
	"""
	with open(loc + name + '.pkl', 'wb') as f:
		pickle.dump(obj, f, pickle.HIGHEST_PROTOCOL)

def loadObjectFromFile(name, loc='results/'):
	""" Load dictionary of results from pickle file
	
	:param name: name of file
	:type name: string
	:param loc: relative location of file
	:type loc: string
	:returns: contents of file
	
	.. _fun-loadObjectFromFile:
	"""
	with open(loc + name + '.pkl', 'r') as f:
		return pickle.load(f)
###############################################################################

#
#  Noisy data functions
#
def makeDataJumpy(dat, sep=1., rep=False, seed=190746937):
	"""Add bimodal jumpy noise to data
	
	This function makes data jumpy.
	The points are either moved up or down by sep/2.
	
	:param dat: Data to modify
	:type dat: ndarray of fl64
	:param sep: width of separation (default=1)
	:type sep: fl64
	:param rep: Should data be reproducible by same seed? (default=False)
	:type rep: bool
	:param seed: User supplied for random number generation
	:type seed: int
	:returns: Data with added noise signals
	:rtype: ndarray of fl64
	
	.. _fun-makeDataJumpy:
	"""
	if rep==True:
		random.seed(seed)
	for i in range(len(dat)):
		dat[i] += sep*(float(random.randint(0,1))-0.5)
	# END OF FUNCTION
	return dat

def makeDataRandNoisy(dat, width=1., rep=False, seed=8847608723):
	"""Add random noise of a given bandwidth to data
	
	This function adds random noise with a specific bandwidth to a data set.
	
	:param dat: Data to modify
	:type dat: ndarray of fl64
	:param width: band width (default=1)
	:type width: fl64
	:param rep: Should data be reproducible by same seed? (default=False)
	:type rep: bool
	:param seed: User supplied for random number generation
	:type seed: int
	:returns: Data with added noise signals
	:rtype: ndarray of fl64
	
	.. _fun-makeDataRandNoisy:
	"""
	if rep==True:
		random.seed(seed)
	for i in range(len(dat)):
		dat[i] += width*(random.random()-0.5)
	# END OF FUNCTION
	return dat

def makeDataGaussNoisy(dat, stdev=1., rep=False, seed=7373608962):
	"""Add Gaussian random noise to data
	
	Add noise with a Gaussian behavior of given standard deviation to a 
	data set.
	
	:param dat: Data to modify
	:type dat: ndarray of fl64
	:param stdev: standard deviation (default=1)
	:type stdev: fl64
	:param rep: Should data be reproducible by same seed? (default=False)
	:type rep: bool
	:param seed: User supplied for random number generation
	:type seed: int
	:returns: Data with added noise signals
	:rtype: ndarray of fl64
	
	.. _fun-makeDataGaussNoisy:
	"""
	if rep==True:
		random.seed(seed)
	for i in range(len(dat)):
		dat[i] += random.gauss(0.,stdev)
	# END OF FUNCTION
	return dat

def makeDataVeryNoisy(dat, sep=0., width=0., stdev=0., rep=False, seeds=[]):
	"""Add all types of random noise to data
	
	This function is an all-in-one wrapper for the noisy data routines.
	For the seeds parameter first seed goes to makeDataJumpy(), second 
	seed to makeDataRandNoisy(), and third seed to makeDataGaussNoisy().
	
	:param dat: Data to modify
	:type dat: ndarray of fl64
	:param sep: Width of separation for makeDataJumpy() (default=0)
	:type sep: fl64
	:param width: Band width for makeDataRandNoisy() (default=0)
	:type width: fl64
	:param stdev: Standard deviation for makeDataGaussNoisy() (default=0)
	:type stdev: fl64
	:param rep: Should data be reproducible by same seed? (default=False)
	:type rep: bool
	:param seeds: Seeds for random number generation [see note] (default=routine defaults)
	:type seeds: list of 3 integers or blank list
	:returns: Data with added noise signals
	:rtype: ndarray of fl64
	
	.. _fun-makeDataVeryNoisy:
	"""
	if seeds==[]:
		dat = makeDataJumpy(dat, sep, rep)
		dat = makeDataRandNoisy(dat, width, rep)
		dat = makeDataGaussNoisy(dat, stdev, rep)
	elif len(seeds)==3:
		dat = makeDataJumpy(dat, sep, rep, seeds[0])
		dat = makeDataRandNoisy(dat, width, rep, seeds[1])
		dat = makeDataGaussNoisy(dat, stdev, rep, seeds[2])
	else:
		print 'Error:  Cannot process random seed options.'
	# END OF FUNCTION
	return dat
###############################################################################

#
# Generate a signal with imposed barometric and earth tide signals
#
def makeDataWithBET(
	dat, 
	alphag=np.array([]), 
	betag=np.array([]), 
	dates=[dt.datetime(2012,7,1,0,0,0),dt.datetime(2013,1,1,0,0,0)], 
	problemdir=[]):
	"""Add earth tide and barometric signals to data
	
	Imposes real earth tide and barometric data onto a data set over a given
	range of times.
	This routine assumes the start and end times supplied by the user will 
	match the starting/ending of the data in dat.
	Note that the magnitude of contribution of B & ET effect to the force
	exerted on the data is very problem dependent, so the effects are not
	encorporated by default.
	
	:param dat: Pressure head data
	:type dat: ndarray of fl64
	:param alphag: Contribution of earth tide effect for generated data
	:type alphag: ndarray of fl64
	:param betag: Contribution of barometric effect for generated data
	:type betag: ndarray of fl64
	:param dates: List of Start and End dates/times (default: 7/1/2012-1/1/2013)
	:type dates: two item list of datetimes
	:param dates: Path to directory containing both baro and et files (default: std location)
	:type dates: absolute os path
	:returns: Data with added physical effects
	:rtype: ndarray of fl64
	
	.. _fun-makeDataWithBET:
	"""
	# 
	#   Internal function for adding one physics to data
	#
	def addOnePhysics(dat, greek, dates, Pfile):
		# check if data will be added
		if len(greek)!=0:
			# get appropriate  data
			P = getPhysicalData(Pfile, dates)
			Pbar = np.average(P)
			Pav = copy.copy(P)-Pbar
			# check length match with data
			if len(Pav) != len(dat):
				print 'Error:  not matching length with data!'
				return dat
			# add barometric effect to data
			l = len(greek)
			for i in range(len(dat)):
				j = 0
				while (j<=i) and (j<l):
					dat[i] += greek[j]*Pav[i-j]
					j += 1
		else:
			P = np.zeros(len(dat))
		return dat, P
	#
	#  Setup the files of physical data
	#
	if problemdir==[]:
		# file locations
		packdir = os.path.dirname(os.path.abspath(__file__))
		datadir = 'data'
		barodir = 'baro'
		etdir = 'earth_tide'
		# individual files
		Bfile = os.path.join(packdir, datadir, barodir, 'baro.csv')
		ETfile = os.path.join(packdir, datadir, etdir, 'earthtide.txt')
	else:
		# individual files
		Bfile = os.path.join(problemdir, 'baro.txt')
		ETfile = os.path.join(problemdir, 'earthtide.txt')
	#
	#  Barometric forces
	#
	dat, B = addOnePhysics(dat, alphag, dates, Bfile)
	#
	#  Earth Tide forces
	#
	dat, ET = addOnePhysics(dat, betag, dates, ETfile)
	# END OF FUNCTION	
	return dat, B, ET

def makeSyntheticHydroData(
	timeLoc=[[dt.datetime(2012,12,1,0,0,0), dt.datetime(2013,2,1,0,0,0)], []],
	heightParams=[0, 0],
	pumpingParams=[],
	BETParams=[],
	noiseParams=[]):
	''' Generates synthetic pump test with baro/ET data and noise
	
	This routine is a jack of all trades; master of none.
	
	:param timeLoc: Path to source for Baro and ET (default: std time & location)
	:type timeLoc:  list of date time pair, absolute os path
	:param heightParams: Initial height and slope (default: no height or slope)
	:type heightParams:  list of 2 fl64
	:param pumpingParams: Arguments to chipbeta's getPumpingEffect() (default: none)
	:type pumpingParams:  list
	:param BETParams: generating alpha and beta for barometric and earthtide data (default: none)
	:type BETParams:  list of 2 lists of fl64
	:param noiseParams: Arguments to makeDataVeryNoisy() (default: none)
	:type noiseParams:  list
	:returns: Time, pressure, barometric, earth tide, data statistics
	:rtype: 4 ndarrays of fl64, 1 dict of statistics
	
	.. _fun-makeSyntheticHydroData:
	'''
	# 
	# generate time and height data
	#
	timezero = dt.datetime(2000,1,1,0,0,0)
	# span of time
	dates = timeLoc[0]
	tspan = dates[1]-dates[0]
	tspanH = tspan.days*24 + int(math.floor(tspan.seconds/3600))
	# initial time
	t0dt = dates[0]-timezero
	t0 = t0dt.days*24 + int(math.floor(t0dt.seconds/3600))
	T = np.arange(tspanH) + t0
	# initialize Pressure data for heights
	H = heightParams[0]*np.ones(tspanH) + heightParams[1]*np.arange(tspanH)
	# 
	# add noise to the data
	#
	if noiseParams != []:
		H = makeDataVeryNoisy(H, *noiseParams)
	stdev0 = np.std(H)
	#
	# add barometric and earth tide forces to the data
	#
	if BETParams != []:
		allBets = [BETParams[0], BETParams[1], timeLoc[0], timeLoc[1]]
		H, B, ET = makeDataWithBET(H, *allBets)
	else:
		B = np.zeros(len(T))
		ET = np.zeros(len(T))
	#
	# add pumping effects to data
	#
	if pumpingParams != []:
		if isinstance(pumpingParams[2][0], dt.datetime):
			# change from date time object to times
			Qtime = [np.array(dtList2hours(dates[0], pumpingParams[2]))]
			pumpingParams[2] = Qtime
		# calculate and impose the pumping effect
		H += cb.getPumpingEffect(T, *pumpingParams)
	# 
	# calc data statistics
	#
	stats = {}
	stats['stdev0'] = stdev0
	# END OF FUNCTION
	return T, H, B, ET, stats
###############################################################################

#
#  Function prototyping code
#
#if __name__ == "__main__":
#	

# 
#  end of file
#
