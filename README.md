# CHiPBETA: Correctign pressure head for pumping, barometric, & Earth Tide effects for data-analytics and model-diagnostics applications

CHiPBETA is a [ZEM](https://gitlab.com/zem) module.

CHiPBETA is released under GNU GENERAL PUBLIC LICENSE Version 3.

LANL Copyright Number Assigned: C18076

## Project Description

This project includes _python_-based scripts for analyzing and correcting well water height data for common imposed effects.
In particular barometric pressure changes and earth tide cycles may be corrected out of the water level data by linear deconvolution.
Additionally, pumping drawdowns and basic random or semi-random noises may also be removed the data.
A series of scripts are included which use CHiPBETA as an engine to test the sensitivity of the inverse analysis of pumping tests to random noises using rudimentary methods.

For several examples of CHiPBETA in action, run:

    $ python cbexamples.py

**For full details on the use and capabilities of the CHiPBETA software see the documentation included under "doc/_build/html/index.html".**


## File Structure

### Subdirectories

* data - Directory contains useful data such as barometric and earth tide records around Los Alamos.
* doc - Directory includes the python documentation for the project (generated using sphinx).
* sensitivity_tests - Directory holds scripts for performing pumping sensitivity tests.

### Files

* README.md - _this_ file.
* cbexamples.py - Several basic examples of the use of primary CHiPBETA functions.
* cbrun.py - The main script used to run working project tests.
* cbtest.py - Functions to verify that chipbeta functions are performing correctly.
* chipbeta.py - **Collection of primary analysis functions.**
* dataproc.py - Functions for data import/export.


## Authors

* Eric M. Benner
* Dan O'Malley
* Velimir V (monty) Vesselinov
