"""Primary script for in-development data processing 

Authors: **Eric M. Benner and Daniel O'Malley**

Date:    7/24/14

Updated: 8/1/14
"""

"""
TODO: Software License?
"""

import chipbeta as cb
import dataproc as dp
import matplotlib.pyplot as plt
import numpy as np

#
#  function testing code
#

# Code main settings
# smoothing level
smoothing = 3
# number of bins
nbins = 50
# pumping on?
doPumping = True
#doPumping = False

# Load the data
#T, H, B, ET = dp.loadKac1()
#T, H, B, ET = dp.loadWipp30()
T, H, B, ET = dp.loadExR1Mo()
#T, H, B, ET = dp.loadExR3Mo()
#T, H, B, ET = dp.loadR34Raw()
#T, H, B, ET = dp.loadSca5Raw()
#T, H, B, ET = dp.loadSci2Raw()

if doPumping:
	# set data
	minT = min(T)
	maxT = max(T)
	Q = [np.array([2000., 0.])]
	Qtime = [np.array([.66 * minT + .34 * maxT, .34 * minT + .66 * maxT])]
	r = [np.array(100.)]
	# params
	stor = 0.01
	trans = 1000.
	ppar = np.array([stor, trans])
	H += cb.getPumpingEffect(T, Q, r, Qtime, ppar)
	ppar0 = np.array([0.001, 100.])
else:
	Q = []
	Qtime = []
	r = []
	ppar0 = []

# parameter settings
alpham = 7
betam = 7
mab = max(alpham, betam)

# Post-smoothed data
# calc initial alpha beta
alpha0, beta0 = cb.getAlphaBet(H, B, ET, alpham, betam)
# get best parameters
alpha, beta, ppar = cb.getBestParams(T, H, B, ET, Q, r, Qtime, alpha0, beta0, ppar0)
#alpha1, beta1, ppar = cb.getBestParams(T, H, B, 0*ET, [], [], [], [0.], [0.], [])
print "  Best params:"
print "  Baro  params: ", alpha
print "  Tidal params: ", beta
print "  T,S", ppar
# correct the data
correctedH = cb.correctHead(T, H, B, ET, Q, r, Qtime, alpha, beta, ppar)
#correctedH1 = cb.correctHead(T, H, B, 0*ET, [], [], [], alpha1, beta1, [])
# (no earth tide correction)
"""
Q = [np.array([200., 0.])]
Qtime = [np.array([.66 * minT + .34 * maxT, .34 * minT + .66 * maxT])]
r = [np.array(100.)]
"""
correctedH2 = cb.correctHead(T, H, B, 0*ET, Q, r, Qtime, alpha, beta, ppar)
#smoothH = cb.smooth(correctedH, smoothing)

# Plot the core data
plt.figure()
plt.title("Data over time")
plt.plot(T, H, 'r+', label="raw data")
plt.plot(T[mab + 1:], correctedH2, 'm+', label="barometric")
plt.plot(T[mab + 1:], correctedH, 'b+', label="tide and baro")
#plt.plot(T[3:], correctedH1, 'g+', label="single baro")
#plt.plot(T[mab + 1:], smoothH, 'g', label="smoothed")
plt.xlabel("time (h)")
plt.ylabel("well head (ft)")
plt.legend(loc=2)

# Plot the differential histograms
plt.figure()
plt.title("Local variation of data")
plt.hist(np.diff(H), bins=nbins, color='red', label="raw data")
#plt.hist(np.diff(correctedH2), bins=nbins, color='magenta', label="barometric")
plt.hist(np.diff(correctedH), bins=nbins, color='blue', label="tide and baro")
#plt.hist(np.diff(smoothH), bins=nbins, color='green', label="smoothed")
plt.xlabel("differential well head (ft)")
plt.ylabel("counts")
plt.legend(loc=2)
plt.show()

# Show the figures
plt.show()
