""" Sensitivity prototypes of pumping/Chipbeta 

Authors: **Eric M. Benner**

Date:    10/29/14

Updated: 10/29/14
"""

"""
TODO: Software License?

TODO: convergence failure issues!!!
"""

###############################################################################

# 
#  import packages and settings
#
# path appending
import sys
sys.path.append('../')
# importing
import cbsensitivity as cbs
import chipbeta as cb
import dataproc as dp
import datetime as dt
import math
import matplotlib as mpl
import matplotlib.pyplot as plt
#from matplotlib.dates import MONDAY
#from matplotlib.dates import MonthLocator, WeekdayLocator, DayLocator, DateFormatter
import numpy as np
import os
import scipy.special as spec

#
#  plot settings
#
# font sizes
mpl.rcParams.update({'font.size': 20})
#mpl.rcParams.update({'label.size': 25})
#sub figure label size
sfls = 24
# plot line widths
llw = 3.
mpl.rcParams.update({'lines.linewidth': llw})
cpthick = float(llw)/2.
cpsize = llw*2.
# axes lines
axt = 2
mpl.rcParams.update({'axes.linewidth': axt})
mpl.rcParams.update({'xtick.major.width': axt})
mpl.rcParams.update({'xtick.major.size': axt*4.})
mpl.rcParams.update({'xtick.minor.width': axt/2.})
mpl.rcParams.update({'xtick.minor.size': axt*2.})
mpl.rcParams.update({'ytick.major.width': axt})
mpl.rcParams.update({'ytick.major.size': axt*4.})
mpl.rcParams.update({'ytick.minor.width': axt/2.})
mpl.rcParams.update({'ytick.minor.size': axt*2.})
# legend
mpl.rcParams.update({'legend.fancybox': True})
mpl.rcParams.update({'legend.fontsize': 'medium'})

# location of results files
#RESULTS_FILE_NAME = 'results-test/'
RESULTS_FILE_NAME = 'results/'
###############################################################################
###############################################################################

#
#  Theis Phase diagram functions (for initial guess help)
#
def theisPhase(T):
	"""Compute Theis equation phase diagram

	Calculates dimensionless drawdown from dimensionless time by 
	the dimensionless Theis equation:

	.. math::
	   \\left( \\frac{T}{Q}D \\right) = \\frac{1}{4\\pi} \\mathrm{E}_i
	   \\left( \\frac{1}{4}\\frac{r^2 S}{T t} \\right)

	or
	
	.. math::
	   \\tilde{D} = \\mathrm{Th}(\\tilde{t})
	
	where *D = TD/Q* and *t = Tt/r^2S*.

	"""
	return map(lambda x: -1./(4.*math.pi)*spec.expi(1./4.*1./x), T)

def theisPhaseDiagram():
	"""Plot the phase diagram of the dimensionless Theis equation
	"""
	# basic tabular values
	#Tx = 10**np.array([0,1,2])
	#ddx = theisPhase(Tx)
	#print Tx
	#print ddx

	# compute diagram
	T = 10**np.linspace(0.5, 5., 51)
	dd = theisPhase(T)
	dder = np.diff(dd)/np.diff(T)
	# plot
	plt.figure()
	plt.title("Drawdown phase behavior plot")
	plt.loglog(T, dd, label='drawdown')
	plt.loglog(T[1:],dder, label='derivative')
	plt.xlabel("dimensionless time (-) "+r'$\frac{T t}{r^2 S}$')
	plt.ylabel("dimensionless drawdown (-) "+r'$\frac{T D}{Q}$')
	plt.legend(loc=4)
	plt.show()
	# END OF EXAMPLE
	return 0

#
#  Show Theis equation and parameter variation for realistic data
#
def plotTheisSTLogx():
	"""Plot of variation of parameters in the Theis Equation
	
	Example plots the variation of the Theis curve for changing values
	of *S*, and *T*.  
	This is helpful to illustrate the usefullness of the Jacob approximation
	This function has no inputs or outputs.
	
	"""
	#printExInitMessage("Running Theis Parameter Example")
	#
	# Initializing data
	#
	# time lengths
	day = 24
	mo = 30*day
	mof = float(mo)
	# set data
	Q = np.array([200., 0.])
	Qtime = np.array([1.*mof, 2.*mof])
	r = .5*5280.;
	# set parameters
	S = 0.01;
	T = 1000.;
	params = [S, T]
	# set time vector
	t = np.linspace(0., 6.*mof, 6*mo)

	# 
	#  S test
	#
	parU = [S*2, T]
	parD = [S*0.5, T]
	parU2 = [S*5, T]
	parD2 = [S*0.2, T]
	#
	dd = map(lambda t: cb.drawdownVariablePumping(t, Q, r, Qtime, params), t)
	ddU= map(lambda t: cb.drawdownVariablePumping(t, Q, r, Qtime, parU), t)
	ddU2=map(lambda t: cb.drawdownVariablePumping(t, Q, r, Qtime, parU2), t)
	ddD= map(lambda t: cb.drawdownVariablePumping(t, Q, r, Qtime, parD), t)
	ddD2=map(lambda t: cb.drawdownVariablePumping(t, Q, r, Qtime, parD2), t)
	# plotting
	#plt.figure()
	plt.subplot(221)
	plt.title(r"$S$")
	plt.plot(t, dd)
	plt.plot(t, ddU)
	plt.plot(t, ddD)
	plt.plot(t, ddU2)
	plt.plot(t, ddD2)
	plt.xlabel("time (h)")
	plt.ylabel("drawdown (ft)")
	# logarithmic plot
	#plt.figure()
	plt.subplot(223)
	plt.title(r"$S$")
	plt.semilogx(t[mo:]-t[mo], dd[mo:])
	plt.semilogx(t[mo:]-t[mo], ddU[mo:])
	plt.semilogx(t[mo:]-t[mo], ddD[mo:])
	plt.semilogx(t[mo:]-t[mo], ddU2[mo:])
	plt.semilogx(t[mo:]-t[mo], ddD2[mo:])
	plt.xlabel("time (h)")
	plt.ylabel("drawdown (ft)")

	#
	#  T test
	#
	parU = [S, T*2]
	parD = [S, T*0.5]
	parU2 = [S, T*5]
	parD2 = [S, T*0.2]
	#
	ddU= map(lambda t: cb.drawdownVariablePumping(t, Q, r, Qtime, parU), t)
	ddU2=map(lambda t: cb.drawdownVariablePumping(t, Q, r, Qtime, parU2), t)
	ddD= map(lambda t: cb.drawdownVariablePumping(t, Q, r, Qtime, parD), t)
	ddD2=map(lambda t: cb.drawdownVariablePumping(t, Q, r, Qtime, parD2), t)
	# plotting
	#plt.figure()
	plt.subplot(222)
	plt.title(r"$T$")
	plt.plot(t, dd)
	plt.plot(t, ddU)
	plt.plot(t, ddD)
	plt.plot(t, ddU2)
	plt.plot(t, ddD2)
	plt.xlabel("time (h)")
	plt.ylabel("drawdown (ft)")
	# logarithmic plot
	#plt.figure()
	plt.subplot(224)
	plt.title(r"$T$")
	plt.semilogx(t[mo:]-t[mo], dd[mo:])
	plt.semilogx(t[mo:]-t[mo], ddU[mo:])
	plt.semilogx(t[mo:]-t[mo], ddD[mo:])
	plt.semilogx(t[mo:]-t[mo], ddU2[mo:])
	plt.semilogx(t[mo:]-t[mo], ddD2[mo:])
	plt.xlabel("time (h)")
	plt.ylabel("drawdown (ft)")

	# display all figures
	plt.show()
	# END OF EXAMPLE
	return 0

def plotNoisyTheis():
	"""Illustrate drawdowns with significant noise
	
	This example plots the variation of the Theis curve for changing values
	of *S*, and *T*.  
	The data has a significantly noisy data set imposed over the
	pumping signal.
	This function has no inputs or outputs.
	
	"""
	#printExInitMessage("Running Theis Parameter Example")
	#
	# Initializing data
	#
	# time lengths
	day = 24
	mo = 30*day
	mof = float(mo)
	# set data
	Q = np.array([200., 0.])
	Qtime = np.array([1.*mof, 2.*mof])
	r = .5*5280.;
	# set parameters
	S = 0.01;
	T = 1000.;
	#params = [S, T]
	# set time vector
	t = np.linspace(0., 6.*mof, 6*mo)
	#
	#  Produce noisy data source
	#
	# specs
	sep = 0.07
	width = 0.025
	stdev = 0.004
	rep = False
	# calc data
	HWell = 5600
	datN = dp.makeDataVeryNoisy(HWell*np.ones(len(t)), sep, width, stdev, rep)
	#
	#  Spectrum test
	#
	# variable parameters
	n = 4
	Si = S*10**np.linspace(-1,1,n+1)
	Ti = T*10**np.linspace(-1,1,n+1)
	# 
	#  Variation of S
	#
	DDS = []
	for i in range(len(Si)):
		pari = [Si[i], T]
		dati = np.array(map(lambda t: cb.drawdownVariablePumping(t, Q, r, Qtime, pari), t))
		DDS.append([datN-dati, pari])
	# plot
	plt.figure()
	plt.subplot(221)
	plt.title(r"$S$")
	for i in range(len(Si)):
		plt.plot(t, DDS[i][0], marker=',', label='S='+str(DDS[i][1][0]))
	plt.xlabel("time (h)")
	plt.ylabel("drawdown (ft)")
	plt.legend(loc=4)
	# logarithm tests
	plt.subplot(223)
	plt.title(r"$S$")
	for i in range(len(Si)):
		plt.semilogx(t[mo:]-t[mo], DDS[i][0][mo:], marker=',', label='S='+str(DDS[i][1][0]))
	plt.xlabel("time (h)")
	plt.ylabel("drawdown (ft)")
	plt.legend(loc=3)
	# 
	#  Variation of T 
	#
	DDT = []
	for i in range(len(Ti)):
		pari = [S, Ti[i]]
		dati = np.array(map(lambda t: cb.drawdownVariablePumping(t, Q, r, Qtime, pari), t))
		DDT.append([datN-dati, pari])
	# plot
	plt.subplot(222)
	plt.title(r"$T$")
	for i in range(len(Ti)):
		plt.plot(t, DDT[i][0], marker=',', label='T='+str(DDT[i][1][1]))
	plt.xlabel("time (h)")
	plt.ylabel("drawdown (ft)")
	plt.legend(loc=4)
	# logarithm tests
	plt.subplot(224)
	plt.title(r"$T$")
	for i in range(len(Ti)):
		plt.semilogx(t[mo:]-t[mo], DDT[i][0][mo:], marker=',', label='T='+str(DDT[i][1][1]))
	plt.xlabel("time (h)")
	plt.ylabel("drawdown (ft)")
	plt.legend(loc=3)

	# display all figures
	plt.show()

	# END OF EXAMPLE
	return 0
###############################################################################
###############################################################################

#
#  Illustrate Basic Generated Pumping test
#
def simplePumpingTest():
	"""
	"""
	#nbins = 50
	# Load the data
	#T, H, B, ET = dp.loadExR3Mo()

	#
	#  Produce noisy data source
	#
	# time lengths
	day = 24
	mo = 30*day
	mof = float(mo)
	tlen = 3
	T = np.linspace(0., tlen*mof, tlen*mo)
	# specs
	sep = 0.07
	#sep = 0	
	width = 0.025
	gsdev = 0.004
	rep = False
	# calc data
	HWell = 5600
	H = dp.makeDataVeryNoisy(HWell*np.ones(len(T)), sep, width, gsdev, rep)
	stdev0 = np.std(H)

	#
	#  Pumping Specs
	#
	# set data
	minT = min(T)
	maxT = max(T)
	Q = [np.array([200., 0.])]
	Qtime = [np.array([.66 * minT + .34 * maxT, .34 * minT + .66 * maxT])]
	r = [np.array(.5*5280.0)]
	# params
	stor = 0.01
	trans = 1000.
	pparS = np.array([stor, trans])
	Hpump = cb.getPumpingEffect(T, Q, r, Qtime, pparS)
	H += Hpump
	ppar0 = np.array([0.001, 100.])

	#
	#  Optimization
	#
	# parameter settings
	alpham = 1
	betam = 1
	mab = max(alpham, betam)
	B = np.zeros(len(T))
	ET = np.zeros(len(T))

	# Post-smoothed data
	# calc initial alpha beta
	alpha0, beta0 = cb.getAlphaBet(H, B, ET, alpham, betam)
	ppar0 = cb.guessPumpingParams(T, H, Q[0], r[0], Qtime[0])
	# get best parameters
	alpha, beta, ppar = cb.getBestParams(T, H, B, ET, Q, r, Qtime, alpha0, beta0, ppar0,'bimodal')
	print "Best params:"
	print alpha
	print beta
	print ppar
	# correct the data
	correctedH = cb.correctHead(T, H, B, ET, Q, r, Qtime, alpha, beta, ppar)
	HpumpCorr = cb.getPumpingEffect(T, Q, r, Qtime, ppar)
	smoothH = cb.correctBimodalData(correctedH)

	#
	#  Error Processing
	#
	# error analysis
	Serr = abs(pparS[0]-ppar[0])/pparS[0]
	Terr = abs(pparS[1]-ppar[1])/pparS[1]
	# print out
	print 'True pumping [S,T]: '+str(pparS)
	print 'S error: '+str(Serr)
	print 'T error: '+str(Terr)
	# total error
	norm = cb.L2norm(Hpump, HpumpCorr)
	print 'norm: '+str(norm)
	# avg drawdown
	ddttrue = cb.averageDrawDown(T, Hpump, Qtime)
	print 'avg drawdown: '+str(ddttrue)
	# residual
	resS = cb.getPumpingResidual(T, H, Q, r, Qtime, pparS)
	res = cb.getPumpingResidual(T, H, Q, r, Qtime, ppar)
	print 'True Residual: '+str(resS)
	print 'Residual: '+str(res)
	print 'Stdev in orig data: '+str(stdev0)

	#
	#  Plotting
	#
	# Plot the core data
	dsep = 0.05
	fig, ax = plt.subplots()
	#plt.title("Data over time")
	ax.plot(T, H, 'r+', label="raw data")
	ax.plot(T, Hpump+H[0]-2.*dsep, color='black', ls='-', marker=',', label="true pump effect")
	ax.plot(T, HpumpCorr+H[0]-2.*dsep, color='gray', ls='-', marker=',', label="corr pump effect")
	ax.plot(T[mab + 1:], correctedH, 'b+', label="tide and baro")
	ax.plot(T[mab + 1:], smoothH, 'g', label="smoothed")
	# Labeling/legend
	plt.xlabel("time (h)")
	plt.ylabel("well head (ft)")
	plt.legend(loc=3)
	
	# Plot the differential histograms
#	plt.figure()
#	plt.title("Variation of neighbor data")
#	plt.hist(np.diff(H), bins=nbins, color='red', label="raw data")
#	plt.hist(np.diff(correctedH), bins=nbins, color='blue', label="tide and baro")
#	plt.hist(np.diff(smoothH), bins=nbins, color='green', label="smoothed")
#	plt.xlabel("differential well head (ft)")
#	plt.ylabel("counts")
#	plt.legend(loc=2)
#	plt.show()
#	
	# Show the figures
	plt.show()
	
	# END OF EXAMPLE
	return 0

#
#  Illustrate Basic Generated Baro ET & Pumping test
#
def fullSyntheticTest():
	"""
	"""
	nbins = 50
	# Load the data
	#T, H, B, ET = dp.loadExR3Mo()
	
	dates=[dt.datetime(2012,12,1,0,0,0), dt.datetime(2013,2,1,0,0,0)]	
	# physics parameters
	alphag = 0.01*np.array([-0.15, -0.20, -0.10, -0.05])
	betag = 0.001*np.array([-0.015, 0.020, -0.010])
	#betParams = [alphag, betag]
	# where to load data from
	packdir = os.path.dirname(os.path.abspath(__file__))
	datadir = 'data'
	testdir = '2012_11-2013_02'
	pdir = os.path.join(packdir, datadir, testdir)
	#pdir = []
	allBets = [alphag, betag, dates, pdir]
	
	# time lengths
	tspan = dates[1]-dates[0]
	tspanH = tspan.days*24 + int(math.floor(tspan.seconds/3600))
	T = np.arange(tspanH)
	# specs
	sep = 0.07
	width = 0.025
	gsdev = 0.004
	rep = False
	noiseParams = [sep, width, gsdev, rep]
	#noiseParams = []
	# calc data
	H0 = 5600
	Hslope = 0#-0.00001
	heightParams = [H0, Hslope]
	H = heightParams[0]*np.ones(tspanH) + heightParams[1]*np.arange(tspanH)
	# add in noise
	H = dp.makeDataVeryNoisy(H, *noiseParams)
	stdev0 = np.std(H)
	# add in barometric and Earth tide
	H, B, ET = dp.makeDataWithBET(H, *allBets)

	#
	#  Pumping Specs
	#
	# set data
	#minT = min(T)
	#maxT = max(T)
	#Qtime = [np.array([.66 * minT + .34 * maxT, .34 * minT + .66 * maxT])]
	Q = [np.array([200., 0.])]
	Qdates = [dt.datetime(2012,12,20,0,0,0), dt.datetime(2013,1,10,0,0,0)]
	dtQ0 = Qdates[0]-dates[0]
	Qt0 = dtQ0.days*24 + int(math.floor(dtQ0.seconds/3600))
	dtQ1 = Qdates[1]-dates[0]
	Qt1 = dtQ1.days*24 + int(math.floor(dtQ1.seconds/3600))
	Qtime = [np.array([Qt0,Qt1])]	
	r = [np.array(.5*5280.0)]
	# params
	stor = 0.01
	trans = 1000.
	pparS = np.array([stor, trans])
	pumpParams = [Q, r, Qtime, pparS]
	Hpump = cb.getPumpingEffect(T, *pumpParams)
	H += Hpump

	#
	#  Optimization
	#
	# parameter settings
	alpham = 7
	betam = 7
	mab = max(alpham, betam)

	# Post-smoothed data
	# calc initial alpha beta
	alpha0, beta0 = cb.getAlphaBet(H, B, ET, alpham, betam)
	ppar0 = cb.guessPumpingParams(T, H, Q[0], r[0], Qtime[0])
	# get best parameters
	alpha, beta, ppar = cb.getBestParams(T, H, B, ET, Q, r, Qtime, alpha0, beta0, ppar0,'bimodal')
	print "Best params:"
	print alpha
	print beta
	print ppar
	# correct the data
	correctedH = cb.correctHead(T, H, B, ET, Q, r, Qtime, alpha, beta, ppar)
	HpumpCorr = cb.getPumpingEffect(T, Q, r, Qtime, ppar)
	smoothH = cb.correctBimodalData(correctedH)

	#
	#  Error Processing
	#
	# error analysis
	Serr = abs(pparS[0]-ppar[0])/pparS[0]
	Terr = abs(pparS[1]-ppar[1])/pparS[1]
	# print out
	print 'True pumping [S,T]: '+str(pparS)
	print 'S error: '+str(Serr)
	print 'T error: '+str(Terr)
	# total error
	norm = cb.L2norm(Hpump, HpumpCorr)
	print 'norm: '+str(norm)
	# avg drawdown
	ddttrue = cb.averageDrawDown(T, Hpump, Qtime)
	print 'avg drawdown: '+str(ddttrue)
	# residual
	resS = cb.getPumpingResidual(T, H, Q, r, Qtime, pparS)
	res = cb.getPumpingResidual(T, H, Q, r, Qtime, ppar)
	print 'True Residual: '+str(resS)
	print 'Residual: '+str(res)
	print 'Stdev in orig data: '+str(stdev0)

	#
	#  Plotting
	#
	# Plot the core data
	dsep = 0.05
	fig, ax = plt.subplots()
	#plt.title("Data over time")
	ax.plot(T, H, 'r+', label="raw data")
	ax.plot(T, Hpump+H[0]-2.*dsep, color='black', ls='-', marker=',', label="true pump effect")
	ax.plot(T, HpumpCorr+H[0]-2.*dsep, color='gray', ls='-', marker=',', label="corr pump effect")
	ax.plot(T[mab + 1:], correctedH, 'b+', label="tide and baro")
	ax.plot(T[mab + 1:], smoothH, 'g', label="smoothed")
	# Labeling/legend
	plt.xlabel("time (h)")
	plt.ylabel("well head (ft)")
	plt.legend(loc=3)
	
	# Plot the differential histograms
	plt.figure()
	plt.title("Variation of neighbor data")
	plt.hist(np.diff(H), bins=nbins, color='red', label="raw data")
	plt.hist(np.diff(correctedH), bins=nbins, color='blue', label="tide and baro")
	plt.hist(np.diff(smoothH), bins=nbins, color='green', label="smoothed")
	plt.xlabel("differential well head (ft)")
	plt.ylabel("counts")
	plt.legend(loc=2)
	plt.show()
	
	# Show the figures
	plt.show()
	
	# END OF EXAMPLE
	return 0

#
#  Use single routine do generate synthetic data test
#
def newSyntheticTest():
	"""
	"""
	# Load the data
	#T, H, B, ET = dp.loadExR3Mo()
	
	#
	#  Time and Location specifications
	#
	dates=[dt.datetime(2012,12,1,0,0,0), dt.datetime(2013,2,1,0,0,0)]
	# where to load data from
	#pdir = os.path.join(os.path.dirname(os.path.abspath(__file__)), 'data', '2012_11-2013_02')
	pdir = []
	# create list
	timeLoc = [dates, pdir]
	#
	#  Height specifications
	#
	H0 = 5600
	#Hslope = -0.00001
	Hslope = 0
	# create list
	heightParams = [H0, Hslope]
	#
	#  Pumping Specs
	#
	Q = [np.array([200., 0.])]
	Qdates = [dt.datetime(2012,12,20,0,0,0), dt.datetime(2013,1,10,0,0,0)]
	# convert times to hours
	Qtime = [np.array(dp.dtList2hours(dates[0], Qdates))]
	r = [np.array(.5*5280.0)]
	# aquifer parameters
	stor = 0.01
	trans = 1000.
	pparS = np.array([stor, trans])
	# create list
	pumpingParams = [Q, r, Qtime, pparS]
	# 
	#  B&ET regression parameters
	#
	alphag = 0.01*np.array([-0.15, -0.20, -0.10, -0.05])
	betag = 0.001*np.array([-0.015, 0.020, -0.010])
	# create list
	betParams = [alphag, betag]
	#
	#  Noise specifications
	#
	sep = 0.07
	width = 0.025
	gsdev = 0.004
	rep = False
	# create list
	noiseParams = [sep, width, gsdev, rep]
	#noiseParams = []
	#
	#  Generate data
	#
	T, H, B, ET, stats = dp.makeSyntheticHydroData(timeLoc, heightParams, pumpingParams, betParams, noiseParams)	

	# smoothing type
	stype = 'bimodal'

	#
	#  Optimization
	#
	# parameter settings
	alpham = 7
	betam = 7
	mab = max(alpham, betam)
	#
	# calc initial alpha beta
	alpha0, beta0 = cb.getAlphaBet(H, B, ET, alpham, betam)
	# calc initial pumping
	ppar0 = cb.guessPumpingParams(T, H, Q[0], r[0], Qtime[0])
	# get best parameters
	alpha, beta, ppar = cb.getBestParams(T, H, B, ET, Q, r, Qtime, alpha0, beta0, ppar0, stype)
	
	#
	#  Processed data
	#
	# correct the data
	correctedH = cb.correctHead(T, H, B, ET, Q, r, Qtime, alpha, beta, ppar)
	# smooth the data
	smoother = cb.smoothingControl(stype)
	smoothH = smoother(correctedH)
	# generate original pumping effect
	Hpump = cb.getPumpingEffect(T, *pumpingParams)
	# generate corrected pumping effect
	HpumpCorr = cb.getPumpingEffect(T, Q, r, Qtime, ppar)
	
	#
	#  Error Processing
	#
	# error analysis
	Serr = abs(pparS[0]-ppar[0])/pparS[0]
	Terr = abs(pparS[1]-ppar[1])/pparS[1]
	# total error
	norm = cb.L2norm(Hpump, HpumpCorr)
	# avg drawdown
	ddttrue = cb.averageDrawDown(T, Hpump, Qtime)
	# residual
	resS = cb.getPumpingResidual(T, H, Q, r, Qtime, pparS)
	res = cb.getPumpingResidual(T, H, Q, r, Qtime, ppar)
	# standard deviation
	stdev0 = stats['stdev0']
	# print out

	# 
	#  Save all data to hash table
	#
	# problem data saver:
	pds = {}
	# initialized data
	indat = {}
	indat['testDates'] = timeLoc[0]
	indat['sourceData'] = timeLoc[1]
	indat['heightParameters'] = heightParams
	indat['pumpingAquiferParameters'] = pumpingParams
	indat['BETParameters'] = betParams
	indat['noiseParameters'] = noiseParams
	indat['smoothingOptions'] = stype
	pds['initializationData'] = indat
	# datasets
	pds['rawData'] = {'Time':T, 'Head':H, 'Baro':B, 'EarthTide':ET}
	# optimization data
	pds['guessedParameters'] = [alpha0, beta0, ppar0]
	pds['optimizedParameters'] = [alpha, beta, ppar]
	# generated data
	pds['generatedData'] = {'correctedHead':correctedH, 'smoothedH':smoothH, 
		'truePumping':Hpump, 'optimizedPumping':HpumpCorr}
	pds['pumpingError'] = {'errorInS':Serr,
		'errorInT':Terr, 'drawdownNorm':norm, 'averageDrawdown':ddttrue,
		'trueResidual': resS, 'optResidual':res, 
		'noiseStandardDeviation':stats['stdev0']}
	
	#
	#  Output Optimization Results
	#
	print "\nBest params:"
	print "  Baro  params: ", alpha
	print "  Tidal params: ", beta
	print "  Opt  [S, T]: ", ppar
	print '  True [S, T]: ', pparS
	
	#
	#  Output error analysis
	#
	print '\nError analysis:'
	print '  Err  [S, T]: ',[Serr, Terr]
	print '  Pumping signal norm: '+str(norm)
	print '  Avg drawdown: '+str(ddttrue)
	print '  True Residual: '+str(resS)
	print '  Optimized Residual: '+str(res)
	print '  Standard deviation in orig data: '+str(stdev0)
	
	#
	#  Plotting
	#
	# Plot the core data
	dsep = 0.05
	fig, ax = plt.subplots()
	#plt.title("Data over time")
	ax.plot(T, H, 'r+', label="raw data")
	ax.plot(T, Hpump+H[0]-2.*dsep, color='black', ls='-', marker=',', label="true pump effect")
	ax.plot(T, HpumpCorr+H[0]-2.*dsep, color='gray', ls='-', marker=',', label="corr pump effect")
	ax.plot(T[mab + 1:], correctedH, 'b+', label="tide and baro")
	ax.plot(T[mab + 1:], smoothH, 'g', label="smoothed")
	# Labeling/legend
	plt.xlabel("time (h)")
	plt.ylabel("well head (ft)")
	plt.legend(loc=3)
	
	# Plot the differential histograms
	plt.figure()
	nbins = 50
	plt.title("Variation of neighbor data")
	plt.hist(np.diff(H), bins=nbins, color='red', label="raw data")
	plt.hist(np.diff(correctedH), bins=nbins, color='blue', label="tide and baro")
	plt.hist(np.diff(smoothH), bins=nbins, color='green', label="smoothed")
	plt.xlabel("differential well head (ft)")
	plt.ylabel("counts")
	plt.legend(loc=2)
	plt.show()
	
	# Show the figures
	plt.show()
	
	# END OF EXAMPLE
	return 0
###############################################################################
###############################################################################

#
# Saving tests
#
def runNSaveSingleTest( datafile='res1'):
	# Load the data
	#T, H, B, ET = dp.loadExR3Mo()
	
	#
	#  Time and Location specifications
	#
	dates=[dt.datetime(2012,12,1,0,0,0), dt.datetime(2013,2,1,0,0,0)]
	# where to load data from
	#pdir = os.path.join(os.path.dirname(os.path.abspath(__file__)), 'data', '2012_11-2013_02')
	pdir = []
	# create list
	timeLoc = [dates, pdir]
	#
	#  Height specifications
	#
	H0 = 5600
	#Hslope = -0.00001
	Hslope = 0
	# create list
	heightParams = [H0, Hslope]
	#
	#  Pumping Specs
	#
	Q = [np.array([200., 0.])]
	Qdates = [dt.datetime(2012,12,20,0,0,0), dt.datetime(2013,1,10,0,0,0)]
	# convert times to hours
	Qtime = [np.array(dp.dtList2hours(dates[0], Qdates))]
	r = [np.array(.5*5280.0)]
	# aquifer parameters
	stor = 0.01
	trans = 1000.
	pparS = np.array([stor, trans])
	# create list
	pumpingParams = [Q, r, Qtime, pparS]
	# 
	#  B&ET regression parameters
	#
	alphag = 0.01*np.array([-0.15, -0.20, -0.10, -0.05])
	betag = 0.001*np.array([-0.015, 0.020, -0.010])
	# create list
	betParams = [alphag, betag]
	#
	#  Noise specifications
	#
	sep = 0.07
	width = 0.025
	gsdev = 0.004
	rep = False
	# create list
	noiseParams = [sep, width, gsdev, rep]
	#noiseParams = []
	#
	#  Smoothing correction type
	#
	stype = 'bimodal'
	#
	#  alpha/beta numbers
	#
	alpham = 7
	betam = 7
	betn = [alpham, betam]
	
	#
	#  Do everything and return all the results in one structure!
	#
	pds = cbs.doOnePumpTest(timeLoc, heightParams, pumpingParams, betParams, noiseParams, betn, stype)
	pds['title'] = 'simple test'
	
	#
	#  Results
	#
	cbs.printTestResults(pds)
	dp.saveObjectToFile(pds, datafile )
	
	# END OF EXAMPLE
	return 0

def readNPlotSingleTestData(datafile='res1'):
	pds = dp.loadObjectFromFile(datafile, RESULTS_FILE_NAME)
	
	#
	#  Get datasets
	#
	# maximum alpha beta length
	mab = max(pds['initializationData']['lenAlphaBeta'])
	# data
	T = pds['rawData']['Time']
	H = pds['rawData']['Head']
	correctedH = pds['generatedData']['correctedHead']
	smoothH = pds['generatedData']['smoothedH']
	Hpump = pds['generatedData']['truePumping']
	HpumpCorr = pds['generatedData']['optimizedPumping']
	
	#
	#  Plotting
	#
	# Plot the core data
	dsep = 0.05
	fig, ax = plt.subplots()
	#plt.title("Data over time")
	ax.plot(T, H, 'r+', label="raw data")
	ax.plot(T, Hpump+H[0]-2.*dsep, color='black', ls='-', marker=',', label="true pump effect")
	ax.plot(T, HpumpCorr+H[0]-2.*dsep, color='gray', ls='-', marker=',', label="corr pump effect")
	ax.plot(T[mab + 1:], correctedH, 'b+', label="tide and baro")
	ax.plot(T[mab + 1:], smoothH, 'g', label="smoothed")
	# Labeling/legend
	plt.xlabel("time (h)")
	plt.ylabel("well head (ft)")
	plt.legend(loc=3)
	
	# Plot the differential histograms
	plt.figure()
	nbins = 50
	plt.title("Variation of neighbor data")
	plt.hist(np.diff(H), bins=nbins, color='red', label="raw data")
	plt.hist(np.diff(correctedH), bins=nbins, color='blue', label="tide and baro")
	plt.hist(np.diff(smoothH), bins=nbins, color='green', label="smoothed")
	plt.xlabel("differential well head (ft)")
	plt.ylabel("counts")
	plt.legend(loc=2)
	
	# Show the figures
	plt.show()
	
	# END OF EXAMPLE
	return 0

###############################################################################
###############################################################################

#
#  Prototype Sensitivity Tests
# 

def runNSaveMultiTest( datafile='resM'):
	# Load the data
	#T, H, B, ET = dp.loadExR3Mo()
	
	#
	#  Time and Location specifications
	#
	dates=[dt.datetime(2012,12,1,0,0,0), dt.datetime(2013,2,1,0,0,0)]
	# where to load data from
	#pdir = os.path.join(os.path.dirname(os.path.abspath(__file__)), 'data', '2012_11-2013_02')
	pdir = []
	# create list
	timeLoc = [dates, pdir]
	#
	#  Height specifications
	#
	H0 = 5600
	#Hslope = -0.00001
	Hslope = 0
	# create list
	heightParams = [H0, Hslope]
	#
	#  Pumping Specs
	#
	Q = [np.array([200., 0.])]
	Qdates = [dt.datetime(2012,12,20,0,0,0), dt.datetime(2013,1,10,0,0,0)]
	# convert times to hours
	Qtime = [np.array(dp.dtList2hours(dates[0], Qdates))]
	r = [np.array(.5*5280.0)]
	# aquifer parameters
	stor = 0.01
	trans = 1000.
	pparS = np.array([stor, trans])
	# create list
	pumpingParams = [Q, r, Qtime, pparS]
	# 
	#  B&ET regression parameters
	#
	alphag = 0.01*np.array([-0.15, -0.20, -0.10, -0.05])
	betag = 0.001*np.array([-0.015, 0.020, -0.010])
	# create list
	betParams = [alphag, betag]
	#
	#  Noise specifications
	#
	sep = 0.07
	width = 0.025
	gsdev = 0.004
	rep = False
	# create list
	noiseParams = [sep, width, gsdev, rep]
	#noiseParams = []
	#
	#  Smoothing correction type
	#
	#stype = 'bimodal'
	stype = 'none'
	#
	#  alpha/beta numbers
	#
	alpham = 7
	betam = 7
	betn = [alpham, betam]
	
	#
	#  Do everything and return all the results in one structure!
	#
	# number of tests
	ntests = 5
	apds = {}
	for i in range(ntests):
		pds = cbs.doOnePumpTest(timeLoc, heightParams, pumpingParams, 
			betParams, noiseParams, betn, stype)
		pds['title'] = 'simple test '+str(i)
		cbs.printTestResults(pds)
		# save to full dataset
		testid = 't'+str(i)
		apds[testid] = pds
	
	#
	#  Results
	#
	dp.saveObjectToFile(apds, datafile )
	
	# END OF EXAMPLE
	return 0

def readNPlotManyTestsData(datafile='resM'):
	apds = dp.loadObjectFromFile(datafile, RESULTS_FILE_NAME)
	
	lserr = []
	lterr = []
	lnorm = []
	lsdev = []
	for i in iter(apds):
		# set problem
		pds = apds[i]
		lserr.append(pds['pumpingError']['errorInS'])
		lterr.append(pds['pumpingError']['errorInT'])
		lnorm.append(pds['pumpingError']['drawdownNorm'])
		lsdev.append(pds['pumpingError']['noiseStandardDeviation'])
		#lnorm.append(pds['pumpingError']['averageDrawdown'])
	# print a list of errors
	print 'all S errors: ', lserr
	print '  - avg:      ', np.average(lserr)
	print '  - sdv:      ', np.std(lserr)
	print 'all T errors: ', lterr
	print '  - avg:      ', np.average(lterr)
	print '  - sdv:      ', np.std(lterr)
	print 'all norms:    ', lnorm
	print '  - avg:      ', np.average(lnorm)
	print '  - sdv:      ', np.std(lnorm)
	print 'all std0:     ', lsdev
	print '  - avg:      ', np.average(lsdev)
	print '  - sdv:      ', np.std(lsdev)
	
	plt.figure()
	plt.plot(lsdev, lnorm, 'r+')
	plt.show()
	
	# END OF EXAMPLE
	return 0

#
#  Full tests
#
def runNSaveTests( datafile='resT'):
	# Load the data
	#T, H, B, ET = dp.loadExR3Mo()
	
	#
	#  Time and Location specifications
	#
	dates=[dt.datetime(2012,12,1,0,0,0), dt.datetime(2013,2,1,0,0,0)]
	# where to load data from
	#pdir = os.path.join(os.path.dirname(os.path.abspath(__file__)), 'data', '2012_11-2013_02')
	pdir = []
	# create list
	timeLoc = [dates, pdir]
	#
	#  Height specifications
	#
	H0 = 5600
	#Hslope = -0.00001
	Hslope = 0
	# create list
	heightParams = [H0, Hslope]
	#
	#  Pumping Specs
	#
	Q = [np.array([200., 0.])]
	Qdates = [dt.datetime(2012,12,20,0,0,0), dt.datetime(2013,1,10,0,0,0)]
	# convert times to hours
	Qtime = [np.array(dp.dtList2hours(dates[0], Qdates))]
	r = [np.array(.5*5280.0)]
	# aquifer parameters
	stor = 0.01
	trans = 1000.
	pparS = np.array([stor, trans])
	# create list
	pumpingParams = [Q, r, Qtime, pparS]
	# 
	#  B&ET regression parameters
	#
	#alphag = 0.01*np.array([-0.15, -0.20, -0.10, -0.05])
	#betag = 0.001*np.array([-0.015, 0.020, -0.010])
	# create list
	#betParams = [alphag, betag]
	betParams = []
	#
	#  Noise specifications
	#
	sep = 0.07
	width = 0.025
	gsdev = 0.004
	rep = False
	# create list
	noiseParams = [sep, width, gsdev, rep]
	#noiseParams = []
	#
	#  Smoothing correction type
	#
	#stype = 'smooth'
	#stype = 'smoothbimodal'
	#stype = 'bimodal'
	stype = 'none'
	#
	#  alpha/beta numbers
	#
	#alpham = 7
	#betam = 7
	# create list
	#betn = [alpham, betam]
	betn = []
	
	#
	#  Do everything and return all the results in one structure!
	#
	testtype = 'All noise + pumping'
	# is there saved data out there to add to?
	if os.path.isfile('results/'+datafile+'.pkl'):
		apds = dp.loadObjectFromFile(datafile)
	else :
		apds = {}
	# number of saved tests in dataset
	tID0 = len(apds)
	tIDi = tID0
	# number of samples per sub test
	ntests = 5
	for i in range(ntests):
		pds = cbs.doOnePumpTest(timeLoc, heightParams, pumpingParams, 
			betParams, noiseParams, betn, stype)
		pds['title'] = testtype+' test '+str(i)
		cbs.printTestResults(pds)
		# save to full dataset
		testid = 't'+str(tIDi)
		apds[testid] = pds
		# increment test ID
		tIDi += 1
	
	#
	#  Results
	#
	dp.saveObjectToFile(apds, datafile )
	
	# END OF EXAMPLE
	return 0

def plotTestsData(datafile='resT'):
	"""
	"""
	apds = dp.loadObjectFromFile(datafile, RESULTS_FILE_NAME)
	
	lserr = []
	lterr = []
	lnorm = []
	lsdev = []
	lddav = []
	for i in iter(apds):
		# set problem
		pds = apds[i]
		lserr.append(pds['pumpingError']['errorInS'])
		lterr.append(pds['pumpingError']['errorInT'])
		lnorm.append(pds['pumpingError']['drawdownNorm'])
		lsdev.append(pds['pumpingError']['noiseStandardDeviation'])
		lddav.append(abs(pds['pumpingError']['averageDrawdown']))
	
	# print a list of errors
	cbs.printListStats(lserr, 'all S errors')
	cbs.printListStats(lterr, 'all T errors')
	cbs.printListStats(lnorm, 'all norms')
	cbs.printListStats(lsdev, 'all std0')
	
	# normalize dimensional lists
	lnormn = np.array(lnorm)/np.array(lddav)
	lsdevn = np.array(lsdev)/np.array(lddav)
	lserrn = np.array(lserr)
	lterrn = np.array(lterr)
	
	# Plot the norms
	plt.figure()
	plt.plot(lsdevn, lnormn, 'r+', label='pumping signal norm')
	plt.xlabel("dimensionless standard deviation")
	plt.ylabel("dimensionless drawdown norm")
	plt.legend(loc=2)
	
	# Plot the errors in T and S
	plt.figure()
	plt.loglog(lsdevn, lserrn, 'r+', label='Error in S')
	plt.loglog(lsdevn, lterrn, 'b+', label='Error in T')
	plt.xlabel("dimernsionless standard deviation")
	plt.ylabel("error")
	plt.legend(loc=2)
	
	# Show the figures
	plt.show()
	
	# END OF EXAMPLE
	return 0

###############################################################################
###############################################################################

#
#  function running code
#
if __name__ == "__main__":
	#
	#  Theis equation analysis
	#

	# show a phase diagram of dimensionless theis behavior
	#theisPhaseDiagram()
	#plotTheisSTLogx()
	# Pumping
	#plotNoisyTheis()

	#
	#  All-in-one pump tests with plots
	#

	# Do pumping test with imposed noise
	#simplePumpingTest()
	# pumping test with imposed b&et and noise 
	#fullSyntheticTest()
	# synthetic data pumping test with unified generator 
	#newSyntheticTest()  # TODO: fix this routine!
	
	#
	#  Separated dictionary functions
	#
	# use dict structure for data between two functions
	#runNSaveSingleTest()  # TODO: fix this routine!
	#readNPlotSingleTestData()
	# multiple sensitivity tests
	#runNSaveMultiTest()  # TODO: fix this routine!
	#readNPlotManyTestsData()
	# full featured test
	#runNSaveTests()  # TODO: fix this routine!
	#plotTestsData()

#
#  End of File
#