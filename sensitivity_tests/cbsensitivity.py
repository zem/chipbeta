""" Sensitivity tests (and prototyping) of pumping/Chipbeta 

Authors: **Eric M. Benner**

Date:    8/14/14

Updated: 10/29/14
"""

"""
TODO: Software License?
"""

###############################################################################

# 
#  import packages and settings
#
# path appending
import sys
sys.path.append('../')
# importing
import chipbeta as cb
import dataproc as dp
import datetime as dt
import math
import matplotlib as mpl
import matplotlib.pyplot as plt
#from matplotlib.dates import MONDAY
#from matplotlib.dates import MonthLocator, WeekdayLocator, DayLocator, DateFormatter
import numpy as np
import os
import random
import scipy.special as spec
import time

#
#  plot settings
#
# font sizes
mpl.rcParams.update({'font.size': 20})
#mpl.rcParams.update({'label.size': 25})
#sub figure label size
sfls = 24
# plot line widths
llw = 3.
mpl.rcParams.update({'lines.linewidth': llw})
cpthick = float(llw)/2.
cpsize = llw*2.
# axes lines
axt = 2
mpl.rcParams.update({'axes.linewidth': axt})
mpl.rcParams.update({'xtick.major.width': axt})
mpl.rcParams.update({'xtick.major.size': axt*4.})
mpl.rcParams.update({'xtick.minor.width': axt/2.})
mpl.rcParams.update({'xtick.minor.size': axt*2.})
mpl.rcParams.update({'ytick.major.width': axt})
mpl.rcParams.update({'ytick.major.size': axt*4.})
mpl.rcParams.update({'ytick.minor.width': axt/2.})
mpl.rcParams.update({'ytick.minor.size': axt*2.})
# legend
mpl.rcParams.update({'legend.fancybox': True})
mpl.rcParams.update({'legend.fontsize': 'medium'})
###############################################################################
###############################################################################

#
#  Do a single draw-down test in one routine
#
def doOnePumpTest(timeLoc, heightParams, pumpingParams, betParams, noiseParams, betN, smoothingType='none'):
	"""
	"""
	# TODO: 
	#
	#  Set key data names
	#
	Q = pumpingParams[0]
	r = pumpingParams[1]
	Qtime = pumpingParams[2]
	pparS = pumpingParams[3]
	#
	#  Generate data
	#
	T, H, B, ET, stats = dp.makeSyntheticHydroData(timeLoc, heightParams, pumpingParams, betParams, noiseParams)
	#
	#  Optimization
	#
	# calc initial alpha beta
	alpha0, beta0 = cb.getAlphaBet(H, B, ET, *betN)
	# calc initial pumping
	ppar0 = cb.guessPumpingParams(T, H, Q[0], r[0], Qtime[0])
	# get best parameters
	alpha, beta, ppar = cb.getBestParams(T, H, B, ET, Q, r, Qtime, alpha0, beta0, ppar0, smoothingType)
	HBETsdev = np.std(H)
	
	#
	#  Processed data
	#
	# correct the data
	correctedH = cb.correctHead(T, H, B, ET, Q, r, Qtime, alpha, beta, ppar)
	# smooth the data
	smoother = cb.smoothingControl(smoothingType)
	smoothH = smoother(correctedH)
	# generate original pumping effect
	Hpump = cb.getPumpingEffect(T, *pumpingParams)
	# generate corrected pumping effect
	HpumpCorr = cb.getPumpingEffect(T, Q, r, Qtime, ppar)
	
	#
	#  Error Processing
	#
	# error analysis
	Serr = abs(pparS[0]-ppar[0])/pparS[0]
	Terr = abs(pparS[1]-ppar[1])/pparS[1]
	# total error
	norm = cb.L2norm(Hpump, HpumpCorr)
	# avg drawdown
	ddttrue = cb.averageDrawDown(T, Hpump, Qtime)
	# residual
	resS = cb.getPumpingResidual(T, H, Q, r, Qtime, pparS)
	res = cb.getPumpingResidual(T, H, Q, r, Qtime, ppar)
	# standard deviation
	stdev0 = HBETsdev #stats['stdev0']
	# print out

	# 
	#  Save all data to hash table
	#
	# problem data saver:
	pds = {}
	# initialized data
	indat = {}
	indat['testDates'] = timeLoc[0]
	indat['sourceData'] = timeLoc[1]
	indat['heightParameters'] = heightParams
	indat['pumpingAquiferParameters'] = pumpingParams
	indat['BETParameters'] = betParams
	indat['noiseParameters'] = noiseParams
	indat['smoothingOptions'] = smoothingType
	indat['lenAlphaBeta'] = betN
	pds['initializationData'] = indat
	# datasets
	pds['rawData'] = {'Time':T, 'Head':H, 'Baro':B, 'EarthTide':ET}
	# optimization data
	pds['guessedParameters'] = [alpha0, beta0, ppar0]
	pds['optimizedParameters'] = [alpha, beta, ppar]
	# generated data
	pds['generatedData'] = {'correctedHead':correctedH, 'smoothedH':smoothH, 
		'truePumping':Hpump, 'optimizedPumping':HpumpCorr}
	pds['pumpingError'] = {'errorInS':Serr,
		'errorInT':Terr, 'drawdownNorm':norm, 'averageDrawdown':ddttrue,
		'trueResidual': resS, 'optResidual':res, 
		'noiseStandardDeviation':stdev0}
	# END OF FUNCTION
	return pds

def printTestResults(pds):
	"""
	"""
	#
	#  Print test name
	#
	print '\nNAME OF TEST: ', pds['title']
	#
	#  Output Optimization Results
	#
	print '\nBest params:'
	print '  Baro  params: ', pds['optimizedParameters'][0]
	print '  Tidal params: ', pds['optimizedParameters'][1]
	print '  Opt  [S, T]: ', pds['optimizedParameters'][2]
	print '  True [S, T]: ', pds['initializationData']['pumpingAquiferParameters'][3]
	
	#
	#  Output error analysis
	#
	print '\nError analysis:'
	print '  Err  [S, T]: ', [pds['pumpingError']['errorInS'], pds['pumpingError']['errorInT']]
	print '  Pumping signal norm: ', pds['pumpingError']['drawdownNorm']
	print '  Avg drawdown: ', pds['pumpingError']['averageDrawdown']
	print '  True Residual: ', pds['pumpingError']['trueResidual']
	print '  Optimized Residual: ', pds['pumpingError']['optResidual']
	print '  Standard deviation in orig data: ', pds['pumpingError']['noiseStandardDeviation']
	# END OF FUNCTION
	return 0

###############################################################################

#
#  Data assembly
#

def assembleErrorData(d, criterion='none', verbose=False):
	"""
	add to list only if criterion is satisfied
	"""
	# criterion for selecting data from raw data sets
	if criterion=='bimodal':
		ctype = 'smoothing'
		smoothing = 'bimodal'
	elif criterion=='smooth':
		ctype = 'smoothing'
		smoothing = 'smooth'
	elif criterion=='smoothbimodal':
		ctype = 'smoothing'
		smoothing = 'smoothbimodal'
	elif criterion=='none':
		ctype = 'smoothing'
		smoothing = 'none'
	elif criterion=='simplebaro':
		ctype = 'baroet'
		smoothing = 'none'
		correction = [1,0]
	elif criterion=='baroet7':
		ctype = 'baroet'
		smoothing = 'none'
		correction = [7,7]
	else: # default (also above)
		ctype = 'smoothing'
		smoothing = 'none'
	# TODO: 
	# initialize data lists
	lserr = []
	lterr = []
	lnorm = []
	lsdev = []
	lddav = []
	# add in data for each set
	for i in iter(d):
		# set problem
		pds = d[i]
		# set more data
		if ctype == 'smoothing':
			if pds['initializationData']['smoothingOptions'] == smoothing:
				lserr.append(pds['pumpingError']['errorInS'])
				lterr.append(pds['pumpingError']['errorInT'])
				lnorm.append(pds['pumpingError']['drawdownNorm'])
				lsdev.append(pds['pumpingError']['noiseStandardDeviation'])
				lddav.append(abs(pds['pumpingError']['averageDrawdown']))
		elif ctype == 'baroet':
			if pds['initializationData']['lenAlphaBeta'] == correction:
				lserr.append(pds['pumpingError']['errorInS'])
				lterr.append(pds['pumpingError']['errorInT'])
				lnorm.append(pds['pumpingError']['drawdownNorm'])
				lsdev.append(pds['pumpingError']['noiseStandardDeviation'])
				lddav.append(abs(pds['pumpingError']['averageDrawdown']))
		else:
			pass
	
	# print a list of errors
	if verbose==True:
		printListStats(lserr, 'all S errors')
		printListStats(lterr, 'all T errors')
		printListStats(lnorm, 'all norms')
		printListStats(lsdev, 'all std0')
	# normalize dimensional lists
	lnormn = np.array(lnorm)/np.array(lddav)
	lsdevn = np.array(lsdev)/np.array(lddav)
	lserrn = np.array(lserr)
	lterrn = np.array(lterr)
	# sort the data
	dtype = [('sd', float), ('norm', float), ('serr', float), ('terr', float)]
	smatrix = np.sort(np.array(zip(lsdevn,lnormn,lserrn,lterrn),dtype), order='sd')
	# reget the data
	lsdevn = []
	lnormn = []
	lserrn = []
	lterrn = []
	for i in range(len(smatrix)):
		lsdevn.append(smatrix[i][0])
		lnormn.append(smatrix[i][1])
		lserrn.append(smatrix[i][2])
		lterrn.append(smatrix[i][3])
	lsdevn = np.array(lsdevn)
	lnormn = np.array(lnormn)
	lserrn = np.array(lserrn)
	lterrn = np.array(lterrn)
	# assemble returning data
	derror = {}
	derror['stdev'] = lsdevn
	derror['norm'] = lnormn
	derror['Serror'] = lserrn
	derror['Terror'] = lterrn
	# END OF FUNCTION
	return derror

def printListStats(l, dataname=''):
	""" Print out full list of dataset statistics in given data file
	"""
	if dataname == '':
		print '\n'
	else:
		print '\n'+dataname+': ', l
	print 'Statistics:'
	print '  - avg:      ', np.average(l)
	print '  - med:      ', np.median(l)
	print '  - sdv:      ', np.std(l)
	return 0
###############################################################################

#
#  Binning functions
#

def binStatsDataSet(asort, bsort, binner):
	""" bin list group for dense pre-sorted lists
	
	asort: sorted x data
	bsort: corresponding y data
	binner: bin boundaries
	
	.. _fun-binStatsDataSet:
	"""
	# TODO: Check whether data is sorted?
	#
	#  Find which bin to put data in
	#
	# initialize the list of bin indices
	bini = []
	# set bin index for all data
	bindex = -1
	for i in range(len(asort)):
		ai = asort[i]
		bi = bindex+1
		if bi < len(binner): # for all left of bin edges
			if ai < binner[bi]: # speeds up checks
				bini.append(bindex)
			else: # keeps increments stable
				bindex = -1
				for j in range(len(binner)):
					if ai > binner[j]:
						bindex += 1
				bini.append(bindex)
		else: # if too big for bins
			bini.append(len(binner)-1)
	#
	#  Put data in the bins
	#
	# initialize the bins
	binbox = [[]]
	for i in range(len(binner)):
		binbox.append([])
	# place data sets into bins
	for i in range(len(bini)):
		inder = bini[i]+1
		binbox[inder].append( (asort[i], bsort[i]) )
	# 
	#  Find the data about each bin
	#
	# initialize loop variables
	bstats = []
	nanc = 0.
	for i in range(len(binbox)):
		# initialize 
		boxi = binbox[i]
		ali = []
		bli = []
		# put together list for specific bin
		for j in range(len(boxi)):
			ali.append(boxi[j][0])
			bli.append(boxi[j][1])
			if math.isnan(boxi[j][1]):
				nanc += 1
		# total failures
		if len(boxi) != 0:
			fails = float(nanc)/float(len(boxi))
		else:
			fails = 0.
		# sort the dat based on
		dtype = [('a', float), ('b', float)]
		sbs = np.sort(np.array(zip(ali,bli),dtype), order='a')
		# return to list form
		ansort = []
		bnsort = []
		for i in range(len(sbs)):
			ansort.append(sbs[i][0])
			bnsort.append(sbs[i][1])
		# 
		#  Set the statistics
		#
		lsa = len(sbs)
		if lsa == 0:
			# do nothing
			pass
		elif lsa%2 == 0:
			# median
			i1 = lsa/2-1
			i2 = lsa/2
			#print i1, i2
			mx = np.average([ansort[i1],ansort[i2]])
			my = np.average([bnsort[i1],bnsort[i2]])
			# standard deviation
			w = np.std(ansort)
			sd = np.std(bnsort)
			
			# add to bsats
			bstats.append( [mx, my, w, sd, fails] )
		else:
			# median
			ir = lsa/2
			mx = ansort[ir]
			my = bnsort[ir]
			# standard deviation
			w = np.std(ansort)
			sd = np.std(bnsort)
			# add to bsats
			bstats.append( [mx, my, w, sd, fails] )
		# reset nan count
		nanc = 0.
	# 
	# get new lists
	#
	arep = []
	brep = []
	aerep = []
	berep = []
	pfail = []
	for i in range(len(bstats)):
		arep.append(bstats[i][0])
		brep.append(bstats[i][1])
		aerep.append(bstats[i][2])
		berep.append(bstats[i][3])
		pfail.append(bstats[i][4])
	# set return data
	ds = {'x':arep, 'y':brep, 'ex':aerep, 'ey':berep, 'failrate':pfail}
	return ds

def extractBinStats(d, binner):
	nd = binStatsDataSet(d['stdev'], d['norm'], binner)
	Sd = binStatsDataSet(d['stdev'], d['Serror'], binner)
	Td = binStatsDataSet(d['stdev'], d['Terror'], binner)
	# process the failure rate
	fnd = nd['failrate']
	fSd = Sd['failrate']
	fTd = Td['failrate']
	fall = []
	for i in range(len(fnd)):
		maxfall = max(fnd[i], fSd[i], fTd[i])
		fall.append(maxfall)
	# set the return data
	dstats = {'norm':nd, 'Serror':Sd, 'Terror':Td, 'Failure Rate':fall}
	return dstats

###############################################################################
###############################################################################

#
#  function running code
#
#if __name__ == "__main__":
#	# END OF RUNS

#
#  End of File
#