""" Sensitivity tests (and prototyping) of pumping/Chipbeta 

Authors: **Eric M. Benner**

Date:    8/14/14

Updated: 10/29/14
"""

"""
TODO: Software License?
"""

###############################################################################

# 
#  import packages and settings
#
# path appending
import sys
sys.path.append('../')
# importing
import cbsensitivity as cbs
import chipbeta as cb
import dataproc as dp
import datetime as dt
import matplotlib as mpl
import matplotlib.pyplot as plt
#from matplotlib.dates import MONDAY
#from matplotlib.dates import MonthLocator, WeekdayLocator, DayLocator, DateFormatter
import numpy as np
import os
import random
import time

#
#  plot settings
#
# font sizes
mpl.rcParams.update({'font.size': 20})
#mpl.rcParams.update({'label.size': 25})
#sub figure label size
sfls = 24
# plot line widths
llw = 3.
mpl.rcParams.update({'lines.linewidth': llw})
cpthick = float(llw)/2.
cpsize = llw*2.
# axes lines
axt = 2
mpl.rcParams.update({'axes.linewidth': axt})
mpl.rcParams.update({'xtick.major.width': axt})
mpl.rcParams.update({'xtick.major.size': axt*4.})
mpl.rcParams.update({'xtick.minor.width': axt/2.})
mpl.rcParams.update({'xtick.minor.size': axt*2.})
mpl.rcParams.update({'ytick.major.width': axt})
mpl.rcParams.update({'ytick.major.size': axt*4.})
mpl.rcParams.update({'ytick.minor.width': axt/2.})
mpl.rcParams.update({'ytick.minor.size': axt*2.})
# legend
mpl.rcParams.update({'legend.fancybox': True})
mpl.rcParams.update({'legend.fontsize': 'medium'})
###############################################################################
###############################################################################

#
#  Jumpy tests
#

def runNSaveJumpyTests( datafile='resJumpy'):
	# Load the data
	#T, H, B, ET = dp.loadExR3Mo()
	tstart = time.clock()
	print "Starting runs", tstart, 'seconds'
	#
	#  Time and Location specifications
	#
	dates=[dt.datetime(2012,12,1,0,0,0), dt.datetime(2013,2,1,0,0,0)]
	# where to load data from
	#pdir = os.path.join(os.path.dirname(os.path.abspath(__file__)), 'data', '2012_11-2013_02')
	pdir = []
	# create list
	timeLoc = [dates, pdir]
	#
	#  Height specifications
	#
	H0 = 5600
	#Hslope = -0.00001
	Hslope = 0
	# create list
	heightParams = [H0, Hslope]
	#
	#  Pumping Specs
	#
	Q = [np.array([200., 0.])]
	Qdates = [dt.datetime(2012,12,20,0,0,0), dt.datetime(2013,1,10,0,0,0)]
	# convert times to hours
	timezero = dt.datetime(2000,1,1,0,0,0)
	Qtime = [np.array(dp.dtList2hours(timezero, Qdates))]
	r = [np.array(.5*5280.0)]
	# aquifer parameters
	stor = 0.01
	trans = 1000.
	pparS = np.array([stor, trans])
	# create list
	pumpingParams = [Q, r, Qtime, pparS]
	# 
	#  B&ET regression parameters
	#
	#alphag = 0.01*np.array([-0.15, -0.20, -0.10, -0.05])
	#betag = 0.001*np.array([-0.015, 0.020, -0.010])
	# create list
	#betParams = [alphag, betag]
	betParams = []
	#
	#  Noise specifications
	#
	#sep = 0.07
	#width = 0.025
	#gsdev = 0.004
	
	sep = 0
	lsep = 0.0001*10**(np.arange(25.)/4.)
	width = 0
	gsdev = 0
	rep = False
	# create list
	noiseParams = [sep, width, gsdev, rep]
	#noiseParams = []
	#
	#  Smoothing correction type
	#
	#stype = 'smooth'
	stype = 'smoothbimodal'
	#stype = 'bimodal'
	#stype = 'none'
	lstype = ['none', 'smooth', 'bimodal', 'smoothbimodal']
	#
	#  alpha/beta numbers
	#
	#alpham = 7
	#betam = 7
	# create list
	#betn = [alpham, betam]
	betn = [1,1]
	
	#
	#  Do everything and return all the results in one structure!
	#
	testtype = 'Jumpy noise + pumping'
	# is there saved data out there to add to?
	if os.path.isfile('results/'+datafile+'.pkl'):
		apds = dp.loadObjectFromFile(datafile)
	else :
		apds = {}
	# number of saved tests in dataset
	tID0 = len(apds)
	tIDi = tID0
	# number of samples per sub test
	nstests = 5
	# loop over interested parameters
	for j in range(len(lsep)):
		# set variables to use in test set
		sep = lsep[j]
		noiseParams = [sep, width, gsdev, rep]
		# print out status and time
		print '\nprogress: '+str(float(j)/float(len(lsep))*100.)+'% '
		print 'd var = ',sep
		ti = time.clock()
		print 'elapsed time: '+str((ti - tstart)/60.)+' minutes'
		# loop over types of smoothing
		for k in iter(lstype):
			stype = k
			# do number of subtests in sample set
			for i in range(nstests):
				pds = cbs.cbs.doOnePumpTest(timeLoc, heightParams, pumpingParams, 
					betParams, noiseParams, betn, stype)
				pds['title'] = testtype+' test '+str(tIDi)
				cbs.printTestResults(pds)
				# save to full dataset
				testid = 't'+str(tIDi)
				apds[testid] = pds
				# increment test ID
				tIDi += 1

	#
	#  Results
	#
	dp.saveObjectToFile(apds, datafile )
	
	# tell me it's over
	end = time.clock()
	print 'Total time elapsed: '+str((end - tstart)/60.)+' minutes'
	
	# END OF EXAMPLE
	return 0

def plotJumpyTestsData(datafile='resJumpy'):
	"""
	"""
	apds = dp.loadObjectFromFile(datafile)
	
	nsdata = cbs.assembleErrorData(apds, 'none')
	sdata = cbs.assembleErrorData(apds, 'smooth')
	bmdata = cbs.assembleErrorData(apds, 'bimodal')
	bmsdata = cbs.assembleErrorData(apds, 'smoothbimodal')
	# get the data in binned form
	nsbins = 2
	bins = 10**np.linspace(-3,3,6*nsbins+1)
	# set the data
	nsed = cbs.extractBinStats(nsdata, bins)
	sed = cbs.extractBinStats(sdata, bins)
	bmed = cbs.extractBinStats(bmdata, bins)
	bmsed = cbs.extractBinStats(bmsdata, bins)
	
	#
	#  Plots
	#
	xlimiter = (10**-3, 10**3)
	
	# Plot the norms
	plt.figure()
	plt.loglog(nsdata['stdev'], nsdata['norm'], 'r+', label='regular data')
	plt.loglog(sdata['stdev'], sdata['norm'], 'g+', label='smoothed data')
	plt.loglog(bmdata['stdev'], bmdata['norm'], 'b+', label='bimodal corrected data')
	plt.loglog(bmsdata['stdev'], bmsdata['norm'], 'y+', label='bimodal smoothed data')
	plt.xlim( xlimiter )
	plt.xlabel("dimensionless standard deviation")
	plt.ylabel("dimensionless drawdown norm")
	plt.legend(loc=2)
	
	# Plot the norms by region
	plt.figure()
	plt.loglog()
	plt.errorbar(nsed['norm']['x'], nsed['norm']['y'], yerr=nsed['norm']['ey'], 
		capthick=cpthick, capsize=cpsize, color='red', label='regular data')
	plt.errorbar(sed['norm']['x'], sed['norm']['y'], yerr=sed['norm']['ey'], 
		capthick=cpthick, capsize=cpsize, color='green', label='smoothed data')
	plt.errorbar(bmed['norm']['x'], bmed['norm']['y'], yerr=bmed['norm']['ey'], 
		capthick=cpthick, capsize=cpsize, color='blue', label='bimodal corrected data')
	plt.errorbar(bmsed['norm']['x'], bmsed['norm']['y'], yerr=bmsed['norm']['ey'], 
		capthick=cpthick, capsize=cpsize, color='yellow', label='bimodal smoothed data')
	plt.xlim( xlimiter )
	plt.xlabel("dimensionless standard deviation")
	plt.ylabel("dimensionless drawdown norm")
	plt.legend(loc=2)
	
	# Plot the errors in T and S
	plt.figure()
	plt.loglog(nsdata['stdev'], nsdata['Serror'], 'r+', label='Error in S')
	plt.loglog(nsdata['stdev'], nsdata['Terror'], 'm+', label='Error in T')
	plt.loglog(sdata['stdev'], sdata['Serror'], 'g+', label='Error in S - smoothed')
	plt.loglog(sdata['stdev'], sdata['Terror'], 'c+', label='Error in T - smoothed')
	plt.loglog(bmdata['stdev'], bmdata['Serror'], 'b+', label='Error in S - bimodal')
	plt.loglog(bmdata['stdev'], bmdata['Terror'], 'k+', label='Error in T - bimodal')
	plt.loglog(bmsdata['stdev'], bmsdata['Serror'], 'y+', label='Error in S - bimodal/smooth')
	plt.loglog(bmsdata['stdev'], bmsdata['Terror'], '+', color='orange', label='Error in T - bimodal/smooth')
	plt.xlim( xlimiter )
	plt.xlabel("dimernsionless standard deviation")
	plt.ylabel("error")
	plt.legend(loc=2)
	
	# Plot the errors in T and S by region
	plt.figure()
	plt.loglog()
	plt.errorbar(nsed['Serror']['x'], nsed['Serror']['y'], yerr=nsed['Serror']['ey'], 
		capthick=cpthick, capsize=cpsize, color='red', label='Error in S')
	plt.errorbar(nsed['Terror']['x'], nsed['Terror']['y'], yerr=nsed['Terror']['ey'], 
		capthick=cpthick, capsize=cpsize, color='magenta', label='Error in T')
	plt.errorbar(sed['Serror']['x'], sed['Serror']['y'], yerr=sed['Serror']['ey'], 
		capthick=cpthick, capsize=cpsize, color='green', label='Error in S - smoothed')
	plt.errorbar(sed['Terror']['x'], sed['Terror']['y'], yerr=sed['Terror']['ey'], 
		capthick=cpthick, capsize=cpsize, color='cyan', label='Error in T - smoothed')
	plt.errorbar(bmed['Serror']['x'], bmed['Serror']['y'], yerr=bmed['Serror']['ey'], 
		capthick=cpthick, capsize=cpsize, color='blue', label='Error in S - bimodal')
	plt.errorbar(bmed['Terror']['x'], bmed['Terror']['y'], yerr=bmed['Terror']['ey'], 
		capthick=cpthick, capsize=cpsize, color='black', label='Error in T - bimodal')
	plt.errorbar(bmsed['Serror']['x'], bmsed['Serror']['y'], yerr=bmsed['Serror']['ey'], 
		capthick=cpthick, capsize=cpsize, color='yellow', label='Error in S - bimodal/smooth')
	plt.errorbar(bmsed['Terror']['x'], bmsed['Terror']['y'], yerr=bmsed['Terror']['ey'], 
		capthick=cpthick, capsize=cpsize, color='orange', label='Error in T - bimodal/smooth')
	plt.xlim( xlimiter )
	plt.xlabel("dimernsionless standard deviation")
	plt.ylabel("error")
	plt.legend(loc=4)
	
	# Show the figures
	plt.show()
	
	# END OF EXAMPLE
	return 0

def plotJumpyTestsDataPub(datafile='resJumpy'):
	"""
	
	These plots may be used in the paper
	"""
	# load the data from file
	apds = dp.loadObjectFromFile(datafile)
	
	#
	#  Collect the data sub-sets
	#
	nsdata = cbs.assembleErrorData(apds, 'none')
	sdata = cbs.assembleErrorData(apds, 'smooth')
	bmdata = cbs.assembleErrorData(apds, 'bimodal')
	bmsdata = cbs.assembleErrorData(apds, 'smoothbimodal')
	# get the data in binned form
	nsbins = 2
	bins = 10**np.linspace(-3,3,6*nsbins+1)
	# set the data
	nsed = cbs.extractBinStats(nsdata, bins)
	sed = cbs.extractBinStats(sdata, bins)
	bmed = cbs.extractBinStats(bmdata, bins)
	bmsed = cbs.extractBinStats(bmsdata, bins)
	# sets of data for all plots
	alldata = [nsed, sed, bmed, bmsed]
	labels = ['raw', 'smoothed', 'bimodal', 'bimodal/smoothed']
	colors = ['red', 'green', 'blue', 'yellow']
	
	#
	#  Plots
	#
	# overall prep
	xlimiter = (10**-3, 10**3)
	stdxlabel = 'dimensionless standard deviation'
	ylimiter = (10**-4, 10**3)
	
	# Plot the norms by region
	plt.figure()
	plt.subplot(221)
	plt.text(3*10**-3, 10**2, r'$\mathrm{\mathbf{(a)}}$', fontweight='bold', fontsize=sfls)
	plt.loglog()
	for i in range(len(alldata)):
		dati = alldata[i]
		plt.errorbar(dati['norm']['x'], dati['norm']['y'], yerr=dati['norm']['ey'], 
			capthick=cpthick, capsize=cpsize, color=colors[i], label=labels[i])
	plt.xlim( xlimiter )
	plt.xlabel( stdxlabel )
	plt.ylim( ylimiter )
	plt.ylabel("dimensionless drawdown norm")
	
	# Plot the rate of failures in convergence
	plt.subplot(222)
	plt.text(3*10**-3, 0.6, r'$\mathrm{\mathbf{(b)}}$', fontweight='bold', fontsize=sfls)
	for i in range(len(alldata)):
		dati = alldata[i]
		plt.semilogx(dati['norm']['x'], dati['Failure Rate'], color=colors[i], label=labels[i])
	plt.xlim( xlimiter )
	plt.xlabel( stdxlabel )
	plt.ylabel("failure rate")
	
	# Plot the errors in S by region
	plt.subplot(223)
	plt.text(3*10**-3, 10**2, r'$\mathrm{\mathbf{(c)}}$', fontweight='bold', fontsize=sfls)
	plt.loglog()
	for i in range(len(alldata)):
		dati = alldata[i]
		plt.errorbar(dati['Serror']['x'], dati['Serror']['y'], yerr=dati['Serror']['ey'], 
			capthick=cpthick, capsize=cpsize, color=colors[i], label=labels[i])
	plt.xlim( xlimiter )
	plt.xlabel( stdxlabel )
	plt.ylim( ylimiter )
	plt.ylabel(r"error in $S$")
	plt.legend(bbox_to_anchor=(1.55, -0.15), ncol=2)

	# Plot the errors in T by region
	plt.subplot(224)
	plt.text(3*10**-3, 10**2, r'$\mathrm{\mathbf{(d)}}$', fontweight='bold', fontsize=sfls)
	plt.loglog()
	for i in range(len(alldata)):
		dati = alldata[i]
		plt.errorbar(dati['Terror']['x'], dati['Terror']['y'], yerr=dati['Terror']['ey'], 
			capthick=cpthick, capsize=cpsize, color=colors[i], label=labels[i])
	plt.xlim( xlimiter )
	plt.xlabel( stdxlabel )
	plt.ylim( ylimiter )
	plt.ylabel(r"error in $T$")

	# Show the figures
	plt.show()
	
	# END OF EXAMPLE
	return 0
###############################################################################
###############################################################################

#
#  Banded tests
#

def runNSaveBandedTests( datafile='resBanded'):
	# Load the data
	#T, H, B, ET = dp.loadExR3Mo()
	tstart = time.clock()
	print "Starting runs", tstart, 'seconds'
	#
	#  Time and Location specifications
	#
	dates=[dt.datetime(2012,12,1,0,0,0), dt.datetime(2013,2,1,0,0,0)]
	# where to load data from
	#pdir = os.path.join(os.path.dirname(os.path.abspath(__file__)), 'data', '2012_11-2013_02')
	pdir = []
	# create list
	timeLoc = [dates, pdir]
	#
	#  Height specifications
	#
	H0 = 5600
	#Hslope = -0.00001
	Hslope = 0
	# create list
	heightParams = [H0, Hslope]
	#
	#  Pumping Specs
	#
	Q = [np.array([200., 0.])]
	Qdates = [dt.datetime(2012,12,20,0,0,0), dt.datetime(2013,1,10,0,0,0)]
	# convert times to hours
	timezero = dt.datetime(2000,1,1,0,0,0)
	Qtime = [np.array(dp.dtList2hours(timezero, Qdates))]
	r = [np.array(.5*5280.0)]
	# aquifer parameters
	stor = 0.01
	trans = 1000.
	pparS = np.array([stor, trans])
	# create list
	pumpingParams = [Q, r, Qtime, pparS]
	# 
	#  B&ET regression parameters
	#
	#alphag = 0.01*np.array([-0.15, -0.20, -0.10, -0.05])
	#betag = 0.001*np.array([-0.015, 0.020, -0.010])
	# create list
	#betParams = [alphag, betag]
	betParams = []
	#
	#  Noise specifications
	#
	#sep = 0.07
	#width = 0.025
	#gsdev = 0.004
	
	sep = 0
	#lsep = 10*10**(np.arange(4.)/4.)
	#lsep = [10**-3]
	width = 0
	lwidth = 0.0001*10**(np.arange(25.)/4.)
	gsdev = 0
	rep = False
	# create list
	noiseParams = [sep, width, gsdev, rep]
	#noiseParams = []
	#
	#  Smoothing correction type
	#
	stype = 'smooth'
	#stype = 'none'
	lstype = ['none', 'smooth']
	#
	#  alpha/beta numbers
	#
	#alpham = 7
	#betam = 7
	# create list
	#betn = [alpham, betam]
	betn = [1,1]
	
	#
	#  Do everything and return all the results in one structure!
	#
	testtype = 'Bandwith noise + pumping'
	# is there saved data out there to add to?
	if os.path.isfile('results/'+datafile+'.pkl'):
		apds = dp.loadObjectFromFile(datafile)
	else :
		apds = {}
	# number of saved tests in dataset
	tID0 = len(apds)
	tIDi = tID0
	# number of samples per sub test
	nstests = 5
	# loop over interested parameters
	for j in range(len(lwidth)):
		# set variables to use in test set
		width = lwidth[j]
		noiseParams = [sep, width, gsdev, rep]
		# print out status and time
		print '\nprogress: '+str(float(j)/float(len(lwidth))*100.)+'% '
		print 'd var = ',width
		ti = time.clock()
		print 'elapsed time: '+str((ti - tstart)/60.)+' minutes'
		# loop over types of smoothing
		for k in iter(lstype):
			stype = k
			# do number of subtests in sample set
			for i in range(nstests):
				pds = cbs.doOnePumpTest(timeLoc, heightParams, pumpingParams, 
					betParams, noiseParams, betn, stype)
				pds['title'] = testtype+' test '+str(tIDi)
				cbs.printTestResults(pds)
				# save to full dataset
				testid = 't'+str(tIDi)
				apds[testid] = pds
				# increment test ID
				tIDi += 1

	#
	#  Results
	#
	dp.saveObjectToFile(apds, datafile )
	
	# tell me it's over
	end = time.clock()
	print 'Total time elapsed: '+str((end - tstart)/60.)+' minutes'
	
	# END OF EXAMPLE
	return 0

def plotBandedTestsData(datafile='resBanded'):
	"""
	"""
	apds = dp.loadObjectFromFile(datafile)
	
	nsdata = cbs.assembleErrorData(apds, 'none')
	sdata = cbs.assembleErrorData(apds, 'smooth')
	# get the data in binned form
	nsbins = 2
	bins = 10**np.linspace(-3,3,6*nsbins+1)
	# set the data
	nsed = cbs.extractBinStats(nsdata, bins)
	sed = cbs.extractBinStats(sdata, bins)
	
	#
	#  Plots
	#
	xlimiter = (10**-3, 10**3)
	
	# Plot the norms
	plt.figure()
	plt.loglog(nsdata['stdev'], nsdata['norm'], 'r+', label='regular data')
	plt.loglog(sdata['stdev'], sdata['norm'], 'g+', label='smoothed data')
	plt.xlim( xlimiter )
	plt.xlabel("dimensionless standard deviation")
	plt.ylabel("dimensionless drawdown norm")
	plt.legend(loc=2)

	# Plot the norms by region
	plt.figure()
	plt.loglog()
	plt.errorbar(nsed['norm']['x'], nsed['norm']['y'], yerr=nsed['norm']['ey'], 
		capthick=cpthick, capsize=cpsize, color='red', label='regular data')
	plt.errorbar(sed['norm']['x'], sed['norm']['y'], yerr=sed['norm']['ey'], 
		capthick=cpthick, capsize=cpsize, color='green', label='smoothed data')
	plt.xlim( xlimiter )
	plt.xlabel("dimensionless standard deviation")
	plt.ylabel("dimensionless drawdown norm")
	plt.legend(loc=2)
	
	# Plot the errors in T and S
	plt.figure()
	plt.loglog(nsdata['stdev'], nsdata['Serror'], 'r+', label='Error in S')
	plt.loglog(nsdata['stdev'], nsdata['Terror'], 'm+', label='Error in T')
	plt.loglog(sdata['stdev'], sdata['Serror'], 'g+', label='Error in S - smoothed')
	plt.loglog(sdata['stdev'], sdata['Terror'], 'c+', label='Error in T - smoothed')
	plt.xlim( xlimiter )
	plt.xlabel("dimernsionless standard deviation")
	plt.ylabel("error")
	plt.legend(loc=2)
	
	# Plot the errors in T and S by region
	plt.figure()
	plt.loglog()
	plt.errorbar(nsed['Serror']['x'], nsed['Serror']['y'], yerr=nsed['Serror']['ey'], 
		capthick=cpthick, capsize=cpsize, color='red', label='Error in S')
	plt.errorbar(nsed['Terror']['x'], nsed['Terror']['y'], yerr=nsed['Terror']['ey'], 
		capthick=cpthick, capsize=cpsize, color='magenta', label='Error in T')
	plt.errorbar(sed['Serror']['x'], sed['Serror']['y'], yerr=sed['Serror']['ey'], 
		capthick=cpthick, capsize=cpsize, color='green', label='Error in S - smoothed')
	plt.errorbar(sed['Terror']['x'], sed['Terror']['y'], yerr=sed['Terror']['ey'], 
		capthick=cpthick, capsize=cpsize, color='cyan', label='Error in T - smoothed')
	plt.xlim( xlimiter )
	plt.xlabel("dimernsionless standard deviation")
	plt.ylabel("error")
	plt.legend(loc=4)
	
	# Show the figures
	plt.show()
	
	# END OF EXAMPLE
	return 0

def plotBandedTestsDataPub(datafile='resBanded'):
	"""
	
	These plots may be used in the paper
	"""
	# load the data from file
	apds = dp.loadObjectFromFile(datafile)
	
	#
	#  Collect the data sub-sets
	#
	nsdata = cbs.assembleErrorData(apds, 'none')
	sdata = cbs.assembleErrorData(apds, 'smooth')
	# get the data in binned form
	nsbins = 2
	bins = 10**np.linspace(-3,3,6*nsbins+1)
	# set the data
	nsed = cbs.extractBinStats(nsdata, bins)
	sed = cbs.extractBinStats(sdata, bins)
	# sets of data for all plots
	alldata = [nsed, sed]
	labels = ['raw', 'smoothed', 'bimodal', 'bimodal/smoothed']
	colors = ['red', 'green', 'blue', 'yellow']
	
	#
	#  Plots
	#
	# overall prep
	xlimiter = (10**-3, 10**3)
	stdxlabel = 'dimensionless standard deviation'
	ylimiter = (10**-5, 10**3)
	
	# Plot the norms by region
	plt.figure()
	plt.subplot(221)
	plt.text(3*10**-3, 10**3, r'$\mathrm{\mathbf{(a)}}$', fontweight='bold', fontsize=sfls)
	plt.loglog()
	for i in range(len(alldata)):
		dati = alldata[i]
		plt.errorbar(dati['norm']['x'], dati['norm']['y'], yerr=dati['norm']['ey'], 
			capthick=cpthick, capsize=cpsize, color=colors[i], label=labels[i])
	plt.xlim( xlimiter )
	plt.xlabel( stdxlabel )
	#plt.ylim( ylimiter )
	plt.ylabel("dimensionless drawdown norm")
	
	# Plot the rate of failures in convergence
	plt.subplot(222)
	plt.text(3*10**-3, 0.8, r'$\mathrm{\mathbf{(b)}}$', fontweight='bold', fontsize=sfls)
	for i in range(len(alldata)):
		dati = alldata[i]
		plt.semilogx(dati['norm']['x'], dati['Failure Rate'], color=colors[i], label=labels[i])
	plt.xlim( xlimiter )
	plt.xlabel( stdxlabel )
	plt.ylabel("failure rate")
	
	# Plot the errors in S by region
	plt.subplot(223)
	plt.text(3*10**-3, 10**2, r'$\mathrm{\mathbf{(c)}}$', fontweight='bold', fontsize=sfls)
	plt.loglog()
	for i in range(len(alldata)):
		dati = alldata[i]
		plt.errorbar(dati['Serror']['x'], dati['Serror']['y'], yerr=dati['Serror']['ey'], 
			capthick=cpthick, capsize=cpsize, color=colors[i], label=labels[i])
	plt.xlim( xlimiter )
	plt.xlabel( stdxlabel )
	plt.ylim( ylimiter )
	plt.ylabel(r"error in $S$")
	plt.legend(bbox_to_anchor=(1.55, -0.15), ncol=2)

	# Plot the errors in T by region
	plt.subplot(224)
	plt.text(3*10**-3, 10**2, r'$\mathrm{\mathbf{(d)}}$', fontweight='bold', fontsize=sfls)
	plt.loglog()
	for i in range(len(alldata)):
		dati = alldata[i]
		plt.errorbar(dati['Terror']['x'], dati['Terror']['y'], yerr=dati['Terror']['ey'], 
			capthick=cpthick, capsize=cpsize, color=colors[i], label=labels[i])
	plt.xlim( xlimiter )
	plt.xlabel( stdxlabel )
	plt.ylim( ylimiter )
	plt.ylabel(r"error in $T$")

	# Show the figures
	plt.show()
	
	# END OF EXAMPLE
	return 0
###############################################################################
###############################################################################

#
#  Gauss tests
#

def runNSaveGaussTests( datafile='resGauss' ):
	# Load the data
	#T, H, B, ET = dp.loadExR3Mo()
	tstart = time.clock()
	print "Starting runs", tstart, 'seconds'
	#
	#  Time and Location specifications
	#
	dates=[dt.datetime(2012,12,1,0,0,0), dt.datetime(2013,2,1,0,0,0)]
	# where to load data from
	#pdir = os.path.join(os.path.dirname(os.path.abspath(__file__)), 'data', '2012_11-2013_02')
	pdir = []
	# create list
	timeLoc = [dates, pdir]
	#
	#  Height specifications
	#
	H0 = 5600
	#Hslope = -0.00001
	Hslope = 0
	# create list
	heightParams = [H0, Hslope]
	#
	#  Pumping Specs
	#
	Q = [np.array([200., 0.])]
	Qdates = [dt.datetime(2012,12,20,0,0,0), dt.datetime(2013,1,10,0,0,0)]
	# convert times to hours
	timezero = dt.datetime(2000,1,1,0,0,0)
	Qtime = [np.array(dp.dtList2hours(timezero, Qdates))]
	r = [np.array(.5*5280.0)]
	# aquifer parameters
	stor = 0.01
	trans = 1000.
	pparS = np.array([stor, trans])
	# create list
	pumpingParams = [Q, r, Qtime, pparS]
	# 
	#  B&ET regression parameters
	#
	#alphag = 0.01*np.array([-0.15, -0.20, -0.10, -0.05])
	#betag = 0.001*np.array([-0.015, 0.020, -0.010])
	# create list
	#betParams = [alphag, betag]
	betParams = []
	#
	#  Noise specifications
	#
	#sep = 0.07
	#width = 0.025
	#gsdev = 0.004
	
	sep = 0
	#lsep = 10*10**(np.arange(4.)/4.)
	#lsep = [10**-3]
	width = 0
	gsdev = 0
	lgsdev = 0.0001*10**(np.arange(25.)/4.)
	rep = False
	# create list
	noiseParams = [sep, width, gsdev, rep]
	#noiseParams = []
	#
	#  Smoothing correction type
	#
	#stype = 'smooth'
	stype = 'none'
	lstype = ['none', 'smooth']
	#
	#  alpha/beta numbers
	#
	#alpham = 7
	#betam = 7
	# create list
	#betn = [alpham, betam]
	betn = [1,1]
	
	#
	#  Do everything and return all the results in one structure!
	#
	testtype = 'Gaussian noise + pumping'
	# is there saved data out there to add to?
	if os.path.isfile('results/'+datafile+'.pkl'):
		apds = dp.loadObjectFromFile(datafile)
	else :
		apds = {}
	# number of saved tests in dataset
	tID0 = len(apds)
	tIDi = tID0
	# number of samples per sub test
	nstests = 5
	# loop over interested parameters
	for j in range(len(lgsdev)):
		# set variables to use in test set
		gsdev = lgsdev[j]
		noiseParams = [sep, width, gsdev, rep]
		# print out status and time
		print '\nprogress: '+str(float(j)/float(len(lgsdev))*100.)+'% '
		print 'd var = ',gsdev
		ti = time.clock()
		print 'elapsed time: '+str((ti - tstart)/60.)+' minutes'
		# loop over types of smoothing
		for k in iter(lstype):
			stype = k
			# do number of subtests in sample set
			for i in range(nstests):
				pds = cbs.doOnePumpTest(timeLoc, heightParams, pumpingParams, 
					betParams, noiseParams, betn, stype)
				pds['title'] = testtype+' test '+str(tIDi)
				cbs.printTestResults(pds)
				# save to full dataset
				testid = 't'+str(tIDi)
				apds[testid] = pds
				# increment test ID
				tIDi += 1

	#
	#  Results
	#
	dp.saveObjectToFile(apds, datafile )
	
	# tell me it's over
	end = time.clock()
	print 'Total time elapsed: '+str((end - tstart)/60.)+' minutes'
	
	# END OF EXAMPLE
	return 0

def plotGaussTestsData(datafile='resGauss'):
	"""
	"""
	apds = dp.loadObjectFromFile(datafile)
	
	nsdata = cbs.assembleErrorData(apds, 'none')
	sdata = cbs.assembleErrorData(apds, 'smooth')
	
	# get the data in binned form
	nsbins = 2
	bins = 10**np.linspace(-3,3,6*nsbins+1)
	# set the data
	nsed = cbs.extractBinStats(nsdata, bins)
	sed = cbs.extractBinStats(sdata, bins)
	
	#
	#  Plots
	#
	xlimiter = (10**-3, 10**3)
	
	# Plot the norms
	plt.figure()
	plt.loglog(nsdata['stdev'], nsdata['norm'], 'r+', label='regular data')
	plt.loglog(sdata['stdev'], sdata['norm'], 'g+', label='smoothed data')
	plt.xlim( xlimiter )
	plt.xlabel("dimensionless standard deviation")
	plt.ylabel("dimensionless drawdown norm")
	plt.legend(loc=2)
	
	# Plot the norms by region
	plt.figure()
	plt.loglog()
	plt.errorbar(nsed['norm']['x'], nsed['norm']['y'], yerr=nsed['norm']['ey'], 
		capthick=cpthick, capsize=cpsize, color='red', label='regular data')
	plt.errorbar(sed['norm']['x'], sed['norm']['y'], yerr=sed['norm']['ey'], 
		capthick=cpthick, capsize=cpsize, color='green', label='smoothed data')
	plt.xlim( xlimiter )
	plt.xlabel("dimensionless standard deviation")
	plt.ylabel("dimensionless drawdown norm")
	plt.legend(loc=2)
	
	# Plot the errors in T and S
	plt.figure()
	plt.loglog(nsdata['stdev'], nsdata['Serror'], 'r+', label='Error in S')
	plt.loglog(nsdata['stdev'], nsdata['Terror'], 'm+', label='Error in T')
	plt.loglog(sdata['stdev'], sdata['Serror'], 'g+', label='Error in S - smoothed')
	plt.loglog(sdata['stdev'], sdata['Terror'], 'c+', label='Error in T - smoothed')
	plt.xlim( xlimiter )
	plt.xlabel("dimernsionless standard deviation")
	plt.ylabel("error")
	plt.legend(loc=2)
	
	# Plot the errors in T and S by region
	plt.figure()
	plt.loglog()
	plt.errorbar(nsed['Serror']['x'], nsed['Serror']['y'], yerr=nsed['Serror']['ey'], 
		capthick=cpthick, capsize=cpsize, color='red', label='Error in S')
	plt.errorbar(nsed['Terror']['x'], nsed['Terror']['y'], yerr=nsed['Terror']['ey'], 
		capthick=cpthick, capsize=cpsize, color='magenta', label='Error in T')
	plt.errorbar(sed['Serror']['x'], sed['Serror']['y'], yerr=sed['Serror']['ey'], 
		capthick=cpthick, capsize=cpsize, color='green', label='Error in S - smoothed')
	plt.errorbar(sed['Terror']['x'], sed['Terror']['y'], yerr=sed['Terror']['ey'], 
		capthick=cpthick, capsize=cpsize, color='cyan', label='Error in T - smoothed')
	plt.xlim( xlimiter )
	plt.xlabel("dimernsionless standard deviation")
	plt.ylabel("error")
	plt.legend(loc=4)
	
	# Show the figures
	plt.show()
	
	# END OF EXAMPLE
	return 0

def plotGaussTestsDataPub(datafile='resGauss'):
	"""
	
	These plots may be used in the paper
	"""
	# load the data from file
	apds = dp.loadObjectFromFile(datafile)
	
	#
	#  Collect the data sub-sets
	#
	nsdata = cbs.assembleErrorData(apds, 'none')
	sdata = cbs.assembleErrorData(apds, 'smooth')
	# get the data in binned form
	nsbins = 2
	bins = 10**np.linspace(-3,3,6*nsbins+1)
	# set the data
	nsed = cbs.extractBinStats(nsdata, bins)
	sed = cbs.extractBinStats(sdata, bins)
	# sets of data for all plots
	alldata = [nsed, sed]
	labels = ['raw', 'smoothed', 'bimodal', 'bimodal/smoothed']
	colors = ['red', 'green', 'blue', 'yellow']
	
	#
	#  Plots
	#
	# overall prep
	xlimiter = (10**-3, 10**3)
	stdxlabel = 'dimensionless standard deviation'
	ylimiter = (10**-5, 10**3)
	
	# Plot the norms by region
	plt.figure()
	plt.subplot(221)
	plt.text(3*10**-3, 2*10**1, r'$\mathrm{\mathbf{(a)}}$', fontweight='bold', fontsize=sfls)
	plt.loglog()
	for i in range(len(alldata)):
		dati = alldata[i]
		plt.errorbar(dati['norm']['x'], dati['norm']['y'], yerr=dati['norm']['ey'], 
			capthick=cpthick, capsize=cpsize, color=colors[i], label=labels[i])
	plt.xlim( xlimiter )
	plt.xlabel( stdxlabel )
	#plt.ylim( ylimiter )
	plt.ylabel("dimensionless drawdown norm")
	
	# Plot the rate of failures in convergence
	plt.subplot(222)
	plt.text(3*10**-3, 0.6, r'$\mathrm{\mathbf{(b)}}$', fontweight='bold', fontsize=sfls)
	for i in range(len(alldata)):
		dati = alldata[i]
		plt.semilogx(dati['norm']['x'], dati['Failure Rate'], color=colors[i], label=labels[i])
	plt.xlim( xlimiter )
	plt.xlabel( stdxlabel )
	plt.ylabel("failure rate")
	
	# Plot the errors in S by region
	plt.subplot(223)
	plt.text(3*10**-3, 10**2, r'$\mathrm{\mathbf{(c)}}$', fontweight='bold', fontsize=sfls)
	plt.loglog()
	for i in range(len(alldata)):
		dati = alldata[i]
		plt.errorbar(dati['Serror']['x'], dati['Serror']['y'], yerr=dati['Serror']['ey'], 
			capthick=cpthick, capsize=cpsize, color=colors[i], label=labels[i])
	plt.xlim( xlimiter )
	plt.xlabel( stdxlabel )
	plt.ylim( ylimiter )
	plt.ylabel(r"error in $S$")
	plt.legend(bbox_to_anchor=(1.55, -0.15), ncol=2)

	# Plot the errors in T by region
	plt.subplot(224)
	plt.text(3*10**-3, 10**2, r'$\mathrm{\mathbf{(d)}}$', fontweight='bold', fontsize=sfls)
	plt.loglog()
	for i in range(len(alldata)):
		dati = alldata[i]
		plt.errorbar(dati['Terror']['x'], dati['Terror']['y'], yerr=dati['Terror']['ey'], 
			capthick=cpthick, capsize=cpsize, color=colors[i], label=labels[i])
	plt.xlim( xlimiter )
	plt.xlabel( stdxlabel )
	plt.ylim( ylimiter )
	plt.ylabel(r"error in $T$")

	# Show the figures
	plt.show()
	
	# END OF EXAMPLE
	return 0
###############################################################################
###############################################################################

#
#  All noise tests
#

def runNSaveAllNoiseTests( datafile='resAllNoise' ):
	# Load the data
	#T, H, B, ET = dp.loadExR3Mo()
	tstart = time.clock()
	print "Starting runs", tstart, 'seconds'
	#
	#  Time and Location specifications
	#
	dates=[dt.datetime(2012,12,1,0,0,0), dt.datetime(2013,2,1,0,0,0)]
	# where to load data from
	#pdir = os.path.join(os.path.dirname(os.path.abspath(__file__)), 'data', '2012_11-2013_02')
	pdir = []
	# create list
	timeLoc = [dates, pdir]
	#
	#  Height specifications
	#
	H0 = 5600
	#Hslope = -0.00001
	Hslope = 0
	# create list
	heightParams = [H0, Hslope]
	#
	#  Pumping Specs
	#
	Q = [np.array([200., 0.])]
	Qdates = [dt.datetime(2012,12,20,0,0,0), dt.datetime(2013,1,10,0,0,0)]
	# convert times to hours
	timezero = dt.datetime(2000,1,1,0,0,0)
	Qtime = [np.array(dp.dtList2hours(timezero, Qdates))]
	r = [np.array(.5*5280.0)]
	# aquifer parameters
	stor = 0.01
	trans = 1000.
	pparS = np.array([stor, trans])
	# create list
	pumpingParams = [Q, r, Qtime, pparS]
	# 
	#  B&ET regression parameters
	#
	#alphag = 0.01*np.array([-0.15, -0.20, -0.10, -0.05])
	#betag = 0.001*np.array([-0.015, 0.020, -0.010])
	# create list
	#betParams = [alphag, betag]
	betParams = []
	#
	#  Noise specifications
	#
	#sep = 0.07
	#width = 0.025
	#gsdev = 0.004
	
	sep = 0
	width = 0
	gsdev = 0
	rep = False
	# create list
	noiseParams = [sep, width, gsdev, rep]
	#noiseParams = []
	#
	#  Smoothing correction type
	#
	#stype = 'smooth'
	stype = 'none'
	lstype = ['none', 'smooth', 'bimodal', 'smoothbimodal']
	#
	#  alpha/beta numbers
	#
	#alpham = 7
	#betam = 7
	# create list
	#betn = [alpham, betam]
	betn = [1, 1]
	
	#
	#  Do everything and return all the results in one structure!
	#
	testtype = 'All generated noise types + pumping'
	# is there saved data out there to add to?
	if os.path.isfile('results/'+datafile+'.pkl'):
		apds = dp.loadObjectFromFile(datafile)
	else :
		apds = {}
	# number of saved tests in dataset
	tID0 = len(apds)
	tIDi = tID0
	# number of total samples in the space
	ntests = 50
	# number of samples per sub test
	nstests = 5
	# loop over interested parameters
	for j in range(ntests):
		# set variables to use in test set
		randr = random.uniform(-4, 2)
		sep   = 10**random.uniform(randr-1, randr+1)
		width = 10**random.uniform(randr-1, randr+1)
		gsdev = 10**random.uniform(randr-1, randr+1)
		noiseParams = [sep, width, gsdev, rep]
		# print out status and time
		print '\nprogress: '+str(float(j)/float(ntests)*100.)+'% '
		print 'd var(s) = ',sep,width,gsdev
		ti = time.clock()
		print 'elapsed time: '+str((ti - tstart)/60.)+' minutes'
		# do the different types of smoothing
		for k in iter(lstype):
			stype = k
			# do number of subtests in sample set
			for i in range(nstests):
				pds = cbs.doOnePumpTest(timeLoc, heightParams, pumpingParams, 
					betParams, noiseParams, betn, stype)
				pds['title'] = testtype+' test '+str(tIDi)
				pcbs.rintTestResults(pds)
				# save to full dataset
				testid = 't'+str(tIDi)
				apds[testid] = pds
				# increment test ID
				tIDi += 1

	#
	#  Results
	#
	dp.saveObjectToFile(apds, datafile )
	
	# tell me it's over
	end = time.clock()
	print 'Total time elapsed: '+str((end - tstart)/60.)+' minutes'
	
	# END OF EXAMPLE
	return 0

def plotAllNoiseTestsData(datafile='resAllNoise'):
	"""
	"""
	apds = dp.loadObjectFromFile(datafile)
	# assemble data into sensible list structures
	nsdata = cbs.assembleErrorData(apds, 'none')
	sdata = cbs.assembleErrorData(apds, 'smooth')
	bmdata = cbs.assembleErrorData(apds, 'bimodal')
	bmsdata = cbs.assembleErrorData(apds, 'smoothbimodal')

	# get the data in binned form
	nsbins = 2
	bins = 10**np.linspace(-3,3,6*nsbins+1)
	# set the data
	nsed = cbs.extractBinStats(nsdata, bins)
	sed = cbs.extractBinStats(sdata, bins)
	bmed = cbs.extractBinStats(bmdata, bins)
	bmsed = cbs.extractBinStats(bmsdata, bins)
	
	#
	#  Plots
	#
	xlimiter = (10**-3, 10**3)
	
	# Plot the norms
	plt.figure()
	plt.loglog(nsdata['stdev'], nsdata['norm'], 'r+', label='regular data')
	plt.loglog(sdata['stdev'], sdata['norm'], 'g+', label='smoothed data')
	plt.loglog(bmdata['stdev'], bmdata['norm'], 'b+', label='bimodal corrected data')
	plt.loglog(bmsdata['stdev'], bmsdata['norm'], 'y+', label='bimodal smoothed data')
	plt.xlim( xlimiter )
	plt.xlabel("dimensionless standard deviation")
	plt.ylabel("dimensionless drawdown norm")
	plt.legend(loc=2)
	
	# Plot the norms by region
	plt.figure()
	plt.loglog()
	plt.errorbar(nsed['norm']['x'], nsed['norm']['y'], yerr=nsed['norm']['ey'], 
		capthick=cpthick, capsize=cpsize, color='red', label='regular data')
	plt.errorbar(sed['norm']['x'], sed['norm']['y'], yerr=sed['norm']['ey'], 
		capthick=cpthick, capsize=cpsize, color='green', label='smoothed data')
	plt.errorbar(bmed['norm']['x'], bmed['norm']['y'], yerr=bmed['norm']['ey'], 
		capthick=cpthick, capsize=cpsize, color='blue', label='bimodal corrected data')
	plt.errorbar(bmsed['norm']['x'], bmsed['norm']['y'], yerr=bmsed['norm']['ey'], 
		capthick=cpthick, capsize=cpsize, color='yellow', label='bimodal smoothed data')
	plt.xlim( xlimiter )
	plt.xlabel("dimensionless standard deviation")
	plt.ylabel("dimensionless drawdown norm")
	plt.legend(loc=2)
	
	# Plot the errors in T and S
	plt.figure()
	plt.loglog(nsdata['stdev'], nsdata['Serror'], 'r+', label='Error in S')
	plt.loglog(nsdata['stdev'], nsdata['Terror'], 'm+', label='Error in T')
	plt.loglog(sdata['stdev'], sdata['Serror'], 'g+', label='Error in S - smoothed')
	plt.loglog(sdata['stdev'], sdata['Terror'], 'c+', label='Error in T - smoothed')
	plt.loglog(bmdata['stdev'], bmdata['Serror'], 'b+', label='Error in S - bimodal')
	plt.loglog(bmdata['stdev'], bmdata['Terror'], 'k+', label='Error in T - bimodal')
	plt.loglog(bmsdata['stdev'], bmsdata['Serror'], 'y+', label='Error in S - bimodal/smooth')
	plt.loglog(bmsdata['stdev'], bmsdata['Terror'], '+', color='orange',
		label='Error in T - bimodal/smooth')
	plt.xlim( xlimiter )
	plt.xlabel("dimernsionless standard deviation")
	plt.ylabel("error")
	plt.legend(loc=2)
	
	# Plot the errors in T and S by region
	plt.figure()
	plt.loglog()
	plt.errorbar(nsed['Serror']['x'], nsed['Serror']['y'], yerr=nsed['Serror']['ey'],  
		capthick=cpthick, capsize=cpsize, color='red', label='Error in S')
	plt.errorbar(nsed['Terror']['x'], nsed['Terror']['y'], yerr=nsed['Terror']['ey'],
		capthick=cpthick, capsize=cpsize, color='magenta', label='Error in T')
	plt.errorbar(sed['Serror']['x'], sed['Serror']['y'], yerr=sed['Serror']['ey'], 
		capthick=cpthick, capsize=cpsize, color='green', label='Error in S - smoothed')
	plt.errorbar(sed['Terror']['x'], sed['Terror']['y'], yerr=sed['Terror']['ey'], 
		capthick=cpthick, capsize=cpsize, color='cyan', label='Error in T - smoothed')
	plt.errorbar(bmed['Serror']['x'], bmed['Serror']['y'], yerr=bmed['Serror']['ey'], 
		capthick=cpthick, capsize=cpsize, color='blue', label='Error in S - bimodal')
	plt.errorbar(bmed['Terror']['x'], bmed['Terror']['y'], yerr=bmed['Terror']['ey'], 
		capthick=cpthick, capsize=cpsize, color='black', label='Error in T - bimodal')
	plt.errorbar(bmsed['Serror']['x'], bmsed['Serror']['y'], yerr=bmsed['Serror']['ey'], 
		capthick=cpthick, capsize=cpsize, color='yellow', label='Error in S - bimodal/smooth')
	plt.errorbar(bmsed['Terror']['x'], bmsed['Terror']['y'], yerr=bmsed['Terror']['ey'], 
		capthick=cpthick, capsize=cpsize, color='orange', label='Error in T - bimodal/smooth')
	plt.xlim( xlimiter )
	plt.xlabel("dimernsionless standard deviation")
	plt.ylabel("error")
	plt.legend(loc=4)

	# Show the figures
	plt.show()
	
	# END OF EXAMPLE
	return 0

def plotAllNoiseTestsDataPub(datafile='resAllNoise'):
	"""
	
	These plots may be used in the paper
	"""
	# load the data from file
	apds = dp.loadObjectFromFile(datafile)
	
	#
	#  Collect the data sub-sets
	#
	nsdata = cbs.assembleErrorData(apds, 'none')
	sdata = cbs.assembleErrorData(apds, 'smooth')
	bmdata = cbs.assembleErrorData(apds, 'bimodal')
	bmsdata = cbs.assembleErrorData(apds, 'smoothbimodal')
	# get the data in binned form
	nsbins = 2
	bins = 10**np.linspace(-3,3,6*nsbins+1)
	# set the data
	nsed = cbs.extractBinStats(nsdata, bins)
	sed = cbs.extractBinStats(sdata, bins)
	bmed = cbs.extractBinStats(bmdata, bins)
	bmsed = cbs.extractBinStats(bmsdata, bins)
	# sets of data for all plots
	alldata = [nsed, sed, bmed, bmsed]
	labels = ['raw', 'smoothed', 'bimodal', 'bimodal/smoothed']
	colors = ['red', 'green', 'blue', 'yellow']
	
	#
	#  Plots
	#
	# overall prep
	xlimiter = (10**-3, 10**3)
	stdxlabel = 'dimensionless standard deviation'
	ylimiter = (10**-5, 10**2)
	
	# Plot the norms by region
	plt.figure()
	plt.subplot(221)
	plt.text(3*10**-3, 10**2, r'$\mathrm{\mathbf{(a)}}$', fontweight='bold', fontsize=sfls)
	plt.loglog()
	for i in range(len(alldata)):
		dati = alldata[i]
		plt.errorbar(dati['norm']['x'], dati['norm']['y'], yerr=dati['norm']['ey'], 
			capthick=cpthick, capsize=cpsize, color=colors[i], label=labels[i])
	plt.xlim( xlimiter )
	plt.xlabel( stdxlabel )
	plt.ylim( (10**-4, 10**3) )
	plt.ylabel("dimensionless drawdown norm")
	
	# Plot the rate of failures in convergence
	plt.subplot(222)
	plt.text(3*10**-3, 0.6, r'$\mathrm{\mathbf{(b)}}$', fontweight='bold', fontsize=sfls)
	for i in range(len(alldata)):
		dati = alldata[i]
		plt.semilogx(dati['norm']['x'], dati['Failure Rate'], color=colors[i], label=labels[i])
	plt.xlim( xlimiter )
	plt.xlabel( stdxlabel )
	plt.ylabel("failure rate")
	
	# Plot the errors in S by region
	plt.subplot(223)
	plt.text(3*10**-3, 10**1, r'$\mathrm{\mathbf{(c)}}$', fontweight='bold', fontsize=sfls)
	plt.loglog()
	for i in range(len(alldata)):
		dati = alldata[i]
		plt.errorbar(dati['Serror']['x'], dati['Serror']['y'], yerr=dati['Serror']['ey'], 
			capthick=cpthick, capsize=cpsize, color=colors[i], label=labels[i])
	plt.xlim( xlimiter )
	plt.xlabel( stdxlabel )
	plt.ylim( ylimiter )
	plt.ylabel(r"error in $S$")
	plt.legend(bbox_to_anchor=(1.55, -0.15), ncol=2)

	# Plot the errors in T by region
	plt.subplot(224)
	plt.text(3*10**-3, 10**1, r'$\mathrm{\mathbf{(d)}}$', fontweight='bold', fontsize=sfls)
	plt.loglog()
	for i in range(len(alldata)):
		dati = alldata[i]
		plt.errorbar(dati['Terror']['x'], dati['Terror']['y'], yerr=dati['Terror']['ey'], 
			capthick=cpthick, capsize=cpsize, color=colors[i], label=labels[i])
	plt.xlim( xlimiter )
	plt.xlabel( stdxlabel )
	plt.ylim( ylimiter )
	plt.ylabel(r"error in $T$")

	# Show the figures
	plt.show()
	
	# END OF EXAMPLE
	return 0
###############################################################################
###############################################################################

#
#  function running code
#
if __name__ == "__main__":
	
	# Jumpy tests
	dfj = 'resJumpy'
	#runNSaveJumpyTests(dfj)
	#plotJumpyTestsData(dfj)
	#plotJumpyTestsDataPub(dfj)
	# Banded tests
	dfb = 'resBanded'
	#runNSaveBandedTests(dfb)
	#plotBandedTestsData(dfb)
	#plotBandedTestsDataPub(dfb)
	# Gaussian tests
	dfg = 'resGauss'
	#runNSaveGaussTests(dfg)
	#plotGaussTestsData(dfg)
	#plotGaussTestsDataPub(dfg)
	# All noise tests
	dfa = 'resAllNoise'
	#runNSaveAllNoiseTests(dfa)
	#plotAllNoiseTestsData(dfa)
	#plotAllNoiseTestsDataPub(dfa)

	# END OF RUNS

#
#  End of File
#