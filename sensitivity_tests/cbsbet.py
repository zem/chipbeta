""" Sensitivity tests for synthetic baro and ET data

Authors: **Eric M. Benner**

Date:    10/29/14

Updated: 10/29/14
"""

"""
TODO: Software License?
"""

###############################################################################

# 
#  import packages and settings
#
# path appending
import sys
sys.path.append('../')
# importing
import cbsensitivity as cbs
import chipbeta as cb
import dataproc as dp
import datetime as dt
import matplotlib as mpl
import matplotlib.pyplot as plt
#from matplotlib.dates import MONDAY
#from matplotlib.dates import MonthLocator, WeekdayLocator, DayLocator, DateFormatter
import numpy as np
import os
import random
import time

#
#  plot settings
#
# font sizes
mpl.rcParams.update({'font.size': 20})
#mpl.rcParams.update({'label.size': 25})
#sub figure label size
sfls = 24
# plot line widths
llw = 3.
mpl.rcParams.update({'lines.linewidth': llw})
cpthick = float(llw)/2.
cpsize = llw*2.
# axes lines
axt = 2
mpl.rcParams.update({'axes.linewidth': axt})
mpl.rcParams.update({'xtick.major.width': axt})
mpl.rcParams.update({'xtick.major.size': axt*4.})
mpl.rcParams.update({'xtick.minor.width': axt/2.})
mpl.rcParams.update({'xtick.minor.size': axt*2.})
mpl.rcParams.update({'ytick.major.width': axt})
mpl.rcParams.update({'ytick.major.size': axt*4.})
mpl.rcParams.update({'ytick.minor.width': axt/2.})
mpl.rcParams.update({'ytick.minor.size': axt*2.})
# legend
mpl.rcParams.update({'legend.fancybox': True})
mpl.rcParams.update({'legend.fontsize': 'medium'})
###############################################################################
###############################################################################

###############################################################################
###############################################################################

#
#  Barometric and Earth tide tests
#

def runNSaveBETTests( datafile='resBET' ):
	#
	#  Internal functions
	#
	# TODO:
	def genRandTimespanInLAdata(daterange):
		# base data point for time conversions
		timezero = dt.datetime(2000,1,1,0,0,0)
		# earliest time in LA data
		firstT = dt.datetime(2004,1,1,0,0,0)
		# last time in LA data
		latestT = dt.datetime(2014,8,14,0,0,0)
		# latest start time possible
		lastT = latestT - daterange
		Tfl = [firstT, lastT]
		# get these times in hours
		Tflh = dp.dtList2hours(timezero, Tfl)
		rT = random.uniform(Tflh[0],Tflh[1])
		# return to a real time
		newStartT = timezero + dt.timedelta(int(rT)/24, (int(rT)%24)*60*60)
		newEndT = newStartT + daterange
		newDates = [newStartT, newEndT]
		# END OF FUNCTION
		return newDates
	
	def getDatesofQt(dates, Qtr):
		# base data point for time conversions
		timezero = dt.datetime(2000,1,1,0,0,0)
		# convert to hours
		Tse = dp.dtList2hours(timezero, dates)
		# find the times
		Qtn = [int(Tse[0]+Qtr[0]*(Tse[1]-Tse[0])), int(Tse[0]+Qtr[1]*(Tse[1]-Tse[0]))]
		Qtimenew = Qtn
		# set datetimes
		newQStartT = timezero + dt.timedelta(Qtn[0]/24, (Qtn[0]%24)*60*60)
		newQEndT = timezero + dt.timedelta(Qtn[1]/24, (Qtn[1]%24)*60*60)
		Qdatesnew = [newQStartT, newQEndT]
		#print Qdatesnew
		# END OF FUNCTION
		return Qdatesnew, Qtimenew
	
	def screwUpPumpingParams(Qrange, rrange, Srange, Trange):
		'''Randomize the parameters of an aquifir within ranges
		
		'''
		Qrandi = random.uniform(Qrange[0][0], Qrange[1][0])
		Qrand = [np.array([Qrandi, Qrange[0][1]])]
		Rrand = [np.array(random.uniform(*rrange))]
		Srand = random.uniform(*Srange)
		Trand = random.uniform(*Trange)
		return Qrand, Rrand, Srand, Trand
	
	# Load the data
	#T, H, B, ET = dp.loadExR3Mo()
	tstart = time.clock()
	print "Starting runs", tstart, 'seconds'
	#
	#  Time and Location specifications
	#
	dates = [dt.datetime(2012,12,1,0,0,0), dt.datetime(2013,2,1,0,0,0)]
	daterange = dates[1] - dates[0]
	#dates = genRandTimespanInLAdata(daterange)
	# where to load data from
	#pdir = os.path.join(os.path.dirname(os.path.abspath(__file__)), 'data', '2012_11-2013_02')
	pdir = []
	# create list
	timeLoc = [dates, pdir]
	#
	#  Height specifications
	#
	H0 = 5600
	#Hslope = -0.00001
	Hslope = 0
	# create list
	heightParams = [H0, Hslope]
	#
	#  Pumping Specs
	#
	Q = [np.array([200., 0.])]
	Qdates = [dt.datetime(2012,12,20,0,0,0), dt.datetime(2013,1,10,0,0,0)]
	# convert times to hours
	Qtime = [np.array(dp.dtList2hours(dates[0], Qdates))]
	# set new date times for the Qt
	Qtratio = [1./3., 2./3.]
	#Qdates, Qtime = getDatesofQt(dates, Qtratio)
	# radius
	r = [np.array(.5*5280.0)]
	# aquifer parameters
	stor = 0.01
	trans = 1000.
	pparS = np.array([stor, trans])
	# create list
	pumpingParams = [Q, r, Qtime, pparS]
	# permanent initial vars
	Qrange = [0.1*Q[0], 10.*Q[0]]
	rrange = [5., 2.*5280.]
	Srange = [0.01, 0.3]
	Trange = [100, 10000]
	#pparS0 = pparS
	# 
	#  B&ET regression parameters
	#
	# good value?
	alphag = 0.01*np.array([-0.15, -0.20, -0.10, -0.05])
	betag = 0.001*np.array([-0.015, 0.020, -0.010])
	# create list
	betParams = [alphag, betag]
	#betParams = []
	#
	#  Noise specifications
	#
	#sep = 0.07
	sep = 0
	width = 0.025
	gsdev = 0.004
	rep = False
	# create list
	noiseParams = [sep, width, gsdev, rep]
	#noiseParams = []
	#
	#  Smoothing correction type
	#
	#stype = 'smooth'
	stype = 'none'
	lstype = [stype]
	lstype = ['none', 'smooth', 'none', 'none']
	#
	#  alpha/beta numbers
	#
	alpham = 1
	betam = 0
	# create list
	betn = [alpham, betam]
	lbetn = [betn]
	lbetn = [[0,0], [0,0], [1,0], [7,7]]
	
	#
	#  Do everything and return all the results in one structure!
	#
	testtype = 'Random B+ET signals + noise types + pumping'
	# is there saved data out there to add to?
	if os.path.isfile('results/'+datafile+'.pkl'):
		apds = dp.loadObjectFromFile(datafile)
	else:
		apds = {}
	# number of saved tests in dataset
	tID0 = len(apds)
	tIDi = tID0
	# number of total samples in the space
	ntests = 5
	# number of samples per sub test
	nstests = 5
	# loop over interested parameters
	for j in range(ntests):
		# set variables to use in test set
		#randr = random.uniform(-4, 2)
		#sep   = 10**random.uniform(randr-1, randr+1)
		dates = genRandTimespanInLAdata(daterange)
		timeLoc = [dates, pdir]
		#
		Qdates, Qtime = getDatesofQt(dates, Qtratio)
		Q, r, Si, Ti = screwUpPumpingParams(Qrange, rrange, Srange, Trange)
		pparS = np.array([Si, Ti])
		pumpingParams = [Q, r, [np.array(Qtime)], pparS]
		# print out status and time
		print '\nprogress: '+str(float(j)/float(ntests)*100.)+'% '
		print 'd var(s) = ', dates, Q, r, Qdates, pparS
		ti = time.clock()
		print 'elapsed time: '+str((ti - tstart)/60.)+' minutes'
		# do the different types of smoothing and
		for k in range(len(lstype)):
			stype = lstype[k]
			betn = lbetn[k]
			# do number of subtests in sample set
			for i in range(nstests):
				pds = cbs.doOnePumpTest(timeLoc, heightParams, pumpingParams, 
					betParams, noiseParams, betn, stype)
				pds['title'] = testtype+' test '+str(tIDi)
				cbs.printTestResults(pds)
				# save to full dataset
				testid = 't'+str(tIDi)
				apds[testid] = pds
				# increment test ID
				tIDi += 1

	#
	#  Results
	#
	dp.saveObjectToFile(apds, datafile )
	
	# tell me it's over
	end = time.clock()
	print 'Total time elapsed: '+str((end - tstart)/60.)+' minutes'
	
	# END OF EXAMPLE
	return 0

def plotBETTestsData(datafile='resBET'):
	"""
	"""
	apds = dp.loadObjectFromFile(datafile)
	# assemble data into sensible list structures
	nsdata = cbs.assembleErrorData(apds, 'none')
	sdata = cbs.assembleErrorData(apds, 'smooth')
	b0data = cbs.assembleErrorData(apds, 'simplebaro')
	bet7data = cbs.assembleErrorData(apds, 'baroet7')

	# get the data in binned form
	nsbins = 1
	bins = 10**np.linspace(-3,3,6*nsbins+1) #zxcvb
	# set the data
	nsed = cbs.extractBinStats(nsdata, bins)
	sed = cbs.extractBinStats(sdata, bins)
	b0ed = cbs.extractBinStats(b0data, bins)
	bet7ed = cbs.extractBinStats(bet7data, bins)
	
	#
	#  Plots
	#
	xlimiter = (10**-3, 10**3) #zxcvb
	
	# Plot the norms
	plt.figure()
	plt.loglog(nsdata['stdev'], nsdata['norm'], 'r+', label='regular data')
	plt.loglog(sdata['stdev'], sdata['norm'], 'g+', label='smoothed data')
	plt.loglog(b0data['stdev'], b0data['norm'], 'b+', label='single baro corrected data')
	plt.loglog(bet7data['stdev'], bet7data['norm'], 'y+', label='baro+ET corrected data')
	plt.xlim( xlimiter )
	plt.xlabel("dimensionless standard deviation")
	plt.ylabel("dimensionless drawdown norm")
	plt.legend(loc=2)
	
	# Plot the norms by region
	plt.figure()
	plt.loglog()
	plt.errorbar(nsed['norm']['x'], nsed['norm']['y'], yerr=nsed['norm']['ey'], 
		capthick=cpthick, capsize=cpsize, color='red', label='regular data')
	plt.errorbar(sed['norm']['x'], sed['norm']['y'], yerr=sed['norm']['ey'], 
		capthick=cpthick, capsize=cpsize, color='green', label='smoothed data')
	plt.errorbar(b0ed['norm']['x'], b0ed['norm']['y'], yerr=b0ed['norm']['ey'], 
		capthick=cpthick, capsize=cpsize, color='blue', label='single baro corrected data')
	plt.errorbar(bet7ed['norm']['x'], bet7ed['norm']['y'], yerr=bet7ed['norm']['ey'], 
		capthick=cpthick, capsize=cpsize, color='yellow', label='baro+ET corrected data')
	plt.xlim( xlimiter )
	plt.xlabel("dimensionless standard deviation")
	plt.ylabel("dimensionless drawdown norm")
	plt.legend(loc=2)
	
	# Plot the errors in T and S
	plt.figure()
	plt.loglog(nsdata['stdev'], nsdata['Serror'], 'r+', label='Error in S')
	plt.loglog(nsdata['stdev'], nsdata['Terror'], 'm+', label='Error in T')
	plt.loglog(sdata['stdev'], sdata['Serror'], 'g+', label='Error in S - smoothed')
	plt.loglog(sdata['stdev'], sdata['Terror'], 'c+', label='Error in T - smoothed')
	plt.loglog(b0data['stdev'], b0data['Serror'], 'b+', label='Error in S - single Baro')
	plt.loglog(b0data['stdev'], b0data['Terror'], 'k+', label='Error in T - single Baro')
	plt.loglog(bet7data['stdev'], bet7data['Serror'], 'y+', label='Error in S - baro + ET')
	plt.loglog(bet7data['stdev'], bet7data['Terror'], '+', color='orange', label='Error in T - baro + ET')
	plt.xlim( xlimiter )
	plt.xlabel("dimernsionless standard deviation")
	plt.ylabel("error")
	plt.legend(loc=2)
	
	# Plot the errors in T and S by region
	plt.figure()
	plt.loglog()
	plt.errorbar(nsed['Serror']['x'], nsed['Serror']['y'], yerr=nsed['Serror']['ey'], 
		capthick=cpthick, capsize=cpsize, color='red', label='Error in S')
	plt.errorbar(nsed['Terror']['x'], nsed['Terror']['y'], yerr=nsed['Terror']['ey'], 
		capthick=cpthick, capsize=cpsize, color='magenta', label='Error in T')
	plt.errorbar(sed['Serror']['x'], sed['Serror']['y'], yerr=sed['Serror']['ey'], 
		capthick=cpthick, capsize=cpsize, color='green', label='Error in S - smoothed')
	plt.errorbar(sed['Terror']['x'], sed['Terror']['y'], yerr=sed['Terror']['ey'], 
		capthick=cpthick, capsize=cpsize, color='cyan', label='Error in T - smoothed')
	plt.errorbar(b0ed['Serror']['x'], b0ed['Serror']['y'], yerr=b0ed['Serror']['ey'], 
		capthick=cpthick, capsize=cpsize, color='blue', label='Error in S - single Baro')
	plt.errorbar(b0ed['Terror']['x'], b0ed['Terror']['y'], yerr=b0ed['Terror']['ey'], 
		capthick=cpthick, capsize=cpsize, color='black', label='Error in T - single Baro')
	plt.errorbar(bet7ed['Serror']['x'], bet7ed['Serror']['y'], yerr=bet7ed['Serror']['ey'], 
		capthick=cpthick, capsize=cpsize, color='yellow', label='Error in S - baro + ET')
	plt.errorbar(bet7ed['Terror']['x'], bet7ed['Terror']['y'], yerr=bet7ed['Terror']['ey'], 
		capthick=cpthick, capsize=cpsize, color='orange', label='Error in T - baro + ET')
	plt.xlim( xlimiter )
	plt.xlabel("dimernsionless standard deviation")
	plt.ylabel("error")
	plt.legend(loc=4)

	# Show the figures
	plt.show()
	
	# END OF EXAMPLE
	return 0

def plotBETTestsDataPub(datafile='resBET'):
	"""
	
	These plots may be used in the paper
	"""
	# load the data from file
	apds = dp.loadObjectFromFile(datafile)
	
	#
	#  Collect the data sub-sets
	#
	# assemble data into sensible list structures
	nsdata = cbs.assembleErrorData(apds, 'none')
	sdata = cbs.assembleErrorData(apds, 'smooth')
	b0data = cbs.assembleErrorData(apds, 'simplebaro')
	bet7data = cbs.assembleErrorData(apds, 'baroet7')

	# get the data in binned form
	nsbins = 1
	bins = 10**np.linspace(-1,5,6*nsbins+1) #zxcvb
	# set the data
	nsed = cbs.extractBinStats(nsdata, bins)
	sed = cbs.extractBinStats(sdata, bins)
	b0ed = cbs.extractBinStats(b0data, bins)
	bet7ed = cbs.extractBinStats(bet7data, bins)
	# sets of data for all plots
	alldata = [nsed, sed, b0ed, bet7ed]
	labels = ['raw', 'smoothed', 'single Baro', 'baro + ET']
	colors = ['red', 'green', 'blue', 'yellow']
	
	#
	#  Plots
	#
	# overall prep
	xlimiter = (10**-1, 10**5)
	stdxlabel = 'dimensionless standard deviation'
	ylimiter = (10**-3, 10**4)
	
	# Plot the norms by region
	plt.figure()
	plt.subplot(221)
	plt.text(3*10**-1, 10**4, r'$\mathrm{\mathbf{(a)}}$', fontweight='bold', fontsize=sfls)
	plt.loglog()
	for i in range(len(alldata)):
		dati = alldata[i]
		plt.errorbar(dati['norm']['x'], dati['norm']['y'], yerr=dati['norm']['ey'], 
			capthick=cpthick, capsize=cpsize, color=colors[i], label=labels[i])
	plt.xlim( xlimiter )
	plt.xlabel( stdxlabel )
	plt.ylim( (10**-2, 10**5) )
	plt.ylabel("dimensionless drawdown norm")
	
	# Plot the rate of failures in convergence
	plt.subplot(222)
	plt.text(3*10**-1, 0.8, r'$\mathrm{\mathbf{(b)}}$', fontweight='bold', fontsize=sfls)
	for i in range(len(alldata)):
		dati = alldata[i]
		plt.semilogx(dati['norm']['x'], dati['Failure Rate'], color=colors[i], label=labels[i])
	plt.xlim( xlimiter )
	plt.xlabel( stdxlabel )
	plt.ylabel("failure rate")
	
	# Plot the errors in S by region
	plt.subplot(223)
	plt.text(3*10**-1, 10**3, r'$\mathrm{\mathbf{(c)}}$', fontweight='bold', fontsize=sfls)
	plt.loglog()
	for i in range(len(alldata)):
		dati = alldata[i]
		plt.errorbar(dati['Serror']['x'], dati['Serror']['y'], yerr=dati['Serror']['ey'], 
			capthick=cpthick, capsize=cpsize, color=colors[i], label=labels[i])
	plt.xlim( xlimiter )
	plt.xlabel( stdxlabel )
	plt.ylim( ylimiter )
	plt.ylabel(r"error in $S$")
	plt.legend(bbox_to_anchor=(1.55, -0.15), ncol=2)

	# Plot the errors in T by region
	plt.subplot(224)
	plt.text(3*10**-1, 10**3, r'$\mathrm{\mathbf{(d)}}$', fontweight='bold', fontsize=sfls)
	plt.loglog()
	for i in range(len(alldata)):
		dati = alldata[i]
		plt.errorbar(dati['Terror']['x'], dati['Terror']['y'], yerr=dati['Terror']['ey'], 
			capthick=cpthick, capsize=cpsize, color=colors[i], label=labels[i])
	plt.xlim( xlimiter )
	plt.xlabel( stdxlabel )
	plt.ylim( ylimiter )
	plt.ylabel(r"error in $T$")

	# Show the figures
	plt.show()
	
	# END OF EXAMPLE
	return 0
###############################################################################
###############################################################################

#
#  function running code
#
if __name__ == "__main__":
	
	# Baro/ET tests
	dfbe = 'resBET'
	#runNSaveBETTests(dfbe)
	#plotBETTestsData(dfbe)
	plotBETTestsDataPub(dfbe)
	# END OF RUNS

#
#  End of File
#