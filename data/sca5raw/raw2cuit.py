import sys

infilename = sys.argv[1]
outfilename = sys.argv[2]

with open(infilename, "r") as f:
	lines = f.readlines()
	f.close()

outfile = open(outfilename, "w")
for line in lines:
	splitline = line.split()
	date = splitline[0]
	splitdate = date.split("/")
	month = splitdate[0]
	day = splitdate[1]
	year = splitdate[2]
	date = "-".join(map(lambda x: str(x).zfill(2), [year, month, day]))
	time = splitline[1]
	splittime = time.split(":")
	hour = int(splittime[0])
	minute = int(splittime[1])
	second = int(splittime[2])
	ampm = splitline[2]
	if "PM" in ampm and hour < 12:
		hour += 12
	if "AM" in ampm and hour == 12:
		hour = 0
	time = ":".join(map(lambda x: str(x).zfill(2), [hour, minute, second]))
	seconds = splitline[3]
	temp = splitline[5]
	psi = splitline[4]
	wl = splitline[6]
	outfile.write(date + " " + time + "," + wl + "\n")
outfile.close()
