import sys

infilename = sys.argv[1]
outfilename = sys.argv[2]

with open(infilename, "r") as f:
	lines = f.readlines()
	f.close()

outfile = open(outfilename, "w")
for line in lines:
	splitline = line.split()
	month = splitline[0]
	day = splitline[1]
	year = splitline[2]
	hour = splitline[3]
	minute = splitline[4]
	second = "00"
	press = splitline[6]
	date = "-".join(map(lambda x: str(x).zfill(2), [year, month, day]))
	time = ":".join(map(lambda x: str(x).zfill(2), [hour, minute, second]))
	outfile.write(date + " " + time + "," + press + "\n")
outfile.close()
