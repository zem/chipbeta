""" Corrected Head In Pumping, Barometric, & Earth Tide Applications (CHIPBETA) functions

These functions process and correct pressure head data for pumping, barometric and 
tidal forces using deconvolution by a least squares optimization method.

Authors: **Eric M. Benner and Daniel O'Malley**

Date:    7/9/14

Updated: 9/15/14
"""

"""
TODO: Software License?
"""

"""
TODO: Use try/except structures?
"""

# Packages
import copy
import datetime
import math
import numpy as np
import scipy.interpolate as interp
import scipy.optimize as optim
import scipy.special as spec
import scipy.stats as stats

#
#  Pumping routines
#
# TODO: May need to write C function to speed up these pumping routines.
# TODO: Do not write it C; we need to couple it with Wells 

# Theis equation of drawdown in aquifer
def theis(t, Q, r, S, T):
	"""Calculate drawdown at a well using Theis equation.
	
	The standard Theis equation is
	
	.. math::
	   D = \\frac{Q}{4 \pi T} \mathrm{E}_i(u), \\textrm{ where } u = \\frac{r^2 S}{4 T t}
	
	and the internal function is the exponential integral.
	
	:param t: Time (t)
	:type t: fl64
	:param Q: Pumping rate (L^3/t)
	:type Q: fl64
	:param r: Well distance (t)
	:type r: fl64
	:param S: Storativity (-)
	:type S: fl64
	:param T: Transmisivity (L^2/t)
	:type T: fl64
	:returns: Drawdown (L)
	:rtype: fl64

	.. _fun-theis:
	"""
	if t == 0 or t == 0.:
		return 0.
	else:
		u = r*r*S/(4*T*t)
		s = -Q/(4*math.pi*T)*spec.expi(-u)
	# END OF FUNCTION
	return s

# Theis equation of drawdown in aquifer with logarithmic parameters
def theisLogST(t, Q, r, logS, logT):
	"""Calculate drawdown using Theis equation with logarithmic S & T.
	
	The standard Theis equation is
	
	.. math::
	   D = \\frac{Q}{4 \pi T} \mathrm{E}_i(u), \\textrm{ where } u = \\frac{r^2 S}{4 T t}
	
	and the internal function is the exponential integral.

	In this function the inputs are changed to log of the 
	aquifer parameters.
	This allows for significantly improved convergence of the
	pumping analysis due to order-of-magnitude influence.
	
	:param t: Time (t)
	:type t: fl64
	:param Q: Pumping rate (L^3/t)
	:type Q: fl64
	:param r: Well distance (L)
	:type r: fl64
	:param logS: log10 of Storativity (-)
	:type logS: fl64
	:param logT: log10 of Transmisivity (L^2/t)
	:type logT: fl64
	:returns: Drawdown (L)
	:rtype: fl64

	.. _fun-theisLogST:
	"""
	S = 10**logS
	T = 10**logT
	if t == 0:
		return 0.
	else:
		u = r*r*S/(4*T*t)
		s = -Q/(4*math.pi*T)*spec.expi(-u)
	# END OF FUNCTION
	return s

# Function for computing total pumping at a specific time
def drawdownVariablePumping(t, Q, r, Qtime, params, fun=theis):
	"""Wrapper for computing total pumping effect at given time

	Function computes the drawdown at a specific time by comparing to the 
	pumping rates at given times.  This method is an adaptive wrapper for
	various drawdown functions.
	
	:param t: Time (t)
	:type t: fl64
	:param Q: Pumping rates (L^3/t)
	:type Q: ndrray of fl64
	:param r: Well distance (L)
	:type r: fl64
	:param Qtime: Times of pumping rate change (L^3/t)
	:type Qtime: ndarray of fl64
	:param params: drawdown function parameters
	:type params: list of fl64
	:param fun: function to compute drawdown (default=theis)
	:type fun: function handle
	:returns: Drawdown (L)
	:rtype: fl64

	.. _fun-drawdownVariablePumping:
	"""
	dd = 0.
	Qprev = 0.
	i = 0
	# set 
	while i < len(Qtime) and t > Qtime[i]:
		dd += fun(t - Qtime[i], Q[i] - Qprev, r, *params)
		Qprev = Q[i]
		i += 1
	# END OF FUNCTION
	return dd

# Function gets full drawdown by all wells for full time range
def getPumpingEffect(T, Q, r, Qtime, ppar, fun=theis):
	"""Computes full drawdown over all pumping wells over time
	
	Length of *Q*, *r*, and *Qtime* lists must be the same
	
	:param T: Time (t)
	:type T: ndarray of fl64
	:param Q: All wells' pumping rates (L^3/t)
	:type Q: list of ndarrays of fl64
	:param r: Well distances (L)
	:type r: list of fl64
	:param Qtime: All wells' times of pumping rate change (L^3/t)
	:type Qtime: list of ndarrays of fl64
	:param ppar: drawdown function parameters
	:type ppar: list of fl64
	:param fun: function to compute drawdown (default=theis)
	:type fun: function handle
	:returns: drawdown over time (L)
	:rtype: ndarray of fl64

	.. _fun-getPumpingEffect:
	"""
	if len(Q) == 0:
		return np.zeros(len(T))
	P = np.zeros(len(T))
	npar = len(ppar) / len(Q)
	# calculate for each well
	for i in range(len(Q)):
		params = ppar[i * npar:(i + 1) * npar]
		dd = np.array(map(lambda t: drawdownVariablePumping(t, Q[i], r[i], Qtime[i], params, fun), T))
		P -= dd
	# END OF FUNCTION
	return P

#
#  Residual Routines
#
# TODO: need to have a built in way to adjust for long-term downward linear trend (This should be coupled with Theis analysis)
# TODO: routine to optimize the length of alpha and beta arrays.

def getdHStar(T, B, ET, Q, r, Qtime, alpha, beta, ppar, fun=theis):
	"""Calculate correction variable at each time

	:param T: Time (t)
	:type T: ndarray of fl64
	:param B: Barometric (N/A)
	:type B: ndarray of fl64
	:param ET: Earth tide (N/A)
	:type ET: ndarray of fl64
	:param Q: All wells' pumping rates (L^3/t)
	:type Q: list of ndarrays of fl64
	:param r: Well distances (L)
	:type r: list of fl64
	:param Qtime: All wells' times of pumping rate change (L^3/t)
	:type Qtime: list of ndarrays of fl64
	:param alpha: Barometric correction vector
	:type alpha: ndarrays of fl64
	:param beta: Earth tide correction vector
	:type beta: ndarrays of fl64
	:param ppar: drawdown function parameters
	:type ppar: list of fl64
	:returns: Well head correction
	:rtype: ndarray of fl64

	.. _fun-getdHStar:
	"""
	# lengths
	al = len(alpha)
	bl = len(beta)
	mab= max(al, bl)
	# differential arrays
	dB = np.diff(B)
	dET = np.diff(ET)
	dl = len(dB)
	dHStar = np.zeros(dl - mab)
	# calc baro and ET effect
	for j in range(al):
		dHStar += alpha[j] * dB[mab-j:dl-j]
	for j in range(bl):
		dHStar += beta[j] * dET[mab-j:dl-j]
	# calc pumping effect
	dHStar += np.diff(getPumpingEffect(T[mab:], Q, r, Qtime, ppar, fun))
	# END OF FUNCTION
	return dHStar

def getSquaredError(T, H, B, ET, Q, r, Qtime, alpha, beta, ppar, fun=theis):
	"""Calculate residual for data and parameters

	:param T: Time (t)
	:type T: ndarray of fl64
	:param H: Pressure Head (L)
	:type H: ndarray of fl64
	:param B: Barometric (N/A)
	:type B: ndarray of fl64
	:param ET: Earth tide (N/A)
	:type ET: ndarray of fl64
	:param Q: All wells' pumping rates (L^3/t)
	:type Q: list of ndarrays of fl64
	:param r: Well distances (L)
	:type r: list of fl64
	:param Qtime: All wells' times of pumping rate change (L^3/t)
	:type Qtime: list of ndarrays of fl64
	:param alpha: Barometric correction vector
	:type alpha: ndarray of fl64
	:param beta: Earth tide correction vector
	:type beta: ndarray of fl64
	:param ppar: drawdown function parameters
	:type ppar: list of fl64
	:returns: residual
	:rtype: fl64

	.. _fun-getSquaredError:
	"""
	mab = max(len(alpha), len(beta))
	# differences
	dH = np.diff(H)[mab:]
	dHStar = getdHStar(T, B, ET, Q, r, Qtime, alpha, beta, ppar, fun)
	dHSqr = dH - dHStar
	dHSqr = map(lambda x: x ** 2, dHSqr)
	# END OF FUNCTION
	return sum(dHSqr)

def getAlphaBetLsq(H, B, ET, al, bl):
	"""Solve for alpha and beta vectors by linear regression

	In this function we determine the best-fit values of :math:`\\alpha` 
	and :math:`\\beta` using linear regression deconvolution of the equation

	.. math::
	   \\Delta H^*(T) = \\sum_{i=0}^{m_a} \\alpha(i) \\Delta B(t-i) + \\sum_{j=0}^{m_b} \\beta(j) \\Delta ET(t-j).
	
	This is computed using the standard least squares routine on the 
	correlation matrix and :math:`\\Delta H^*` vector.

	This function may also be accesed using the simple function call
	getAlphaBet().

	:param H: Pressure Head (L)
	:type H: ndarray of fl64
	:param B: Barometric (N/A)
	:type B: ndarray of fl64
	:param ET: Earth tide (N/A)
	:type ET: ndarray of fl64
	:param al: length of :math:`\\alpha`
	:type al: int
	:param bl: length of :math:`\\beta`
	:type bl: int
	:returns: Barometric correction vector (:math:`\\alpha`) and Earth tide correction vector (:math:`\\beta`)
	:rtype: Tuple of 2 ndarrays of fl64

	.. _fun-getAlphaBetLsq:
	
	.. _fun-getAlphaBet:
	"""
	mab = max(al, bl)
	# differences
	dB = np.diff(B)
	dET= np.diff(ET)
	dl = len(dB)
	# determine coefficient(s) for various cases of required numbers
	if (al > 0 and bl > 0) or (al > 1): # enough to invert matrix
		# assemble correlation matrix
		Mi = np.array(dB[mab:])
		for j in range(1, al):
			Mi = np.vstack([Mi, dB[mab-j:dl-j]])
		for j in range(bl):
			Mi = np.vstack([Mi, dET[mab-j:dl-j]])
		M = Mi.T
		# solve for coefficients
		dH = np.diff(H[mab:])
		ab0 = np.linalg.lstsq(M, dH)[0]
		# get coefficients
		a0 = ab0[:al]
		b0 = ab0[al:]
	elif al == 1 and bl == 0:
		# solve for coefficient
		dH = np.diff(H)
		a0 = np.array([np.inner(dH,dB)/np.inner(dB,dB)])
		b0 = np.array([])
	elif al == 0 and bl == 1:
		# solve for coefficient
		dH = np.diff(H)
		b0 = np.array([np.inner(dH,dET)/np.inner(dET,dET)])
		a0 = np.array([])
	else:
		a0 = []
		b0 = []
	# END OF FUNCTION
	return a0, b0

getAlphaBet = getAlphaBetLsq

def theilsen(y, x):
	"""Calculates Theil Sen estimator

	This function calculates the median slope for a set of data using
	the Theil-Sen method.
	
	Function is used in getAlphaBetTheil().

	:param y: dependent data
	:type y: ndarray of fl64
	:param x: independent data
	:type x: ndarray of fl64
	:returns: median
	:rtype: fl64

	.. _fun-theilsen:
	"""
	# prototype set
	#y = [-.1, 1.2, 1.9, 3.1, 3.8]
	#x = [0, 1, 2, 3, 4]
	numgoods = 0
	for i in range(len(x)):
		for j in range(len(x)):
			if x[i] != x[j]:
				numgoods += 1
	ms = np.zeros(numgoods)
	numgoods = 0
	for i in range(len(x)):
		for j in range(len(x)):
			if x[i] != x[j]:
				ms[numgoods] = (y[j] - y[i]) / (x[j] - x[i])
				numgoods += 1
	ms.sort()
	retval = np.median(ms)
	# END OF FUNCTION
	return retval

def getAlphaBetTheil(H, B, ET, al, bl):
	"""Solve for alpha and beta vectors through Theil Sen estimator

	In this function we determine the best-fit values of :math:`\\alpha` 
	and :math:`\\beta` using the Theil Sen estimator given in the function
	theilsen(y, x).

	:param H: Pressure Head (L)
	:type H: ndarray of fl64
	:param B: Barometric (N/A)
	:type B: ndarray of fl64
	:param ET: Earth tide (N/A)
	:type ET: ndarray of fl64
	:param al: length of :math:`\\alpha`
	:type al: int
	:param bl: length of :math:`\\beta`
	:type bl: int
	:returns: Barometric correction vector (:math:`\\alpha`) and Earth tide correction vector (:math:`\\beta`)
	:rtype: Tuple of 2 ndarrays of fl64

	.. _fun-getAlphaBetTheil:
	"""
	workingH = np.array(H)
	dB = np.diff(B)
	dET= np.diff(ET)
	alpha = np.zeros(al)
	beta = np.zeros(bl)
	for i in range(al):
		dH = np.diff(workingH)
		if i == 0:
			alpha[i] = theilsen(dH, dB)
			workingH[1:] -= np.cumsum(alpha[i] * dB)
		else:
			alpha[i] = theilsen(dH[i:], dB[:-i])
			workingH[i + 1:] -= np.cumsum(alpha[i] * dB[:-i])
	for i in range(bl):
		dH = np.diff(workingH)
		if i == 0:
			beta[i] = theilsen(dH, dET)
			workingH[1:] -= np.cumsum(beta[i] * dET)
		else:
			beta[i] = theilsen(dH[i:], dET[:-i])
			workingH[i + 1:] -= np.cumsum(beta[i] * dET[:-i])
	# END OF FUNCTION
	return alpha, beta

def guessPumpingParams(t, H, Q, r, Qtime, TTdefault=500., Sdefault=0.05):
	""" Use Jacob's Method to estimate pumping parameters
	
	Jacob's approximation of the Theis equation for drawdown may be used to
	determine the approximate value of *T* and *S*;
	
	.. math::
	   T = \\frac{2.3 Q}{4 \\pi \\Delta s}
	
	.. math::
	   S = \\frac{2.25 T t_0}{r^2}
	
	where delta s and t_0 are found using the slope and x-intercept of the 
	pumping time semilogx plot of the drawdown versus the pumping.
	
	:param t: Time (t)
	:type t: fl64
	:param H: Pressure Head (L)
	:type H: ndarray of fl64
	:param Q: Pumping rates (L^3/t)
	:type Q: ndrray of fl64
	:param r: Well distance (L)
	:type r: fl64
	:param Qtime: Times of pumping rate change (L^3/t)
	:type Qtime: ndarray of fl64
	:returns: drawdown function parameters (ppar0)
	:rtype: ndarray of list of 2 fl64
	
	.. _fun-guessPumpingParams:
	"""
	if len(t) != len(H):
		print 'Error:  Length of time and drawdown do not match!'
		return np.array([0, 0])
	# data prep
	cip = t[0] - Qtime[0]
	cep = t[0] - Qtime[1]
	# if undefined:
	Istart = 1
	Iend = len(t)
	for i in range(len(t)-1):
		i += 1
		ci = t[i] - Qtime[0]
		ce = t[i] - Qtime[1]
		if ci*cip <= 0:
			Istart = i
		if ce*cep <= 0:
			Iend = i
		cip = ci
		cep = ce
	# semilogx fitting
	lt = np.log10(t[Istart:Iend]-t[Istart-1])
	Head = H[Istart:Iend]-np.average(H[Istart-10:Istart])
	ifit = len(lt)/2
	m, intr, res,p,std = stats.linregress(lt[ifit:], -Head[ifit:])
	t0 = 10**(-intr/m)
	# Jacob's Method
	if m < 0:
		TT = TTdefault
		S = Sdefault
	else:
		TT = (2.3*Q[0])/(4*math.pi*m)
		S = (2.25*TT*t0)/(r*r)
	ppar0 = np.array([S, TT])	
	# END OF FUNCTION
	return ppar0

def getPumpingResiduals(T, H, Q, r, Qtime, ppar, fun=theis):
	"""Calculate the residuals for the current pumping parameters
	
	:param T: Time (t)
	:type T: ndarray of fl64
	:param H: Pressure Head (L)
	:type H: ndarray of fl64
	:param Q: All wells' pumping rates (L^3/t)
	:type Q: list of ndarrays of fl64
	:param r: Well distances (L)
	:type r: list of fl64
	:param Qtime: All wells' times of pumping rate change (L^3/t)
	:type Qtime: list of ndarrays of fl64
	:param ppar: Drawdown function parameters
	:type ppar: list of fl64
	:returns: residual
	:rtype: fl64

	.. _fun-getPumpingResiduals:
	"""
	#print 'pumping residual data'
	#print len(T)
	#print len(H)
	# remove pumping effect
	Hrp = H - getPumpingEffect(T, Q, r, Qtime, ppar, fun)
	# normalize
	Hrpn = Hrp - np.mean(Hrp)
	return Hrpn
	# END OF FUNCTION

def getPumpingResidual(T, H, Q, r, Qtime, ppar, fun=theis):
	"""Calculate the residual for the current pumping parameters
	
	:param T: Time (t)
	:type T: ndarray of fl64
	:param H: Pressure Head (L)
	:type H: ndarray of fl64
	:param Q: All wells' pumping rates (L^3/t)
	:type Q: list of ndarrays of fl64
	:param r: Well distances (L)
	:type r: list of fl64
	:param Qtime: All wells' times of pumping rate change (L^3/t)
	:type Qtime: list of ndarrays of fl64
	:param ppar: Drawdown function parameters
	:type ppar: list of fl64
	:returns: residual
	:rtype: fl64

	.. _fun-getPumpingResidual:
	"""
	#print 'pumping residual data'
	#print len(T)
	#print len(H)
	# remove pumping effect
	#Hrp = H - getPumpingEffect(T, Q, r, Qtime, ppar, fun)
	# normalize
	#Hrpn = Hrp - np.mean(Hrp)
	# END OF FUNCTION
	#return np.sum(Hrpn * Hrpn)
	Hrpn = getPumpingResiduals(T, H, Q, r, Qtime, ppar, fun)
	return np.sum(np.abs(Hrpn))

#
#  Primary User functions
#

def getBestParams(T, H, B, ET, Q, r, Qtime, alpha0, beta0, ppar0, smoothing='none', cmode='log', itr=4):
	"""Iteratively determine optimal fitting parameters
	
	This routine is the primary function for iteratively utilizing 
	linear regression, nonlinear optimization, and smoothing routines
	to extract pumping signals/parameters from a data set.
	
	:param T: Time (t)
	:type T: ndarray of fl64
	:param H: Pressure Head (L)
	:type H: ndarray of fl64
	:param B: Barometric (N/A)
	:type B: ndarray of fl64
	:param ET: Earth tide (N/A)
	:type ET: ndarray of fl64
	:param Q: All wells' pumping rates (L^3/t)
	:type Q: list of ndarrays of fl64
	:param r: Well distances (L)
	:type r: list of fl64
	:param Qtime: All wells' times of pumping rate change (L^3/t)
	:type Qtime: list of ndarrays of fl64
	:param alpha0: Initial Barometric correction vector
	:type alpha0: ndarray of fl64
	:param beta0: Initial Earth tide correction vector
	:type beta0: ndarray of fl64
	:param ppar0: Initial drawdown function parameters
	:type ppar0: list of fl64
	:param smoothing: Mode of smoothing correction on data (options: none, smooth, bimodal, smoothbimodal) (default=none)
	:type smoothing: str
	:param cmode: Mode for convergence (opdions: log or normal) (default=log)
	:type cmode: str
	:param itr: Number of linear regression & pumping iterations (default=4)
	:type itr: str
	:returns: Barometric correction vector (alpha), Earth tide correction vector (beta), and pumping parameters (ppar)
	:rtype: Tuple of 3 ndarrays of fl64
	
	.. _fun-getBestParams:
	"""
	# initialize linear parameters
	if len(alpha0) != 0 or len(beta0) != 0:
		mab = max(len(alpha0), len(beta0))
		mabp = mab + 1
		alpha, beta = getAlphaBet(H, B, ET, len(alpha0), len(beta0))
	else:
		mab = 0
		mabp = mab + 1
		alpha = alpha0
		beta = beta0
	# if no pumping; return the linear effects only
	if len(ppar0) == 0:
		return alpha, beta, ppar0
	# initialize smoothing settings
	smoother = smoothingControl(smoothing)
	#
	#  Pumping modes
	#
	# normal Theis convergence method
	if cmode=='normal':
		# first pass denoise of data
		Hstar0 = correctHead(T, H, B, ET, [], [], [], alpha, beta, [])
		Hstar0 = smoother(Hstar0)
		# first pass pumping parameter estimation
		ppar = optim.fmin_l_bfgs_b(lambda x: getPumpingResidual(T[mabp:], Hstar0, Q, r, Qtime, map(lambda y: y, x)), ppar0, maxfun=int(1e2), approx_grad=True, bounds=[(1e-9, 1), (1e-3, 1e6)])[0]
		# iterate over the data
		for i in range(itr-1):
			Hi = H - getPumpingEffect(T, Q, r, Qtime, ppar)
			alpha, beta = getAlphaBet(Hi, B, ET, len(alpha0), len(beta0))
			Hstari = correctHead(T, H, B, ET, [], [], [], alpha, beta, [])
			Hstari = smoother(Hstari)
			ppar = optim.fmin_l_bfgs_b(lambda x: getPumpingResidual(T[mabp:], Hstari, Q, r, Qtime, map(lambda y: y, x)), ppar, maxfun=int(1e2), approx_grad=True, bounds=[(1e-9, 1), (1e-3, 1e6)])[0]
	elif cmode=='log':
		# logarithmic Theis convergence method
		fun = theisLogST
		# change parameters into log form
		ppar0 = map(np.log10,ppar0)
		# first pass denoise of data
		Hstar0 = correctHead(T, H, B, ET, [], [], [], alpha, beta, [])
		Hstar0 = smoother(Hstar0)
		# first pass pumping parameter estimation theisLogST
		ppar = optim.fmin_l_bfgs_b(lambda x: getPumpingResidual(T[mabp:], Hstar0, Q, r, Qtime, x, fun), ppar0, maxfun=int(1e2), bounds=[(-9., 0.), (-3., 6.)], approx_grad=True)[0]
		#ppar = optim.leastsq(lambda x: getPumpingResiduals(T[mabp:], Hstar0, Q, r, Qtime, map(lambda y: y, x), fun=theisLogST), ppar0, maxfev=int(1e1))[0]
		# iterate over the data
		for i in range(itr-1):
			Hi = H - getPumpingEffect(T, Q, r, Qtime, ppar,fun)
			alpha, beta = getAlphaBet(Hi, B, ET, len(alpha0), len(beta0))
			Hstari = correctHead(T, H, B, ET, [], [], [], alpha, beta, [])
			Hstari = smoother(Hstari)
			ppar = optim.fmin_l_bfgs_b(lambda x: getPumpingResidual(T[mabp:], Hstari, Q, r, Qtime, x, fun), ppar, maxfun=int(1e2), bounds=[(-9., 0.), (-3., 6.)], approx_grad=True)[0]
			#ppar = optim.leastsq(lambda x: getPumpingResiduals(T[mabp:], Hstari, Q, r, Qtime, map(lambda y: y, x), fun=theisLogST), ppar, maxfev=int(1e1))[0]
		ppar = 10**ppar
	elif cmode=='experimental':
		# Theis using logarithmic iteration (not working)
		print 'Are you sure you want to do this one?'
		# first pass denoise of data
		Hstar0 = correctHead(T, H, B, ET, [], [], [], alpha, beta, [])
		Hstar0 = smoother(Hstar0)
		# first pass pumping parameter estimation theisLogST
		#ppar = optim.fmin_l_bfgs_b(lambda x: getPumpingResidual(T[mabp:], Hstar0, Q, r, Qtime, x, ), ppar0, maxfun=int(1e2), approx_grad=True)[0]
		#ppar = optim.fmin_tnc(lambda x: getPumpingResidual(T[mabp:], Hstar0, Q, r, Qtime, x), ppar0, approx_grad=True, maxfun=int(1e2), bounds=[(-5., 0.), (0., 5.)])[0]
		ppar = optim.fmin_l_bfgs_b(lambda x: getPumpingResidual(T[mabp:], Hstar0, Q, r, Qtime, x), map(np.log10, ppar0), maxfun=int(1e2), approx_grad=True)[0]
		#ppar = optim.fmin_l_bfgs_b(lambda x: getPumpingResidual(T[mabp:], Hstar0, Q, r, Qtime, map(lambda y: y, x)), ppar0, maxfun=int(1e2), approx_grad=True, bounds=[(1e-9, 1), (1e-3, 1e6)])[0]
		# iterate over the data
		for i in range(itr-1):
			Hi = H - getPumpingEffect(T, Q, r, Qtime, ppar)
			alpha, beta = getAlphaBet(Hi, B, ET, len(alpha0), len(beta0))
			Hstari = correctHead(T, H, B, ET, [], [], [], alpha, beta, [])
			Hstari = smoother(Hstari)
			ppar = optim.fmin_l_bfgs_b(lambda x: getPumpingResidual(T[mabp:], Hstari, Q, r, Qtime, map(lambda y: 10 ** y, x)), map(np.log10, ppar), maxfun=int(1e2), approx_grad=True, bounds=[(1e-9, 1), (1e-3, 1e6)])[0]
			#ppar = optim.fmin_l_bfgs_b(lambda x: getPumpingResidual(T[mabp:], Hstari, Q, r, Qtime, map(lambda y: y, x)), ppar, maxfun=int(1e2), approx_grad=True, bounds=[(1e-9, 1), (1e-3, 1e6)])[0]
	else:
		print 'Error: incorrect convergence option'
	# END OF FUNCTION
	return alpha, beta, ppar

def correctHead(T, H, B, ET, Q, r, Qtime, alpha, beta, ppar, fun=theis):
	"""Calculate corrected head data
	
	:param T: Time (t)
	:type T: ndarray of fl64
	:param H: Pressure Head (L)
	:type H: ndarray of fl64
	:param B: Barometric (N/A)
	:type B: ndarray of fl64
	:param ET: Earth tide (N/A)
	:type ET: ndarray of fl64
	:param Q: All wells' pumping rates (L^3/t)
	:type Q: list of ndarrays of fl64
	:param r: Well distances (L)
	:type r: list of fl64
	:param Qtime: All wells' times of pumping rate change (L^3/t)
	:type Qtime: list of ndarrays of fl64
	:param alpha: Barometric correction vector
	:type alpha: ndarray of fl64
	:param beta: Earth tide correction vector
	:type beta: ndarray of fl64
	:param ppar: Drawdown function parameters
	:type ppar: list of fl64
	:returns: Corrected well pressure head
	:rtype: ndarray of fl64

	.. _fun-correctHead:
	"""
	mab = max(len(alpha), len(beta))
	cumdHStar = np.cumsum(getdHStar(T, B, ET, Q, r, Qtime, alpha, beta, ppar, fun))
	Hcorr = np.array(H[mab + 1:]) - cumdHStar
	# END OF FUNCTION
	return Hcorr

#
#  External correction routines
#
def removeSamplingEvents(waterlevels, wldts, threshold=0.15, samplinglength=5):
	"""Makes an effort to remove sampling events from a water level sequence

	:param waterlevels: Observed water levels
	:type waterlevels: list of fl64
	:param wldts:  Times at which the water levels were observed
	:type wldts: list of datetime objects
	:param threshold: The threshold for determining when something is a sampling event (default=0.15)
	:type threshold: fl64
	:param samplinglength: The number of neighboring water levels on each side that a sampling event might encompass (default=5)
	:type samplinglength: int
	:returns: water levels without sampling events and denoised water level times without sampling events
	:rtype: Tuple of 2 lists of fl64

	.. _fun-removeSamplingEvents:
	"""
	newwaterlevels = list(waterlevels[:samplinglength])
	newwldts = wldts[:samplinglength]
	j = 0
	for i in range(samplinglength, len(waterlevels) - samplinglength):
		pastwl = waterlevels[i - samplinglength]
		futurewl = waterlevels[i + samplinglength]
		if abs(waterlevels[i] - .5 * (pastwl + futurewl)) < threshold:
			newwaterlevels.append(waterlevels[i])
			newwldts.append(wldts[i])
			j += 1
	newwaterlevels.extend(waterlevels[-samplinglength:])
	newwldts.extend(wldts[-samplinglength:])
	# TODO: Use cubic splines to interpolate missing data?
	# END OF FUNCTION
	return newwaterlevels, newwldts

def denoise(waterlevels, wldts, barometricpressures, barodts, earthtide, etdts, numalpha=7, numbeta=7, regressionmethod="leastsquares"):
	"""Get water levels with reduced impact from barometric pressure and earth tide force

	:param waterlevels: Observed water levels
	:type waterlevels: list or ndarray of fl64
	:param wldts:  Times at which the water levels were observed
	:type wldts: list of datetime objects
	:param barometricpressures: Observed barometric pressures
	:type barometricpressures: list or ndarray of fl64
	:param barodts: Times at which the barometric pressures were observed
	:type barodts: list of datetime objects
	:param earthtide: Observed or calculated Earth tide forces
	:type earthtide: list or ndarray of fl64
	:param etdts: Times corresponding to the Earth tide forces
	:type etdts: list of datetime objects
	:param numalpha: Hours in the past to correlate for barometric effects (default=7)
	:type numalpha: int
	:param numbeta: Hours in the past to correlate for Earth tide effects (default=7)
	:type numbeta: int
	:returns: Denoised water levels and corresponding times
	:rtype: Tuple of 2 lists of fl64

	.. _fun-denoise:
	"""
	if len(etdts) == 0:
		raise ValueError("Earth tide list is empty")
	if len(wldts) == 0:
		raise ValueError("Water level list is empty")
	if len(barodts) == 0:
		raise ValueError("Barometric list is empty")
	denoisebegin = max(min(etdts), min(barodts), min(wldts))
	#denoiseend = min(max(etdts), max(barodts), max(wldts))
	date0 = datetime.datetime(denoisebegin.year, denoisebegin.month, denoisebegin.day)
	wltimes = []
	barotimes = []
	ettimes = []
	for dt in wldts:
		delta = dt - date0
		wltimes.append(delta.days * 24. + delta.seconds / 3600.)
	for dt in barodts:
		delta = dt - date0
		barotimes.append(delta.days * 24. + delta.seconds / 3600.)
	for dt in etdts:
		delta = dt - date0
		ettimes.append(delta.days * 24. + delta.seconds / 3600.)
	firsttime = int(math.ceil(max(min(wltimes), min(barotimes), min(ettimes))))
	lasttime = int(math.floor(min(max(wltimes), max(barotimes), max(ettimes))))
	fwl = interp.interp1d(wltimes, waterlevels)
	fbaro = interp.interp1d(barotimes, barometricpressures)
	fet = interp.interp1d(ettimes, earthtide)
	evaltimes = range(firsttime, lasttime + 1)
	H = fwl(evaltimes)
	B = fbaro(evaltimes)
	ET = fet(evaltimes)
	if regressionmethod == "theilsen":
		alpha, beta = getAlphaBetTheil(H, B, ET, numalpha, numbeta)
	else:
		alpha, beta = getAlphaBetLsq(H, B, ET, numalpha, numbeta)
	goodwaterlevels = correctHead(evaltimes, H, B, ET, [], [], [], alpha, beta, [])
	goodtimes = map(lambda x: date0 + datetime.timedelta(hours=x), evaltimes[numalpha + 1:])
	# END OF FUNCTION
	return goodwaterlevels, goodtimes

#
#  Data smoothing routines
#
def smooth(X, n=3):
	"""Regenerate smoothed data over a range of n points
	
	This function smoothes out a set of data by taking *n*
	points before and after the point and averaging that 
	value at that point (except near the edges of the data).
	
	:param X: original data
	:type X: ndarray or list of fl64
	:param n: smoothing distance (default=3)
	:type n: int
	:returns: corrected data
	:rtype: ndarray or list of fl64
	
	.. _fun-smooth:
	"""
	xl = len(X)
	Y = np.zeros(xl)
	for i in range(xl):
		div = 0
		for j in range(-n, n + 1):
			if i + j >= 0 and i + j < xl:
				div += 1
				Y[i] += X[i + j]
		Y[i] /= div
	# END OF FUNCTION 
	return Y

def correctBimodalData( H ):
	"""Adjust upper and lower parts of a bimodal dataset to an average behavior
	
	This function assumes that the input data has a bimodal structure 
	(I.E. data has a distinct an upper and lower band).  
	It process the data to move the upper band down, and lower band up
	such that the average value of the data is preserved. 
	**Only use this function if the data is bimodal noisy!**
	
	:param H: original pressure head
	:type H: ndarray of fl64
	:returns: corrected pressure head  
	:rtype: ndarray of fl64

	.. _fun-correctBimodalData:
	"""
	#
	#  Internal functions
	#
	def firstSwitch( x ):
		"""Finds the upper or lower direction of the first modulation"""
		for i in range(len(x)):
			if x[i] != 0:
				return x[i]
		return 0
	def setFirstPoint( mul, pul ):
		"""Sets the value of the first point in H as upper/lower"""
		if mul[0] == 1:
			pul0 = -pul[0]
		elif mul[0] == -1:
			pul0 = -pul[0]
		else: #mul[0] == 0
			pul0 = pul[0]
		return pul0
	#
	#  Initialize core data
	#
	# find differential data
	dH = np.diff(H)
	#print dH
	# determine modal separation/2
	#sep = 2./3.*np.average(abs(dH))*1.1
	sep = 2./3.*np.average(abs(dH))
	#print sep
	#
	#  determine location of upper/lower points
	#
	# initialize list
	mul = []
	# modulation of data
	for i in range(len(dH)):
		if dH[i] > sep:
			mul.append(1)
		elif dH[i] < -sep:
			mul.append(-1)
		else: # in between
			mul.append(0)
	# initialize list
	pul = []
	# upper/lower position
	for i in range(len(mul)):
		if mul[i] == 1: # upper
			pul.append(1)
		elif mul[i] == -1: # lower
			pul.append(-1)
		else: # in between
			if i == 0:
				pul.append(-firstSwitch(mul))
			else:
				pul.append(pul[-1])
	# set the first point's value
	pul.insert(0, setFirstPoint(mul, pul))
	# process to 2 lists of upper and lower point locations
	# initialize lists
	upos = []
	lpos = []	
	cpos = []
	for i in range(len(pul)):
		if pul[i] == 1:
			upos.append(i)
		elif pul[i] == -1:
			lpos.append(i)
		else:
			#raise ValueError("Failed to classify upper/lower positions!")
			cpos.append(i)
	# Error stabilizing check
	if len(upos) == 0 or len(lpos) == 0:
		print 'Warning: bimodal classification is failing!'
		print '         - Returning original data set'
		return H
	#print upos
	#print lpos
	#
	#  find verticle deviations
	#
	# get total average
	avgH = np.average(H)
	# get upper average
	summeru = 0
	for j in range(len(upos)):
		summeru += H[upos[j]]
	avgu = summeru/len(upos)
	# set upper deviation
	devu = avgu - avgH

	# get lower average
	summerl = 0
	for j in range(len(lpos)):
		summerl += H[lpos[j]]
	avgl = summerl/len(lpos)
	# set lower deviation
	devl = avgl - avgH
	#
	#  data adjustment
	#
	Hul = copy.deepcopy(H)
	# upward adjustment (toward lower)
	for j in range(len(upos)):
		Hul[upos[j]] -= devu
	# downward adjustment (toward upper)
	for j in range(len(lpos)):
		Hul[lpos[j]] -= devl
	# TODO: add readjustments for over-corrected points
	# END OF FUNCTION
	return Hul

def correctAndSmoothBimodalData( H, n=3 ):
	"""Use smoothing AND correction for bimodal data
	
	This function is a wrapper for the functions correctBimodalData() 
	and smooth() to dually opperate on noisy data.
	**Only use this function if the data is bimodal noisy!**
	
	Also known as awesomeizeData().
	
	:param X: original data
	:type X: ndarray or list of fl64
	:param n: smoothing distance (default=3)
	:type n: int
	:returns: corrected and smoothed data
	:rtype: ndarray or list of fl64
	
	.. _fun-correctAndSmoothBimodalData:
	"""
	# END OF FUNCTION
	return smooth(correctBimodalData( H ), n)

def noSmooth(X, s=0):
	"""Don't do anything to data
	
	Ghost function for smoothingControl().
	
	:param X: original data
	:type X: ndarray or list of fl64
	:returns: original dat
	:rtype: ndarray or list of fl64
	
	.. _fun-noSmooth:
	"""
	# END OF FUNCTION 
	return X

def smoothingControl(smoothing='none'):
	''' Recieves string smoothing setting, returns function to smooth with
	
	There are several options, each corresponding to a routine;
	none, smooth, bimodal, smoothbimodal
	
	:param smoothing: Mode of smoothing correction on data  (default=none)
	:type smoothing: str
	:returns: function handle
	:rtype: pointer
	
	.. _fun-smoothingControl:
	'''
	if smoothing=='none':
		smoother = noSmooth
	elif smoothing=='smooth':
		smoother = smooth
	elif smoothing=='bimodal':
		smoother = correctBimodalData
	elif smoothing=='smoothbimodal':
		smoother = correctAndSmoothBimodalData
	else:
		print 'Warning: Unrecognized smoothing option; no smoothing added.'
		smoother = noSmooth
	return smoother

#
#  Data post-analysis functions
#
# TODO: Where should post-processing go? Move to dataproc?
def L2norm(X, Y):
	""" Returns the 2-norm between 2 vectors
	
	:param X: First dataset
	:type X: ndarray or list of fl64
	:param Y: Second dataset
	:type Y: ndarray or list of fl64
	:returns: Calculated norm
	:rtype: fl64
	
	.. _fun-L2norm:
	"""
	if len(X) != len(Y):
		print 'Lengths do not match!'
		return 0
	norm = 0	
	for i in range(len(X)):
		norm += (X[i]-Y[i])*(X[i]-Y[i])
	norm = np.sqrt(norm)
	# END OF FUNCTION
	return norm

def averageDrawDown(T, Hp, Qt):
	""" Returns the integrated average drawdown of pumping signal
	
	:param T: Time in hours or seconds
	:type T: ndarray (or list) of fl64 (or int)
	:param Hp: Pumping signal
	:type Hp: ndarray or list of fl64
	:returns: Average integral drawdown
	:rtype: fl64
	
	.. _fun-averageDrawDown:
	"""
	if len(T) != len(Hp):
		print 'Lengths do not match!'
		return 0
	summ = 0	
	for i in range(len(T)):
		if T[i] > Qt[0][0]:
			summ += Hp[i]
	dtt = T[-1] - Qt[0][0]
	avgd = summ/dtt
	# END OF FUNCTION
	return avgd

# 
#  end of file
#
