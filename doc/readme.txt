README.TXT for chipbeta documentation

By: Eric M. Benner

Updated: 8/6/14


Compiling This Documentation
============================

To compile the html documentation, from the doc directory the user will type in the commandline:

``$ make html``

Similarly a LaTeX pdf can be produced by:

``$ make latexpdf``

Note that a full and up to date pdflatex must be installed.  
Some font errors are known to occur with old LaTeX distributions.


Sphinx and Restructured Text File Resources
===========================================

The general page for sphinx documentation is located at:

http://sphinx-doc.org/index.html


For the first place to start with getting a feel for how to work with sphinx, I recommend matplotlib's sampledoc:

http://matplotlib.org/sampledoc/index.html


An alternative getting started tutorial is the pypi package:

https://pythonhosted.org/an_example_pypi_project/intro.html


A great quick reference for .rst file conventions is:

http://docutils.sourceforge.net/docs/ref/rst/restructuredtext.html


TODO
====

.. todo:: Write basic tutorial into the documentation.  
          We may want to take elements from the cbexamples to create this.

.. todo:: Update function descriptions (e.g. standardize nd/np arrays, variable types, etc.)

.. todo:: Update equations in function descriptions

.. todo:: Hyperlink function names across documentation and function descriptions



