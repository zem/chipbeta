.. chipbeta documentation master file, created by
   sphinx-quickstart on Fri Aug  1 11:49:02 2014.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to chipbeta's documentation!
====================================

In this documentation page for chipbeta (Corrected Head In Pumping, Barometric, & Earth Tide Applications), we discuss the use of chipbeta.

Contents:

.. toctree::
   :maxdepth: 2

   intro
   chipbeta
   dataproc
   cbexamples
   cbtest

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

