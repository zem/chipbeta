.. _intro-chapter:

Introduction
============

Python is an open-source, object-oriented scripting language with extensive functionality, a large, active 
development community and loads of online resources for troubleshooting. 
Chipbeta functions can process well pressure head data to account for the effects of barometric, earth tide, pumping, and certain types of experimental error. 
Chipbeta functionality includes

1. Routines for processing physical effects (see Chapter :ref:`2 <cb-chapter>`)

2. Functions for data input and correction (see Chapter :ref:`3 <dp-chapter>`)

3. Practical examples (see Chapter :ref:`4 <cbex-chapter>`)

4. Test suite (see Chapter :ref:`5 <cbt-chapter>`)

A script to run a single problem of current interest is included in the distribution, but not discussed in this documentation because this file is often changing significantly and scripts are harder to document.

.. An introductory :ref:`tutorial <cbtutorial-chapter>` to Chipbeta is also provided.

Installation
------------

Python 
^^^^^^

Chipbeta is supported on Python 2.6 and 2.7, but NOT 3.x. 
Instructions for downloading and installing Python can be found at `www.python.org`__.

.. _Python: https://www.python.org

__ Python_

Chipbeta requires the following Python modules to be installed: NumPy, SciPy, Matplotlib. 
For windows users, 32- and 64-bit installers (for several Python versions) of these modules can be obtained from http://www.lfd.uci.edu/~gohlke/pythonlibs/

Chipbeta
^^^^^^^^

The current repository version of Chipbeta can be downloaded through GitLab at `gitlab.com/ebenner/chipbeta`__.

.. _cbgit: https://gitlab.com/ebenner/chipbeta

__ cbgit

.. To install, download and extract the zip file, and run the setup script at the command line: ``python setup.py install``

Using Chipbeta
--------------

Chipbeta consists of several Python modules. 
To access their functionality, the user must include the following line at the top of any Python script

``from chipbeta import *``

Using this manual
-----------------

This manual comprises sections for each of the main Chipbeta modules and scripts: 
:ref:`chipbeta.py <cb-chapter>`, :ref:`dataproc.py <dp-chapter>`, :ref:`cbexamples.py <cbex-chapter>`, and :ref:`cbtest.py <cbt-chapter>`. 
In these, all user functions are documented.  

We recommend that the beginner run and browse through the source for :ref:`cbexamples.py <cbex-chapter>` to familiarize with the capabilities of Chipbeta.


