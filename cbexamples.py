""" This script runs interesting examples of chipbeta results.

Authors: **Eric M. Benner and Daniel O'Malley**

Date:    7/24/14

Updated: 9/10/14
"""

"""
TODO: Software License?
"""

import chipbeta as cb
import dataproc as dp
import matplotlib.pyplot as plt
import numpy as np

# 
#  Regular string for starting example
#
def printExInitMessage(mstring):
	# Standard initialization message string for chipbeta examples
	print ""
	print "******************************************************************"
	print "  "+mstring
	print "******************************************************************"
	print ""

#######################################################################
#  Dataproc.py feature examples
#######################################################################

# TODO: add more examples of dataproc

#
#  Noisy Data Examples
#
def demonstrateNoisifiers():
	"""Test and plot all types of noise adders
	
	This function tests the following functions from dataproc.py
	makeDataJumpy(), makeDataRandNoisy(), makeDataGaussNoisy(),
	and makeDataVeryNoisy().
	
	.. _fun-demonstrateNoisifiers:
	"""
	printExInitMessage("Running Example Illustrating makeDataNoisy Routines")
	# Time 
	nt = 1000.
	T = np.linspace(0., nt, nt+1)
	# specs
	sep = 1
	width = 0.35
	stdev = 0.05
	rep = False
	# calc data
	datJ = dp.makeDataJumpy(1.*sep*np.ones(len(T)), sep, rep)
	datR = dp.makeDataRandNoisy(2.*sep*np.ones(len(T)), width, rep)
	datG = dp.makeDataGaussNoisy(3.*sep*np.ones(len(T)), stdev, rep)
	datN = dp.makeDataVeryNoisy(4.*sep*np.ones(len(T)), sep, width, stdev, rep)
	# plot
	plt.figure()
	#plt.title(r"$T$")
	plt.title('various kinds of noisy data')
	plt.plot(T, datJ, label='Jumpy')
	plt.plot(T, datR, label='Random')
	plt.plot(T, datG, label='Gaussian')
	plt.plot(T, datN, label='Jumpy Noisy')
	plt.xlabel("time (h)")
	plt.ylabel("drawdown (ft)")
	plt.legend(loc=1)
	# plot histogram of differentials
	plt.figure()
	nbins = 50
	#plt.title(r"$T$")
	plt.title('various kinds of noisy data')
	plt.hist(np.diff(datJ), bins=nbins, label='Jumpy')
	plt.hist(np.diff(datR), bins=nbins, label='Random')
	plt.hist(np.diff(datG), bins=nbins, label='Gaussian')
	plt.hist(np.diff(datN), bins=nbins, label='Jumpy Noisy')
	plt.xlabel("drawdown (ft)")
	plt.ylabel("counts")
	plt.legend(loc=1)
	# show
	plt.show()
	# END OF EXAMPLE
	return 0

def reproduceTransducerNoise():
	"""Plot reproduced noise signal of LANL transducers
	
	This function uses the makeData*Noisy routines from dataproc.py
	to reproduce the noisy signal from select transducers at LANL.
	
	.. _fun-reproduceTransducerNoise:
	"""
	printExInitMessage("Running Transducer Noise Reproduction Example")
	# Time 
	nt = 500.
	T = np.linspace(0., nt, nt+1)
	# specs
	sep = 0.07
	width = 0.025
	stdev = 0.004
	rep = True
	# calc data
	datJ = dp.makeDataJumpy(1.*sep*np.ones(len(T)), sep, rep)
	datR = dp.makeDataRandNoisy(2.*sep*np.ones(len(T)), width, rep)
	datG = dp.makeDataGaussNoisy(3.*sep*np.ones(len(T)), stdev, rep)
	datN = dp.makeDataVeryNoisy(4.*sep*np.ones(len(T)), sep, width, stdev, rep)
	# recalc smoothed from noisy
	datS = cb.correctBimodalData(datN) + sep

	# plot data
	plt.figure()
	#plt.title(r"$T$")
	plt.title('Correlating noisy data signals')
	plt.plot(T, datJ, label='Jumpy')
	plt.plot(T, datR, label='Random')
	plt.plot(T, datG, label='Gaussian')
	plt.plot(T, datN, label='Jumpy Noisy')
	plt.plot(T, datS, label='Smoothed')
	plt.xlabel("time (h)")
	plt.ylabel("drawdown (ft)")
	plt.legend(loc=1)

	# plot histogram of differentials
	plt.figure()
	nbins = 50
	#plt.title(r"$T$")
	plt.title('neighbor differentials')
	plt.hist(np.diff(datJ), bins=nbins, label='Jumpy')
	plt.hist(np.diff(datR), bins=nbins, label='Random')
	plt.hist(np.diff(datG), bins=nbins, label='Gaussian')
	plt.hist(np.diff(datN), bins=nbins, label='Jumpy Noisy')
	plt.hist(np.diff(datS), bins=nbins, label='Smoothed')
	plt.xlabel("drawdown (ft)")
	plt.ylabel("counts")
	plt.legend(loc=1)
	# show
	plt.show()
	# END OF EXAMPLE
	return 0

#######################################################################
#  Chipbeta.py feature examples
#######################################################################

#
#  Illustrate correction of bimodal-jumping data with contrived data
#
def runBimodalCorrSimple():
	"""Example of correced bimodal data with contrived data
	
	This function runs a short made-up data set to visually validate
	the bimodal correction routine by plotting the corrections and differentials.
	This function has no inputs or outputs.
	
	.. _fun-runBimodalCorrSimple:
	"""
	printExInitMessage("Running Rudimentary Bimodial Correction Example")
	# example dataset
	T = np.linspace(0,20,21)
	# weird exception to corrected smoothing!
	#H = [1, 1, 0, 1, 0, 1, 1, 1, 0, 0, 0, 1, 2, 1, 0, -1, 0, 0, 1, 0, 1]
	# this one is ok to run
	H = [1, 1, 0, 1, 0, 1, 1, 1, 0, 0, 0, 1, 2, 1, 0, 0, -1, 0, 1, 0, 1]
	# adjust the data
	Hn = cb.correctBimodalData(H)

	# plot results
	plt.figure()
	plt.subplot(211)
	plt.plot(T, H, 'b', label='original')
	plt.plot(T, Hn, 'g', linewidth=2, label='processed')
	plt.xlabel("time (h)")
	plt.ylabel("well head (ft)")
	plt.legend(loc=2)
	# show histogram
	plt.subplot(212)
	plt.hist(np.diff(Hn), color='green', label='processed', bins=50)
	plt.hist(np.diff(H), color='blue', label='original', bins=50)
	plt.xlabel("differential well head (ft)")
	plt.ylabel("counts")
	plt.legend(loc=2)
	#
	plt.show()
	return 0

#
#  Illustrate correction of bimodal-jumping data with experimental data
#
def runBimodalCorrExample():
	"""Example of correced bimodal data using real data

	This function runs a standard example data set to visually validate
	the bimodal correction routine by plotting the corrections and differentials.
	This function has no inputs or outputs.
	
	.. _fun-runBimodalCorrExample:
	"""
	printExInitMessage("Running Bimodial Correction Example with Raw Data")
	# dataset to repair
	T, H, B, ET = dp.loadExR3Mo()
	# adjust the data
	Hn = cb.correctBimodalData(H)

	# plot results
	plt.figure()
	plt.subplot(211)
	plt.plot(T, H, 'b', label='original')
	plt.plot(T, Hn, 'g', linewidth=2, label='processed')
	plt.xlabel("time (h)")
	plt.ylabel("well head (ft)")
	plt.legend(loc=2)
	# show histogram
	plt.subplot(212)
	plt.hist(np.diff(Hn), color='green', label='processed', bins=50)
	plt.hist(np.diff(H), color='blue', label='original', bins=50)
	plt.xlabel("differential well head (ft)")
	plt.ylabel("counts")
	plt.legend(loc=2)
	#
	plt.show()
	return 0

#
#  Illustrate the Corrections with Added Smoothing functionality
#
def runBimodalSmoothingExample(smoothing=3):
	"""Example of correlating and smoothing bimodal data

	Function prints correlation parameters and plots head and head deaviations.
	This function has no outputs.
	
	:param smoothing: forward/backward smoothing range (default=3)
	:type smoothing: int
	
	.. _fun-runBimodalSmoothingExample:
	"""
	printExInitMessage("Running Data Correlation and Smoothing Example")
	# Code main settings
	# number of bins
	nbins = 50

	# Load the data set
	#T, H, B, ET = dp.loadKac1()
	#T, H, B, ET = dp.loadWipp30()
	T, H, B, ET = dp.loadExR3Mo()

	# parameter number settings
	alpham = 7
	betam = 7
	mab = max(alpham, betam)
	
	# Post-smoothed data
	# calc initial alpha beta
	alpha0, beta0 = cb.getAlphaBet(H, B, ET, alpham, betam)
	# get best parameters
	alpha, beta, ppar = cb.getBestParams(T, H, B, ET, [], [], [], alpha0, beta0, [])
	print "  Best parameters:"
	print "  Baro  params: ", alpha
	print "  Tidal params: ", beta
	# for naive correlation
	alpha10, beta10 = cb.getAlphaBet(H, B, 0*ET, 1, 1)
	alpha1, beta1, ppar1 = cb.getBestParams(T, H, B, 0*ET, [], [], [], alpha10, beta10, [])
	print "  Simple Barometric Fit:"
	print "  Baro factor: ", alpha1
	# correct the data
	correctedH = cb.correctHead(T, H, B, ET, [], [], [], alpha, beta, [])
	correctedH1 = cb.correctHead(T, H, B, 0*ET, [], [], [], alpha1, beta1, [])
	# (no earth tide correction)
	correctedH2 = cb.correctHead(T, H, B, 0*ET, [], [], [], alpha, beta, [])
	smoothH = cb.smooth(correctedH, smoothing)

	# Pre-smoothed data
	ismoothH = cb.smooth(H, smoothing)
	# calc initial alpha beta
	alpha0, beta0 = cb.getAlphaBet(ismoothH, B, ET, alpham, betam)
	# get best parameters
	alphasmooth, betasmooth, ppars = cb.getBestParams(T, ismoothH, B, ET, [], [], [], alpha0, beta0, [])
	print "  Smoothed parameters:"
	print "  Baro  params: ", alphasmooth
	print "  Tidal params: ", betasmooth
	# correct the data
	correctedSmoothH = cb.correctHead(T, ismoothH, B, ET, [], [], [], alphasmooth, betasmooth, [])
	
	# Plot the core data
	plt.figure()
	plt.subplot(211)
	plt.title("Data over time")
	plt.plot(T, H, 'r+', label="raw data")
	plt.plot(T[2:], correctedH1, 'k+', label="single baro") #*
	plt.plot(T[mab + 1:], correctedH2, 'm+', label="barometric")
	plt.plot(T[mab + 1:], correctedH, 'b+', label="tide and baro")
	plt.plot(T[mab + 1:], smoothH, 'g', label="smoothed")
	plt.plot(T[mab + 1:], correctedSmoothH, 'c', label="smoothed then corr")
	plt.xlabel("time (h)")
	plt.ylabel("well head (ft)")
	plt.legend(loc=2)

	# Plot the differential histograms
	plt.subplot(212)
	plt.title("Local variation of data")
	plt.hist(np.diff(H), bins=nbins, color='red', label="raw data")
	plt.hist(np.diff(correctedH2), bins=nbins, color='magenta', label="barometric")
	plt.hist(np.diff(correctedH), bins=nbins, color='blue', label="tide and baro")
	plt.hist(np.diff(smoothH), bins=nbins, color='green', label="smoothed")
	plt.hist(np.diff(correctedSmoothH), bins=nbins, color='cyan', label="smoothed then corr")
	plt.xlabel("differential well head (ft)")
	plt.ylabel("counts")
	plt.legend(loc=2)

	# Plot smoothing effects only
	plt.figure()
	plt.subplot(311)
	plt.plot(T[mab + 1:], smoothH, 'g', label="smoothed")
	plt.plot(T[mab + 1:], correctedSmoothH, 'c', label="smoothed then corr")
	plt.xlabel("time (h)")
	plt.ylabel("well head (ft)")
	# histograms post corr
	plt.subplot(312)
	plt.hist(np.diff(smoothH), bins=nbins, color='green', label="smoothed")
	plt.xlabel("differential well head (ft)")
	plt.ylabel("counts")
	# histograms pre corr
	plt.subplot(313)
	plt.hist(np.diff(correctedSmoothH), bins=nbins, color='cyan', label="smoothed then corr")
	plt.xlabel("differential well head (ft)")
	plt.ylabel("counts")
	
	# Show the figures
	plt.show()
	# END OF EXAMPLE
	return 0

#
#  Illustrate the Corrections with Bimodal Correction
#
def runFullBimodalCorrExample():
	"""Example of correlating and smoothing bimodal data
	
	Function prints correlation parameters and plots head and head deaviations.
	This function has no inputs or outputs.
	
	.. _fun-runFullBimodalCorrExample:
	"""
	printExInitMessage("Running Data Correlation and Bimodal Correction Example")
	# Code main settings
	# number of bins
	nbins = 50

	# Load the data set
	T, H, B, ET = dp.loadExR3Mo()

	# parameter number settings
	alpham = 7
	betam = 7
	mab = max(alpham, betam)
	
	# Post-smoothed data
	# calc initial alpha beta
	alpha0, beta0 = cb.getAlphaBet(H, B, ET, alpham, betam)
	# get best parameters
	alpha, beta, ppar = cb.getBestParams(T, H, B, ET, [], [], [], alpha0, beta0, [])
	print "  Best parameters:"
	print "  Baro  params: ", alpha
	print "  Tidal params: ", beta
	# for naive correlation
	alpha10, beta10 = cb.getAlphaBet(H, B, 0*ET, 1, 1)
	alpha1, beta1, ppar1 = cb.getBestParams(T, H, B, 0*ET, [], [], [], alpha10, beta10, [])
	print "  Simple Barometric Fit:"
	print "  Baro factor : ", alpha1
	# correct the data
	correctedH = cb.correctHead(T, H, B, ET, [], [], [], alpha, beta, [])
	correctedH1 = cb.correctHead(T, H, B, 0*ET, [], [], [], alpha1, beta1, [])
	# (no earth tide correction)
	correctedH2 = cb.correctHead(T, H, B, 0*ET, [], [], [], alpha, beta, [])
	smoothH = cb.correctBimodalData(correctedH)

	# Pre-smoothed data
	ismoothH = cb.correctBimodalData(H)
	# calc initial alpha beta
	alpha0, beta0 = cb.getAlphaBet(ismoothH, B, ET, alpham, betam)
	# get best parameters
	alphasmooth, betasmooth, ppars = cb.getBestParams(T, ismoothH, B, ET, [], [], [], alpha0, beta0, [])
	print "  Smoothed parameters:"
	print "  Baro  params: ", alphasmooth
	print "  Tidal params: ", betasmooth
	# correct the dro  params: ",:uata
	correctedSmoothH = cb.correctHead(T, ismoothH, B, ET, [], [], [], alphasmooth, betasmooth, [])
	
	# Plot the core data
	plt.figure()
	plt.subplot(211)
	plt.title("Data over time")
	plt.plot(T, H, 'r+', label="raw data")
	plt.plot(T[2:], correctedH1, 'k+', label="single baro") #*
	plt.plot(T[mab + 1:], correctedH2, 'm+', label="barometric")
	plt.plot(T[mab + 1:], correctedH, 'b+', label="tide and baro")
	plt.plot(T[mab + 1:], smoothH, 'g', label="smoothed")
	#plt.plot(T[mab + 1:], correctedSmoothH, 'c', label="bsmoothed then corr")
	plt.xlabel("time (h)")
	plt.ylabel("well head (ft)")
	plt.legend(loc=2)

	# Plot the differential histograms
	plt.subplot(212)
	plt.title("Local variation of data")
	plt.hist(np.diff(H), bins=nbins, color='red', label="raw data")
	plt.hist(np.diff(correctedH2), bins=nbins, color='magenta', label="barometric")
	plt.hist(np.diff(correctedH), bins=nbins, color='blue', label="tide and baro")
	plt.hist(np.diff(smoothH), bins=nbins, color='green', label="smoothed")
	#plt.hist(np.diff(correctedSmoothH), bins=nbins, color='cyan', label="bsmoothed then corr")
	plt.xlabel("differential well head (ft)")
	plt.ylabel("counts")
	plt.legend(loc=2)

	# Plot smoothing effects only
	plt.figure()
	plt.subplot(311)
	plt.plot(T[mab + 1:], smoothH, 'g', label="smoothed")
	plt.plot(T[mab + 1:], correctedSmoothH, 'c', label="smoothed then corr")
	plt.xlabel("time (h)")
	plt.ylabel("well head (ft)")
	# histograms post corr
	plt.subplot(312)
	plt.hist(np.diff(smoothH), bins=nbins, color='green', label="smoothed")
	plt.xlabel("differential well head (ft)")
	plt.ylabel("counts")
	# histograms pre corr
	plt.subplot(313)
	plt.hist(np.diff(correctedSmoothH), bins=nbins, color='cyan', label="smoothed then corr")
	plt.xlabel("differential well head (ft)")
	plt.ylabel("counts")
	
	# Show the figures
	plt.show()
	# END OF EXAMPLE
	return 0

#
#  Show Theis equation and parameter variation for realistic data
#
def runTheisParamExample():
	"""Example of variation of parameters in the Theis Equation
	
	Example plots the variation of the Theis curve for changing values
	of *Q*, *r*, *S*, and *T*.  
	This function has no inputs or outputs.
	
	.. _fun-runTheisParamExample:
	"""
	printExInitMessage("Running Theis Parameter Example")
	#
	# Initializing data
	#
	# time lengths
	day = 24
	mo = 30*day
	mof = float(mo)
	# set data
	Q = np.array([200., 0.])
	Qtime = np.array([1*mof, 2*mof])
	r = .5*5280.;
	# set parameters
	S = 0.01;
	T = 1000.;
	params = [S, T]
	# set time vector
	tres = 1000
	t = np.linspace(0., 6*mof, tres)
	# Example Theis calculation
	#plt.figure()
	#ddt = map(lambda t: cb.theis(t, Q[0], r, *params), t)
	#plt.plot(t, ddt)

	#
	# Q test
	#
	dd = map(lambda t: cb.drawdownVariablePumping(t, Q, r, Qtime, params), t)
	ddU= map(lambda t: cb.drawdownVariablePumping(t, Q*2., r, Qtime, params), t)
	ddU2=map(lambda t: cb.drawdownVariablePumping(t, Q*5., r, Qtime, params), t)
	ddD= map(lambda t: cb.drawdownVariablePumping(t, Q*0.5, r, Qtime, params), t)
	ddD2=map(lambda t: cb.drawdownVariablePumping(t, Q*0.2, r, Qtime, params), t)
	# plotting
	plt.figure()
	plt.subplot(221)
	plt.title(r"$Q$")
	plt.plot(t, dd)
	plt.plot(t, ddU)
	plt.plot(t, ddD)
	plt.plot(t, ddU2)
	plt.plot(t, ddD2)
	plt.xlabel("time (h)")
	plt.ylabel("drawdown (ft)")

	#
	# r test
	#
	dd = map(lambda t: cb.drawdownVariablePumping(t, Q, r, Qtime, params), t)
	ddU= map(lambda t: cb.drawdownVariablePumping(t, Q, r*2., Qtime, params), t)
	ddU2=map(lambda t: cb.drawdownVariablePumping(t, Q, r*5., Qtime, params), t)
	ddD= map(lambda t: cb.drawdownVariablePumping(t, Q, r*.5, Qtime, params), t)
	ddD2=map(lambda t: cb.drawdownVariablePumping(t, Q, r*.2, Qtime, params), t)
	# plotting
	#plt.figure()
	plt.subplot(222)
	plt.title(r"$r$")
	plt.plot(t, dd)
	plt.plot(t, ddU)
	plt.plot(t, ddD)
	plt.plot(t, ddU2)
	plt.plot(t, ddD2)
	plt.xlabel("time (h)")
	plt.ylabel("drawdown (ft)")

	# 
	#  S test
	#
	parU = [S*2, T]
	parD = [S*0.5, T]
	parU2 = [S*5, T]
	parD2 = [S*0.2, T]
	# 
	ddU= map(lambda t: cb.drawdownVariablePumping(t, Q, r, Qtime, parU), t)
	ddU2=map(lambda t: cb.drawdownVariablePumping(t, Q, r, Qtime, parU2), t)
	ddD= map(lambda t: cb.drawdownVariablePumping(t, Q, r, Qtime, parD), t)
	ddD2=map(lambda t: cb.drawdownVariablePumping(t, Q, r, Qtime, parD2), t)
	# plotting
	#plt.figure()
	plt.subplot(223)
	plt.title(r"$S$")
	plt.plot(t, dd)
	plt.plot(t, ddU)
	plt.plot(t, ddD)
	plt.plot(t, ddU2)
	plt.plot(t, ddD2)
	plt.xlabel("time (h)")
	plt.ylabel("drawdown (ft)")

	#
	#  T test
	#
	parU = [S, T*2]
	parD = [S, T*0.5]
	parU2 = [S, T*5]
	parD2 = [S, T*0.2]
	#
	ddU= map(lambda t: cb.drawdownVariablePumping(t, Q, r, Qtime, parU), t)
	ddU2=map(lambda t: cb.drawdownVariablePumping(t, Q, r, Qtime, parU2), t)
	ddD= map(lambda t: cb.drawdownVariablePumping(t, Q, r, Qtime, parD), t)
	ddD2=map(lambda t: cb.drawdownVariablePumping(t, Q, r, Qtime, parD2), t)
	# plotting
	#plt.figure()
	plt.subplot(224)
	plt.title(r"$T$")
	plt.plot(t, dd)
	plt.plot(t, ddU)
	plt.plot(t, ddD)
	plt.plot(t, ddU2)
	plt.plot(t, ddD2)
	plt.xlabel("time (h)")
	plt.ylabel("drawdown (ft)")

	# display all figures
	plt.show()
	# END OF EXAMPLE
	return 0

#
#  function testing code
#
if __name__ == "__main__":
	demonstrateNoisifiers()
	reproduceTransducerNoise()
	runBimodalCorrSimple()
	runBimodalCorrExample()
	runBimodalSmoothingExample()
	runFullBimodalCorrExample()
	runTheisParamExample()
	# END OF RUNS

#
#  End of File
#
